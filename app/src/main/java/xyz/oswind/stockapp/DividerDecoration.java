//
//  DividerDecoration.java
//  StockApp
//
//  A class that represents a divider between recycler view list items.
//
//  Created by David McKnight on 2016-04-22.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class DividerDecoration extends RecyclerView.ItemDecoration {
    final Drawable mDividerDrawable; /* The divider drawable to use onDraw. */

    /* Default constructor that prepares the divider for drawing. */
    public DividerDecoration(Context context) {
        super();
        /* Request the divider drawable to use onDraw. */
        mDividerDrawable = ContextCompat
                .getDrawable(context, R.drawable.list_divider);
    }

    @Override
    /* Draw the divider onto list items. */
    public void onDraw(Canvas canvas, RecyclerView parent,
                       RecyclerView.State state) {
        /* Obtain parent bounds. */
        int lBound = parent.getLeft() + parent.getPaddingLeft();
        int rBound = parent.getRight() - parent.getPaddingRight();
        /* Iterate over all list items. */
        for (int index = 0; index < parent.getChildCount(); index++) {
            View child = parent.getChildAt(index); /* Request a list item. */
            /* Obtain the list item layout. */
            RecyclerView.LayoutParams layout = (RecyclerView.LayoutParams)
                    child.getLayoutParams();
            /* Obtain top and bottom bounds from the list item. */
            int tBound = child.getBottom() + layout.bottomMargin;
            int bBound = tBound + mDividerDrawable.getIntrinsicHeight();
            /* Set the bounds of the divider drawable. */
            mDividerDrawable.setBounds(lBound, tBound, rBound, bBound);
            /* Draw the divider onto this list item. */
            mDividerDrawable.draw(canvas);
        }
    }
}
