//
//  StockQueryDecoration.java
//  StockApp
//
//  A class that represents a custom decoration on a SearchView. Many convenient
//  XML attributes were added in Lollipop but they are unavailable on prior
//  APIs. This class encapsulates an OnFocusChangeListener and functionality
//  which can be used to provide a consistent appearance on pre-Lollipop
//  devices. Setting the OnQueryTextFocusChangeListener of a SearchView to an
//  instance of this class is all that is required.
//
//  Created by David McKnight on 2016-04-23.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.SearchView;

public class StockQueryDecoration
        implements View.OnFocusChangeListener {
    private final InputMethodManager mInputMethodManager; /* Keyboard control. */
    private final View mQueryLayout; /* The query text layout to decorate. */

    /* A default constructor which accepts the search view to decorate. The
     * parameter `context` allows explicit keyboard control when focus changes.
     * The activity context should suffice for keyboard control. */
    public StockQueryDecoration(Context context, SearchView searchView) {
        /* Request the input method manager for keyboard control. */
        mInputMethodManager = (InputMethodManager)context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        /* Request the search plate identifier. */
        int queryTextLayoutId = searchView
                .getResources() /* Request the generated resources. */
                /* Request the contained layout's identifier. */
                .getIdentifier("android:id/search_plate", null, null);
        /* Request the query text layout to decorate. */
        mQueryLayout = searchView.findViewById(queryTextLayoutId);
        /* Apply the default background before focused state takes effect. */
        mQueryLayout.setBackgroundResource(R.drawable.bg_search_default);
    }

    @Override
    /* Change the drawable background based on focused state. */
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            /* The parent search view is the current focus. Change the
             * background to indicate focused state. */
            mQueryLayout.setBackgroundResource(R.drawable.bg_search_focused);
            /* Keyboard will display automatically on focus gained. */
        } else {
            /* The parent search view is not the current focus. Change the
             * background to indicate unfocused state. */
            mQueryLayout.setBackgroundResource(R.drawable.bg_search_default);
            /* Explicitly remove the keyboard on focus lost. */
            mInputMethodManager.hideSoftInputFromWindow(
                    v.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
