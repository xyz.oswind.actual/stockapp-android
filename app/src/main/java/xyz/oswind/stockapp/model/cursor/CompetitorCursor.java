//
//  CompetitorCursor.java
//  StockApp
//
//  A wrapper class for convenient access to the contents of a cursor. Enables
//  the easy extraction of Competitor object from the database.
//
//  Created by David McKnight on 2016-05-03.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.model.cursor;

import android.database.Cursor;

import xyz.oswind.stockapp.model.entities.Competitor;
import xyz.oswind.stockapp.model.StockDbSchema.CompetitorsTable;

public class CompetitorCursor extends StockDbCursor {
    /* A default constructor which wraps a competitor data cursor. */
    public CompetitorCursor(Cursor cursor) {
        super(cursor);
    }

    /* Extract a Competitor from the cursor. Note that the cursor must have been
     * positioned at the row containing the competitor entity of interest.
     * Returns a Competitor object corresponding to the current cursor row. */
    public Competitor getCompetitor() {
        Long stockId = getLong(
                getColumnIndex(CompetitorsTable.Cols.SYMBOL_ID));
        String symbol = getString(
                getColumnIndex(CompetitorsTable.Cols.COMPETITOR_SYMBOL));
        String name = getString(
                getColumnIndex(CompetitorsTable.Cols.COMPETITOR_NAME));
        String percentChg = getString(
                getColumnIndex(CompetitorsTable.Cols.PERCENT_CHANGE));
        String marketCap = getString(
                getColumnIndex(CompetitorsTable.Cols.MARKET_CAP));
        /* Return the initialized Competitor object. */
        return new Competitor(stockId, symbol, name, percentChg, marketCap);
    }
}
