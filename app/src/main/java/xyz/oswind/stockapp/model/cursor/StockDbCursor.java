//
//  StockDbCursor.java
//  StockApp
//
//  A base wrapper class for convenient access to the contents of a cursor.
//  Enables the easy extraction of StockApp objects from the database.
//
//  Created by David McKnight on 2016-05-05.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.model.cursor;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.provider.BaseColumns;

public class StockDbCursor extends CursorWrapper {
    /* A default constructor which wraps a cursor containing stockapp data. */
    public StockDbCursor(Cursor cursor) {
        super(cursor);
    }

    /* Extract and return the primary key. */
    public long getId() {
        return getLong(getColumnIndex(BaseColumns._ID));
    }
}
