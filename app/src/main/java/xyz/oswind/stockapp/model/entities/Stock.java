//
//  Stock.java
//  StockApp
//
//  An object that represents a stock which is currently being displayed or
//  persisted in StockApp.
//
//  Created by David McKnight on 2016-04-17.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.model.entities;

public class Stock {
    private Long mId;            /* The stock SQL id if persisted. */
    private String mStockSymbol; /* The stock symbol. */
    private String mStockName;   /* The stock name. */
    private Long mStatusId;      /* If exists the stock status or null. */
    private String mLastUpdate;  /* If exists last status update or null. */

    /* A default constructor which accepts parameters for the Stock symbol and
     * name. Returns a Stock object initialized with the values passed. */
    public Stock(String stockSymbol, String stockName) {
        this(null, stockSymbol, stockName, null, null);
    }

    /* A default constructor which accepts parameters for the Stock symbol and
    * name. Returns a Stock object initialized with the values passed. */
    public Stock(Long id, String stockSymbol, String stockName, Long statusId, String lastUpdate) {
        mId = id;
        mStockSymbol = stockSymbol;
        mStockName = stockName;
        mStatusId = statusId;
        mLastUpdate = lastUpdate;
    }

    /* Returns the stock Id. */
    public Long getId() {
        return mId;
    }

    /* Set the stock Id. */
    public void setId(Long id) {
        mId = id;
    }

    /* Returns this Stock's symbol. */
    public String getStockSymbol() {
        return mStockSymbol;
    }

    /* Sets this Stock's symbol to the parameter passed. */
    public void setStockSymbol(String stockSymbol) {
        mStockSymbol = stockSymbol;
    }

    /* Returns this Stock's name. */
    public String getStockName() {
        return mStockName;
    }

    /* Sets this Stock's name to the parameter passed. */
    public void setStockName(String stockName) {
        mStockName = stockName;
    }

    /* Returns the Stock's statusId. */
    public Long getStatusId() {
        return mStatusId;
    }

    /* Sets the statusId. */
    public void setStatusId(Long statusId) {
        mStatusId = statusId;
    }

    /* Returns last updated date string. */
    public String getLastUpdate() {
        return mLastUpdate;
    }

    /* Sets the last updated date string. */
    public void setLastUpdate(String lastUpdate) {
        mLastUpdate = lastUpdate;
    }
}
