//
//  StockDbSchema.java
//  StockApp
//
//  Declares the SQLite database schema used in StockApp.
//
//  Created by David McKnight on 2016-04-17.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.model;

public class StockDbSchema {
    public static final int VERSION = 7;
    public static final String DATABASE_NAME = "StockApp.db";

    public static final class WatchedTable {
        public static final String NAME = "watched";
        public static final String SQL_CREATE =
                "create table " + NAME + "( " +
                "_id integer primary key autoincrement, " +
                Cols.SYMBOL + " text unique, " +
                Cols.NAME + " text, " +
                Cols.STATUS_ID + " integer unique, " +
                Cols.LAST_UPDATE + " text, " +
                "foreign key(" + Cols.STATUS_ID + ") " +
                "references " + StatusTable.NAME + "(_id) )";
        public static final String SQL_DELETE =
                "drop table if exists " + NAME;

        public static final class Cols {
            public static final String SYMBOL = "symbol";
            public static final String NAME = "name";
            public static final String STATUS_ID = "status_id";
            public static final String LAST_UPDATE = "last_update";
        }
    }

    public static final class StatusTable {
        public static final String NAME = "status";
        public static final String SQL_CREATE =
                "create table " + NAME + "( " +
                        "_id integer primary key autoincrement, " +
                        Cols.SYMBOL_ID + " integer unique not null, " +
                        Cols.CURRENCY + " text, " +
                        Cols.CHANGE + " text, " +
                        Cols.PERCENT_CHANGE + " text, " +
                        Cols.PREVIOUS_CLOSE + " text, " +
                        Cols.OPEN + " text, " +
                        Cols.BID + " text, " +
                        Cols.ASK + " text, " +
                        Cols.YEAR_TARGET_EST + " text, " +
                        Cols.DAYS_RANGE + " text, " +
                        Cols.YEARS_RANGE + " text, " +
                        Cols.VOLUME + " text, " +
                        Cols.AVG_VOLUME + " text, " +
                        Cols.MARKET_CAP +  " text, " +
                        Cols.LAST_TRADE_DATE + " text, " +
                        Cols.LAST_TRADE_TIME + " text, " +
                        Cols.CHART_DATA + " blob, " +
                        "foreign key(" + Cols.SYMBOL_ID + ") " +
                        "references " + WatchedTable.NAME + "(_id) )";
        public static final String SQL_DELETE =
                "drop table if exists " + NAME;

        public static final class Cols {
            public static final String SYMBOL_ID = "symbol_id";
            public static final String CURRENCY = "currency";
            public static final String CHANGE = "change";
            public static final String PERCENT_CHANGE = "percent_change";
            public static final String PREVIOUS_CLOSE = "prev_close";
            public static final String OPEN = "open";
            public static final String BID = "bid";
            public static final String ASK = "ask";
            public static final String YEAR_TARGET_EST = "year_target_est";
            public static final String DAYS_RANGE = "days_range";
            public static final String YEARS_RANGE = "years_range";
            public static final String VOLUME = "volume";
            public static final String AVG_VOLUME = "avg_volume";
            public static final String MARKET_CAP = "market_cap";
            public static final String LAST_TRADE_DATE = "last_trade_date";
            public static final String LAST_TRADE_TIME = "last_trade_time";
            public static final String CHART_DATA = "chart_data";
        }

    }

    public static final class CompetitorsTable {
        public static final String NAME = "competitors";
        public static final String SQL_CREATE =
                "create table " + NAME + "( " +
                        "_id integer primary key autoincrement, " +
                        Cols.SYMBOL_ID + " integer not null, " +
                        Cols.COMPETITOR_SYMBOL + " text, " +
                        Cols.COMPETITOR_NAME + " text, " +
                        Cols.PERCENT_CHANGE + " text, " +
                        Cols.MARKET_CAP + " text, " +
                        "foreign key(" + Cols.SYMBOL_ID + ") " +
                        "references " + WatchedTable.NAME + "(_id) )";
        public static final String INDEX_CREATE =
                /* Index symbol_id for good update performance. */
                "create index " + NAME + "_" + Cols.SYMBOL_ID +
                        " on " + NAME + "(" + Cols.SYMBOL_ID + ")";
        public static final String SQL_DELETE =
                "drop table if exists " + NAME;
        public static final String INDEX_DELETE =
                "drop index if exists " + NAME + "_" + Cols.SYMBOL_ID;

        public static final class Cols {
            public static final String SYMBOL_ID = "symbol_id";
            public static final String COMPETITOR_SYMBOL = "symbol";
            public static final String COMPETITOR_NAME = "name";
            public static final String PERCENT_CHANGE = "percent_change";
            public static final String MARKET_CAP = "market_cap";
        }
    }
}
