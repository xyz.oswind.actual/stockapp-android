//
//  Status.java
//  StockApp
//
//  An entity which contains stock status data.
//
//  Created by David McKnight on 2016-05-04.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.model.entities;

import java.util.List;

public class Status {
    private Long mStockId; /* The stock SQL id if persisted. */
    /* The attributes of a Status entity. */
    private String mCurrency;
    private String mChange;
    private String mPercentChange;
    private String mPrevClose;
    private String mOpen;
    private String mBid;
    private String mAsk;
    private String mYearTargetEst;
    private String mDaysRange;
    private String mYearsRange;
    private String mVolume;
    private String mAvgVolume;
    private String mMarketCap;
    private String mLastTradeDate;
    private String mLastTradeTime;
    private Chart mChartData;
    private List<Competitor> mCompetitorData;

    /* A default constructor which accepts Status parameters. */
    public Status(Long stockId, String currency, String change,
                  String percentChange, String prevClose,
                  String open, String bid, String ask, String yearTargetEst,
                  String daysRange, String yearsRange, String volume,
                  String avgVolume, String marketCap, String lastTradeDate,
                  String lastTradeTime, Chart chartData,
                  List<Competitor> competitorData) {
        /* Configure attributes. */
        mStockId = stockId;
        mCurrency = currency;
        mChange = change;
        mPercentChange = percentChange;
        mPrevClose = prevClose;
        mOpen = open;
        mBid = bid;
        mAsk = ask;
        mYearTargetEst = yearTargetEst;
        mDaysRange = daysRange;
        mYearsRange = yearsRange;
        mVolume = volume;
        mAvgVolume = avgVolume;
        mMarketCap = marketCap;
        mLastTradeDate = lastTradeDate;
        mLastTradeTime = lastTradeTime;
        mChartData = chartData;
        mCompetitorData = competitorData;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String currency) {
        mCurrency = currency;
    }

    public String getChange() {
        return mChange;
    }

    public void setChange(String change) {
        mChange = change;
    }

    public String getPercentChange() {
        return mPercentChange;
    }

    public void setPercentChange(String percentChange) {
        mPercentChange = percentChange;
    }

    public String getPrevClose() {
        return mPrevClose;
    }

    public void setPrevClose(String prevClose) {
        mPrevClose = prevClose;
    }

    public String getOpen() {
        return mOpen;
    }

    public void setOpen(String open) {
        mOpen = open;
    }

    public String getBid() {
        return mBid;
    }

    public void setBid(String bid) {
        mBid = bid;
    }

    public String getAsk() {
        return mAsk;
    }

    public void setAsk(String ask) {
        mAsk = ask;
    }

    public String getYearTargetEst() {
        return mYearTargetEst;
    }

    public void setYearTargetEst(String yearTargetEst) {
        mYearTargetEst = yearTargetEst;
    }

    public String getDaysRange() {
        return mDaysRange;
    }

    public void setDaysRange(String daysRange) {
        mDaysRange = daysRange;
    }

    public String getYearsRange() {
        return mYearsRange;
    }

    public void setYearsRange(String yearsRange) {
        mYearsRange = yearsRange;
    }

    public String getVolume() {
        return mVolume;
    }

    public void setVolume(String volume) {
        mVolume = volume;
    }

    public String getAvgVolume() {
        return mAvgVolume;
    }

    public void setAvgVolume(String avgVolume) {
        mAvgVolume = avgVolume;
    }

    public String getMarketCap() {
        return mMarketCap;
    }

    public void setMarketCap(String marketCap) {
        mMarketCap = marketCap;
    }

    public String getLastTradeDate() {
        return mLastTradeDate;
    }

    public void setLastTradeDate(String lastTradeDate) {
        mLastTradeDate = lastTradeDate;
    }

    public String getLastTradeTime() {
        return mLastTradeTime;
    }

    public void setLastTradeTime(String lastTradeTime) {
        mLastTradeTime = lastTradeTime;
    }

    /* Returns the stockId of this Status. */
    public Long getStockId() {
        return mStockId;
    }

    /* Sets the stockId of this Status. */
    public void setStockId(Long stockId) {
        mStockId = stockId;
    }

    public byte[] getChartData() {
        return mChartData.getChartData();
    }

    public void setChartData(Chart chartData) {
        mChartData = chartData;
    }

    public List<Competitor> getCompetitorData() {
        return mCompetitorData;
    }

    public void setCompetitorData(List<Competitor> competitorData) {
        mCompetitorData = competitorData;
    }
}
