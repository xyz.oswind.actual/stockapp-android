//
//  StockDbHelperDelegate.java
//  StockApp
//
//  Declares the protocol used when a controller is interested in the result of
//  long-running database operations. Facilitates the running of database
//  operations on a background thread where they may otherwise block the UI
//  thread.
//
//  Created by David McKnight on 2016-04-18.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.model;

import java.util.List;

import xyz.oswind.stockapp.model.entities.Stock;

public interface StockDbHelperDelegate {

    interface Filter {
        /* A filter operation on the watched table has completed. The parameter
         * `filteredStocks` contains the result of filtering stocks. */
        void didCompleteFilterWithResults(List<Stock> filteredStocks);
    }

    interface Status {
        /* The pending Status update operation has now completed. */
        void didCompletePendingStatusOperation();
    }
}
