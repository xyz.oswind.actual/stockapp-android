//
//  StockDbLoader.java
//  StockApp
//
//  A class for managing a StockCursor automatically. An instance will monitor
//  the table provided for changes. Reloads the StockCursor as required for
//  configuration changes or changes in the database. Tasks are performed using
//  background threads and updated StockCursors are posted to the UI thread.
//  This implementation is based on CursorLoader but does not include the
//  content provider related code.
//
//  Created by David McKnight on 2016-04-18.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.CancellationSignal;
import android.os.OperationCanceledException;
import android.support.v4.content.AsyncTaskLoader;

import xyz.oswind.stockapp.model.cursor.StockCursor;

public class StockDbLoader extends AsyncTaskLoader<StockCursor> {
    private final ForceLoadContentObserver mObserver;
    private CancellationSignal mCancellationSignal;
    private StockCursor mStockCursor;
    private final SQLiteDatabase mStockDb;
    private final String mTable;
    private final String mOrderBy;

    /* Default constructor which accepts the context, database, table, and order
     * by clause for cursor loading operations. */
    public StockDbLoader(Context context, SQLiteDatabase db,
                         String table, String orderBy) {
        super(context);
        mObserver = new ForceLoadContentObserver();
        mStockDb = db;
        mTable = table;
        mOrderBy = orderBy;
    }

    @Override
    /* Begin updating the StockCursor on a background thread. */
    public StockCursor loadInBackground() {
        synchronized (this) {
            /* Check if the operation is cancelled most likely due to the loader
             * being reset. */
            if (isLoadInBackgroundCanceled()) {
                throw new OperationCanceledException();
            }
            mCancellationSignal = new CancellationSignal();
        }
        try {
            /* Query the table for the latest contents. */
            Cursor cursor = mStockDb.query(
                    false,  /* Distinct. */
                    mTable,
                    null,   /* Columns to include. */
                    null,   /* Where clause. */
                    null,   /* Where arguments. */
                    null,   /* Group by. */
                    null,   /* Having. */
                    mOrderBy,
                    null,   /* Limit. */
                    mCancellationSignal
            );
            /* Check if the query was successful. */
            if (cursor != null) {
                try {
                    /* Check the cursor window has results. */
                    cursor.getCount();
                    /* Set the content observer. */
                    cursor.registerContentObserver(mObserver);
                } catch (RuntimeException e) {
                    cursor.close(); /* Something went wrong. */
                    throw e;
                }
            }
            return new StockCursor(cursor); /* Return the wrapped cursor. */
        } finally {
            /* Unset the cancellation signal. */
            synchronized (this) {
                mCancellationSignal = null;
            }
        }
    }

    @Override
    /* Cancel load in background operations most likely due to the loader being
     * reset. */
    public void cancelLoadInBackground() {
        super.cancelLoadInBackground();
        synchronized (this) {
            if (mCancellationSignal != null) {
                mCancellationSignal.cancel();
            }
        }
    }

    @Override
    /* Update the current view of the database on the UI thread. */
    public void deliverResult(StockCursor data) {
        /* Check if the loader is reset. */
        if (isReset()) {
            if (data != null) {
                data.close(); /* Close the cursor when reset. */
            }
            return;
        }
        Cursor oldCursor = mStockCursor; /* Swap the cursor in use. */
        mStockCursor = data; /* Now use the latest cursor. */
        /* Check if the loader is started. */
        if (isStarted()) {
            /* Post the latest cursor tp listeners. */
            super.deliverResult(data);
        }
        /* Close the old cursor as required. */
        if (oldCursor != null && oldCursor != data && !oldCursor.isClosed()) {
            oldCursor.close();
        }
    }

    @Override
    /* Start loading cursors asynchronously using the database. */
    protected void onStartLoading() {
        if (mStockCursor != null) {
            deliverResult(mStockCursor);
        }
        if (takeContentChanged() || mStockCursor == null) {
            forceLoad();
        }
    }

    @Override
    /* Cancel loading cursors when the loader is stopped. */
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    /* Once cancelled close the pending cursor. */
    public void onCanceled(StockCursor data) {
        if (data != null && !data.isClosed()) {
            data.close();
        }
    }

    @Override
    /* When reset stop loading cursors and close the current cursor. */
    protected void onReset() {
        super.onReset();
        onStopLoading();
        if (mStockCursor != null && !mStockCursor.isClosed()) {
            mStockCursor.close();
        }
        mStockCursor = null;
    }
}