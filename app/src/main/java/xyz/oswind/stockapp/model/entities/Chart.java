//
//  Chart.java
//  StockApp
//
//  An entity which contains chart data.
//
//  Created by David McKnight on 2016-05-04.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.model.entities;

public class Chart {
    private byte[] mChartData; /* The chart data. */

    /* A default constructor accepting chart data. */
    public Chart(byte[] data) {
        mChartData = data;
    }

    /* Returns the chart data. */
    public byte[] getChartData() {
        return mChartData;
    }

    /* Sets the chart data. */
    public void setChartData(byte[] chartData) {
        mChartData = chartData;
    }
}
