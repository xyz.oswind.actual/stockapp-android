//
//  StockCursor.java
//  StockApp
//
//  A wrapper class for convenient access to the contents of a cursor. Enables
//  the easy extraction of Stock object from the database.
//
//  Created by David McKnight on 2016-04-17.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.model.cursor;

import android.database.Cursor;
import android.provider.BaseColumns;

import xyz.oswind.stockapp.model.entities.Stock;

import static xyz.oswind.stockapp.model.StockDbSchema.*;

public class StockCursor extends StockDbCursor {
    /* A default constructor which wraps a cursor containing stocks. */
    public StockCursor(Cursor cursor) {
        super(cursor);
    }

    /* Extract a Stock from the cursor. Note that the cursor must have been
     * positioned at the row containing the stock entity of interest. Returns
     * a Stock object corresponding to the current cursor row. */
    public Stock getStock() {
        /* Extract the primary key. */
        Long id = getLong(getColumnIndex(BaseColumns._ID));
        /* Extract the stock symbol. */
        String symbol = getString(getColumnIndex(WatchedTable.Cols.SYMBOL));
        /* Extract the stock name. */
        String name = getString(getColumnIndex(WatchedTable.Cols.NAME));
        /* Extract the stock status_id if exists or null. */
        Long statusId = null;
        if (!isNull(getColumnIndex(WatchedTable.Cols.STATUS_ID))) {
            statusId = getLong(getColumnIndex(WatchedTable.Cols.STATUS_ID));
        }
        /* Extract the date of status_id if exists or null. */
        String lastUpdate = null;
        if (!isNull(getColumnIndex(WatchedTable.Cols.LAST_UPDATE))) {
            lastUpdate = getString(getColumnIndex(WatchedTable.Cols.LAST_UPDATE));
        }
        /* Return the initialized Stock object. */
        return new Stock(id, symbol, name, statusId, lastUpdate);
    }
}
