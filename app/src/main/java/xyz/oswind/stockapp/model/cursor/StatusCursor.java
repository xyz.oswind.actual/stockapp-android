//
//  StatusCursor.java
//  StockApp
//
//  A wrapper class for convenient access to the contents of a cursor. Enables
//  the easy extraction of Status object from the database.
//
//  Created by David McKnight on 2016-05-04.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.model.cursor;

import android.database.Cursor;

import xyz.oswind.stockapp.model.entities.Chart;
import xyz.oswind.stockapp.model.entities.Status;

import static xyz.oswind.stockapp.model.StockDbSchema.*;

public class StatusCursor extends StockDbCursor {
    /* A default constructor which wraps a status data cursor. */
    public StatusCursor(Cursor cursor) {
        super(cursor);
    }

    /* Extract a Status from the cursor. Note that the cursor must have been
     * positioned at the row containing the status entity of interest. Returns
     * a Status object corresponding to the current cursor row. */
    public Status getStatus() {
        Long stockId = getLong(getColumnIndex(StatusTable.Cols.SYMBOL_ID));
        String currency = getString(getColumnIndex(StatusTable.Cols.CURRENCY));
        String change = getString(getColumnIndex(StatusTable.Cols.CHANGE));
        String percentChg = getString(
                getColumnIndex(StatusTable.Cols.PERCENT_CHANGE));
        String prevClose = getString(
                getColumnIndex(StatusTable.Cols.PREVIOUS_CLOSE));
        String open = getString(
                getColumnIndex(StatusTable.Cols.OPEN));
        String bid = getString(
                getColumnIndex(StatusTable.Cols.BID));
        String ask = getString(
                getColumnIndex(StatusTable.Cols.ASK));
        String yearTargetEst = getString(
                getColumnIndex(StatusTable.Cols.YEAR_TARGET_EST));
        String daysRange = getString(
                getColumnIndex(StatusTable.Cols.DAYS_RANGE));
        String yearsRange = getString(
                getColumnIndex(StatusTable.Cols.YEARS_RANGE));
        String volume = getString(
                getColumnIndex(StatusTable.Cols.VOLUME));
        String avgVolume = getString(
                getColumnIndex(StatusTable.Cols.AVG_VOLUME));
        String marketCap = getString(
                getColumnIndex(StatusTable.Cols.MARKET_CAP));
        String lastTradeDate = getString(
                getColumnIndex(StatusTable.Cols.LAST_TRADE_DATE));
        String lastTradeTime = getString(
                getColumnIndex(StatusTable.Cols.LAST_TRADE_TIME));
        Chart chart = null;
        if(!isNull(getColumnIndex(StatusTable.Cols.CHART_DATA))) {
            chart = new Chart(getBlob(
                    getColumnIndex(StatusTable.Cols.CHART_DATA)));
        }
        /* Return the initialized Status object. */
        return new Status(stockId, currency, change, percentChg, prevClose,
                open, bid, ask, yearTargetEst, daysRange, yearsRange, volume,
                avgVolume, marketCap, lastTradeDate, lastTradeTime,  chart,
                null);
    }
}
