//
//  StockDbHelper.java
//  StockApp
//
//  A helper class for managing access to the StockApp database. An instance
//  acts as a singleton which can be retrieved application-wide. Provides a
//  application shared interface for database operations.
//
//  Created by David McKnight on 2016-04-17.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.provider.BaseColumns;
import android.support.v4.content.Loader;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import xyz.oswind.stockapp.model.StockDbSchema.CompetitorsTable;
import xyz.oswind.stockapp.model.StockDbSchema.StatusTable;
import xyz.oswind.stockapp.model.cursor.CompetitorCursor;
import xyz.oswind.stockapp.model.cursor.StatusCursor;
import xyz.oswind.stockapp.model.cursor.StockCursor;
import xyz.oswind.stockapp.model.entities.Competitor;
import xyz.oswind.stockapp.model.entities.Status;
import xyz.oswind.stockapp.model.entities.Stock;

import static xyz.oswind.stockapp.model.StockDbSchema.DATABASE_NAME;
import static xyz.oswind.stockapp.model.StockDbSchema.VERSION;
import static xyz.oswind.stockapp.model.StockDbSchema.WatchedTable;

@SuppressWarnings("TryFinallyCanBeTryWithResources")
public class StockDbHelper extends SQLiteOpenHelper {
    private static final String TAG = "StockDbHelper";
    private static StockDbHelper sStockDbHelper; /* App static StockDbHelper. */
    private static Context sContext; /* Static in the app's context.*/
    private final SQLiteDatabase mStockDb; /* The StockApp database. */
    private StockDbLoader mWatchedStockLoader; /* A watched stock loader. */
    private StockDbHelperDelegate.Filter mFilterDelegate; /* Watched filter. */
    private StockDbHelperDelegate.Status mStatusDelegate; /* Status updates. */
    private StockDbFilter mFilterTask; /* Task for the watched table filter. */
    private StatusUpdate mUpdateTask;  /* Task status updates. */
    /* Indicate if the last status submitted has been saved. */
    private volatile boolean mDidCompleteSubmitStatus;
    private Long mLastStock; /* The stock of the last status update. */

    /* Static initializer for a StockDbHelper singleton. */
    public static StockDbHelper get(Context context) {
        /* Check if the StockDbHelper already exists. */
        if (sStockDbHelper == null) {
            /* Use the application-shared context for creating the singleton. */
            sContext = context.getApplicationContext();
            /* Create the StockDbHelper. */
            sStockDbHelper = new StockDbHelper();
        }
        /* Return the application-shared StockDbHelper. */
        return sStockDbHelper;
    }

    /* A default private constructor for creating a StockDbHelper singleton. */
    private StockDbHelper() {
        /* Initialize the SQLiteOpenHelper. */
        super(sContext, DATABASE_NAME, null, VERSION);
        mStockDb = getWritableDatabase(); /* Request write access. */
        mDidCompleteSubmitStatus = false;
        mLastStock = null;
    }

    /* Check if pending database operations have completed. */
    public boolean isCompletedPendingUpdates() {
        /* For now just return if the last status update completed. */
        return (mLastStock != null) && mDidCompleteSubmitStatus;
    }

    /* Reset the StockDbHelper's history of status updates. */
    public void resetForNextStatus(Stock stock) {
        /* Check if the stock is different from the last status update. */
        if ((mLastStock != null) && !mLastStock.equals(stock.getId())) {
            /* Reset status completed indicators. */
            mDidCompleteSubmitStatus = false;
        }
    }

    /* Set the delegate when performing potentially long-running filter tasks.
     * The delegate will be notified when the tasks have completed. This
     * facilitates performing database operations on a background thread. */
    public void setFilterDelegate(StockDbHelperDelegate.Filter delegate) {
        mFilterDelegate = delegate;
    }

    /* Set the delegate when performing potentially long-running update tasks.
     * The delegate will be notified when the tasks have completed. This
     * facilitates performing database operations on a background thread. */
    public void setStatusDelegate(StockDbHelperDelegate.Status delegate) {
        mStatusDelegate = delegate;
    }

    /* Request the loader used for monitoring the watched table for changes. The
     * loader manages a cursor into watched table contents and operates on a
     * background thread. */
    public Loader<StockCursor> getWatchedStockLoader() {
        /* Check if the loader already exists. */
        if (mWatchedStockLoader == null) {
            /* Create the loader. */
            mWatchedStockLoader = new StockDbLoader(sContext,
                    mStockDb, WatchedTable.NAME, WatchedTable.Cols.SYMBOL);
        }
        return mWatchedStockLoader;
    }

    @Override
    /* Override point for handling schema creation. */
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(WatchedTable.SQL_CREATE); /* Create watched table. */
        db.execSQL(StatusTable.SQL_CREATE);  /* Create status table. */
        db.execSQL(CompetitorsTable.SQL_CREATE); /* Create competitors table. */
        db.execSQL(CompetitorsTable.INDEX_CREATE); /* Add manual indexes. */
    }

    @Override
    /* Override point for handling database schema upgrades. For now this does
     * not perform any task. Simply delete the tables and recreate them. */
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(CompetitorsTable.INDEX_DELETE); /* Delete manual indexes. */
        db.execSQL(CompetitorsTable.SQL_DELETE); /* Drop competitors table. */
        db.execSQL(StatusTable.SQL_DELETE);  /* Drop status table. */
        db.execSQL(WatchedTable.SQL_DELETE); /* Drop watched table. */
        onCreate(db); /* Recreate schema. */
    }

    /* Add a new stock to the watched table using the parameter `stock`. */
    public void addWatched(Stock stock) {
        /* Request the stock wrapped in a content values. */
        ContentValues values = contentValuesForStock(stock);
        /* Add the stock to the watched table. */
        mStockDb.insertWithOnConflict(WatchedTable.NAME, null, values,
                SQLiteDatabase.CONFLICT_IGNORE);
        /* Notify the loader of a content change. */
        mWatchedStockLoader.onContentChanged();
    }

    /* Delete a stock from the watched table using the parameter `stock`. */
    public void deleteWatched(Stock stock) {
        deleteCompetitorsForSymbolId(stock.getId()); /* Cleanup competitors. */
        deleteStatusForSymbolId(stock.getId());      /* Cleanup status. */
        /* Delete the stock from the watched table. */
        mStockDb.delete(
                WatchedTable.NAME,
                BaseColumns._ID + " = ?",
                new String[]{stock.getId().toString()});
        /* Notify the loader of a content change. */
        mWatchedStockLoader.onContentChanged();
    }

    /* Submit a filter query to the database. Filter delegates are notified of
     * filter operation completion. */
    public void submitFilterToWatched(String filter) {
        /* Guard against a missing delegate or empty filter string. */
        if (mFilterDelegate != null && filter != null && !filter.equals("")) {
            /* Reset any pending filter task that has not completed. */
            if (mFilterTask != null &&
                    mFilterTask.getStatus() == AsyncTask.Status.RUNNING) {
                /* Cancel should be okay since the filter operation requires
                 * only database reading. */
                mFilterTask.cancel(true);
            }
            /* Request a new filter operation on the database. */
            mFilterTask = new StockDbFilter(filter);
        }
    }

    /* Submit a Status to save in the StockApp database. Status delegates are
     * notified of status update operations completed. */
    public void submitStatus(Status status) {
        mLastStock = status.getStockId(); /* Update StockDbHelper history. */
        mDidCompleteSubmitStatus = false; /* Reset completion indicator. */
        if (mUpdateTask != null) {
            mUpdateTask.cancel(false);
        }
        /* Perform the Status update on a background thread. */
        mUpdateTask = new StatusUpdate(status);
    }

    /* Add a Status to the the StockApp database. This version is private to
     * ensure the method is called in a StatusUpdate task. */
    private void addStatus(Status status) {
         /* If a status is being updated delete competitor data. */
        deleteCompetitorsForSymbolId(status.getStockId());
        /* Request the status wrapped in a content values. */
        ContentValues statusValues = contentValuesForStatus(status);
        /* Create or update the status after competitor cleanup. */
        mStockDb.insertWithOnConflict(StatusTable.NAME, null, statusValues,
                SQLiteDatabase.CONFLICT_REPLACE);
        /* Update the statusId of the stock in watched table. */
        StatusCursor statuses = queryStatus( /* Need the status id. */
                StatusTable.Cols.SYMBOL_ID + " = ?",
                new String[]{status.getStockId().toString()});
        try {
            statuses.moveToFirst(); /* Request the status entity. */
            Stock stock = getStockWithSymbolId(status.getStockId());
            stock.setStatusId(statuses.getId()); /* Update statusId. */
            /* ISO8601 type date string for last_update. */
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            stock.setLastUpdate(dateFormat.format(new Date()));
            /* Request the updated stock wrapped in a content values. */
            ContentValues stockValues = contentValuesForStock(stock);
            mStockDb.updateWithOnConflict(WatchedTable.NAME, stockValues,
                    BaseColumns._ID + " = ?",
                    new String[]{status.getStockId().toString()},
                    SQLiteDatabase.CONFLICT_REPLACE);
            /* Notify the watched stock loader of a content change. */
            mWatchedStockLoader.onContentChanged();
        } finally {
            statuses.close();
        }
        /* Add the competitors at this status to the database. */
        if (status.getCompetitorData() != null) {
            for (Competitor competitor: status.getCompetitorData()) {
                competitor.setStockId(status.getStockId()); /* Set symbolId. */
                addCompetitor(competitor);
            }
        }
    }

    /* Request competitor data using the Id of a watched stock. */
    public List<Competitor> getCompetitorsForSymbolId(Long symbolId) {
        /* Request competitor data that matches the symbolId. */
        CompetitorCursor competitorCursor = queryCompetitors(
                CompetitorsTable.Cols.SYMBOL_ID + " = ?",
                new String[]{symbolId.toString()});
        /* Create a list to contain the extracted entities. */
        List<Competitor> competitors = new ArrayList<>();
        try {
            /* Move the cursor to the first row. */
            competitorCursor.moveToFirst();
            /* Loop while not at the end of the cursor. */
            while (!competitorCursor.isAfterLast()) {
                /* Extract the stock and add it to the list of results. */
                competitors.add(competitorCursor.getCompetitor());
                /* Re-position the cursor at the next row. */
                competitorCursor.moveToNext();
            }
        } finally {
            competitorCursor.close(); /* Close the cursor. */
        }
        /* Return the list of extracted stocks. */
        return competitors;
    }

    /* Request stock data using the watched stock Id. */
    public Stock getStockWithSymbolId(Long symbolId) {
        /* Request the matching stock data. */
        StockCursor stockCursor = queryWatched(
                BaseColumns._ID + " = ?",
                new String[]{symbolId.toString()},
                null);
        try {
            if (stockCursor.getCount() == 0) {
                return null;
            }
            stockCursor.moveToFirst();
            return stockCursor.getStock();
        } finally {
            stockCursor.close();
        }
    }

    /* Request the stock status data using the Id of a watched stock. */
    public Status getStatusForSymbolId(Long symbolId) {
        /* Request the matching status data. */
        StatusCursor statusCursor = queryStatus(
                StatusTable.Cols.SYMBOL_ID + " = ?",
                new String[]{symbolId.toString()});
        try {
            if (statusCursor.getCount() == 0) {
                return null;
            }
            statusCursor.moveToFirst();
            return statusCursor.getStatus();
        } finally {
            statusCursor.close();
        }
    }

    /* Request the chart data using the Id of a watched stock. */
    public byte[] getChartDataForSymbolId(Long symbolId) {
        /* Request the matching status data. */
        StatusCursor statusCursor = queryStatus(
                StatusTable.Cols.SYMBOL_ID + " = ?",
                new String[]{symbolId.toString()});
        try {
            if (statusCursor.getCount() == 0) {
                return null;
            }
            statusCursor.moveToFirst();
            return statusCursor.getStatus().getChartData();
        } finally {
            statusCursor.close();
        }
    }

    /* Add a competitor to the StockApp database. This is private to ensure the
     * method is called in a StatusUpdate task. */
    private void addCompetitor(Competitor competitor) {
        /* Request the competitor wrapped in a content values. */
        ContentValues values = contentValuesForCompetitor(competitor);
        /* Add the competitor to the competitors table. */
        mStockDb.insert(CompetitorsTable.NAME, null, values);
    }

    /* Delete the status data using the Id of a watched stock. */
    private void deleteStatusForSymbolId(Long symbolId) {
        /* Delete the matching status from the status table. */
        mStockDb.delete(
                StatusTable.NAME,
                StatusTable.Cols.SYMBOL_ID + " = ?",
                new String[]{symbolId.toString()});
    }

    /* Delete the competitor data using the Id of a watched stock. */
    private void deleteCompetitorsForSymbolId(Long symbolId) {
        /* Delete the matching entries from the competitors table. */
        mStockDb.delete(
                CompetitorsTable.NAME,
                CompetitorsTable.Cols.SYMBOL_ID + " = ?",
                new String[]{symbolId.toString()});
    }

    /* Perform a filter operation on the watched table using the parameter
     * `filter`. The filter operation is performed using the SQL operator LIKE
     * with the parameter `filter` as operand. Returns a list containing the
     * filtered stocks. Note this operation is performed on the calling thread.
     * This should be used in a background thread as the LIKE operator with
     * operand %operand% can be slow. */
    private List<Stock> queryWatchedUsingFilter(String filter) {
        /* Bind the parameters for each of SYMBOL and NAME. */
        String symbolLike = WatchedTable.Cols.SYMBOL + " like ?";
        String nameLike = WatchedTable.Cols.NAME + " like ?";
        /* The LIKE operator will search in SYMBOL or NAME. */
        String likeOperator = symbolLike + " or " + nameLike;
        /* The operand is the parameter `filter` substring. */
        String likeOperand = "%" + filter + "%";
        /* Query the watched table for stock entities containing the string
         * `filter` in either SYMBOL or NAME. Order the results by SYMBOL. */
        StockCursor filteredStocks = queryWatched(
                likeOperator,
                new String[] { likeOperand, likeOperand },
                WatchedTable.Cols.SYMBOL);
        /* Return a list containing filtered stocks. */
        return getWatchedStocksList(filteredStocks);
    }

    /* Extract Stock objects from the stockCursor parameter. Returns a list
     * containing the extracted stocks. */
    private List<Stock> getWatchedStocksList(StockCursor stockCursor) {
        /* Create a list to contain the extracted stocks. */
        List<Stock> watched = new ArrayList<>();
        try {
            /* Move the cursor to the first row. */
            stockCursor.moveToFirst();
            /* Loop while not at the end of the cursor. */
            while (!stockCursor.isAfterLast()) {
                /* Extract the stock and add it to the list of results. */
                watched.add(stockCursor.getStock());
                /* Re-position the cursor at the next row. */
                stockCursor.moveToNext();
            }
        } finally {
            stockCursor.close(); /* Close the cursor. */
        }
        /* Return the list of extracted stocks. */
        return watched;
    }

    /* Wrap the stock parameter provided in a ContentValues instance for use
     * with SQLiteOpenHelper insert methods. */
    private ContentValues contentValuesForStock(Stock stock) {
        ContentValues values = new ContentValues();
        /* Add the stock symbol and name. */
        values.put(WatchedTable.Cols.SYMBOL, stock.getStockSymbol());
        values.put(WatchedTable.Cols.NAME, stock.getStockName());
        if (stock.getStatusId() == null) {
            values.putNull(WatchedTable.Cols.STATUS_ID);
        } else {
            values.put(WatchedTable.Cols.STATUS_ID, stock.getStatusId());
        }
        if (stock.getLastUpdate() == null) {
            values.putNull(WatchedTable.Cols.LAST_UPDATE);
        } else {
            values.put(WatchedTable.Cols.LAST_UPDATE, stock.getLastUpdate());
        }
        /* Return the ContentValues containing the stock parameter. */
        return values;
    }

    /* Wrap the status parameter provided in a ContentValues instance for use
     * with SQLiteOpenHelper insert methods. */
    private ContentValues contentValuesForStatus(Status status) {
        ContentValues values = new ContentValues();
        /* Add the status data. */
        values.put(StatusTable.Cols.SYMBOL_ID, status.getStockId());
        values.put(StatusTable.Cols.CURRENCY, status.getCurrency());
        values.put(StatusTable.Cols.CHANGE, status.getChange());
        values.put(StatusTable.Cols.PERCENT_CHANGE, status.getPercentChange());
        values.put(StatusTable.Cols.PREVIOUS_CLOSE, status.getPrevClose());
        values.put(StatusTable.Cols.OPEN, status.getOpen());
        values.put(StatusTable.Cols.BID, status.getBid());
        values.put(StatusTable.Cols.ASK, status.getAsk());
        values.put(StatusTable.Cols.YEAR_TARGET_EST, status.getYearTargetEst());
        values.put(StatusTable.Cols.DAYS_RANGE, status.getDaysRange());
        values.put(StatusTable.Cols.YEARS_RANGE, status.getYearsRange());
        values.put(StatusTable.Cols.VOLUME, status.getVolume());
        values.put(StatusTable.Cols.AVG_VOLUME, status.getAvgVolume());
        values.put(StatusTable.Cols.MARKET_CAP, status.getMarketCap());
        values.put(StatusTable.Cols.LAST_TRADE_DATE, status.getLastTradeDate());
        values.put(StatusTable.Cols.LAST_TRADE_TIME, status.getLastTradeTime());
        if (status.getChartData() == null) {
            values.putNull(StatusTable.Cols.CHART_DATA);
        } else {
            values.put(StatusTable.Cols.CHART_DATA, status.getChartData());
        }
        /* Return the ContentValues containing the status parameter. */
        return values;
    }

    /* Wrap the status parameter provided in a ContentValues instance for use
     * with SQLiteOpenHelper insert methods. */
    private ContentValues contentValuesForCompetitor(Competitor competitor) {
        ContentValues values = new ContentValues();
        /* Add the competitor data. */
        values.put(CompetitorsTable.Cols.SYMBOL_ID, competitor.getStockId());
        values.put(CompetitorsTable.Cols.COMPETITOR_SYMBOL, competitor.getSymbol());
        values.put(CompetitorsTable.Cols.COMPETITOR_NAME, competitor.getName());
        values.put(CompetitorsTable.Cols.PERCENT_CHANGE, competitor.getPercentChange());
        values.put(CompetitorsTable.Cols.MARKET_CAP, competitor.getMarketCap());
        return values;
    }

    /* Query watched table using the parameters for where and order-by. */
    private StockCursor queryWatched(String whereClause, String[] whereArgs,
                                     String orderBy) {
        return new StockCursor(queryStockDb(
                WatchedTable.NAME, whereClause, whereArgs, orderBy));
    }

    /* Query status table using the parameters for where and order-by. */
    private StatusCursor queryStatus(String whereClause, String[] whereArgs) {
        return new StatusCursor(queryStockDb(
                StatusTable.NAME, whereClause, whereArgs, null));
    }

    /* Query competitors table using the parameters for where and order-by. */
    private CompetitorCursor queryCompetitors(String whereClause,
                                              String[] whereArgs) {
        return new CompetitorCursor(queryStockDb(
                CompetitorsTable.NAME, whereClause, whereArgs, null));
    }


    /* Query a table in StockApp's database with where and order-by. */
    private Cursor queryStockDb(String table, String whereClause,
                                String[] whereArgs, String orderBy) {
        /* Perform the select statement. */
       return mStockDb.query(
                table,/* StockDb table name. */
                null, /* Columns to include. Return all columns. */
                whereClause,
                whereArgs,
                null, /* groupBy */
                null, /* having */
                orderBy);
    }

    /* An implementation of AsyncTask which will perform filtering of watched
     * stocks using the SQL operator LIKE with operand `%filter%`. This type of
     * database operation can be slow so it is performed on a background thread.
     * A Filter delegate is notified of completion. */
    private class StockDbFilter extends AsyncTask<String,Void,List<Stock>> {
        /* A default constructor which performs a filter operation. */
        public StockDbFilter(String filterString) {
            /* Support parallel execution for performance. */
            executeOnExecutor(THREAD_POOL_EXECUTOR, filterString);
        }

        @Override
        /* Perform a filter operation on a background thread. Requires a single
         * string parameter containing a (sub)string to filter results. Returns
         * a filtered list of stocks which contain the filter string passed as
         * a parameter. */
        protected List<Stock> doInBackground(String... params) {
            if (params.length != 1) {
                throw new IllegalArgumentException(
                        "A filter was executed with an invalid parameter. " +
                        "Provide a single filter string parameter.");
            }
            /* Use the filter parameter passed during instance creation. */
            String filterString = params[0];
            /* Perform the filter operation and return a list of stocks. */
            return queryWatchedUsingFilter(filterString);
        }

        @Override
        /* Notify a delegate of the filter task completion. This method will run
         * on the UI thread automatically. The delegate must have implemented
         * the StockDbHelperDelegate protocol and be set in StockDbHelper. */
        protected void onPostExecute(List<Stock> filteredStocks) {
            super.onPostExecute(filteredStocks);
            /* Notify the delegate and provide the filtered results. */
            if (mFilterDelegate != null) {
                mFilterDelegate.didCompleteFilterWithResults(filteredStocks);
            }
        }
    }

    /* An implementation of AsyncTask which will perform status updates. This
     * might include cleaning up existing status updates. A Status delegate will
     * be notified of completion. */
    private class StatusUpdate extends AsyncTask<Status,Void,Void> {
        /* A default constructor which performs a status update. */
        public StatusUpdate(xyz.oswind.stockapp.model.entities.Status status) {
            /* Support parallel execution for performance. */
            executeOnExecutor(THREAD_POOL_EXECUTOR, status);
        }

        @Override
        /* Run the status update on a background thread. */
        protected Void doInBackground(
                xyz.oswind.stockapp.model.entities.Status... params) {
            addStatus(params[0]); /* Begin status update. */
            return null;
        }

        @Override
        /* Notify delegates and update completion indicator on ui thread. */
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            /* Notify the delegate of update completion. */
            if (mStatusDelegate != null) {
                mStatusDelegate.didCompletePendingStatusOperation();
            }
            mDidCompleteSubmitStatus = true; /* Update completion indicator. */
        }
    }
}
