//
//  Competitor.java
//  StockApp
//
//  An entity which contains competitor data.
//
//  Created by David McKnight on 2016-05-04.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.model.entities;

public class Competitor {
    private Long mStockId;  /* The stock SQL id if persisted. */
    private String mSymbol; /* The competitor symbol. */
    private String mName;   /* The competitor name. */
    private String mPercentChange; /* The competitor percent change. */
    private String mMarketCap;     /* The competitor market cap. */

    /* A default constructor accepting competitor attributes. */
    public Competitor(Long stockId, String symbol, String name,
                      String percentChange, String marketCap) {
        mStockId = stockId;
        mSymbol = symbol;
        mName = name;
        mPercentChange = percentChange;
        mMarketCap = marketCap;
    }

    /* Returns the stockId. */
    public Long getStockId() {
        return mStockId;
    }

    /* Sets the stockId. */
    public void setStockId(Long stockId) {
        mStockId = stockId;
    }

    /* Returns the competitor percent change. */
    public String getPercentChange() {
        return mPercentChange;
    }

    /* Returns the competitor symbol. */
    public String getSymbol() {
        return mSymbol;
    }

    /* Sets the competitor symbol. */
    public void setSymbol(String symbol) {
        mSymbol = symbol;
    }

    /* Returns the competitor name. */
    public String getName() {
        return mName;
    }

    /* Sets the competitor name. */
    public void setName(String name) {
        mName = name;
    }

    /* Sets the competitor percent change. */
    public void setPercentChange(String percentChange) {
        mPercentChange = percentChange;
    }

    /* Returns the competitor market cap. */
    public String getMarketCap() {
        return mMarketCap;
    }

    /* Sets the competitor market cap. */
    public void setMarketCap(String marketCap) {
        mMarketCap = marketCap;
    }
}
