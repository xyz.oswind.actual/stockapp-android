//
//  StockStatusFragment.java
//  StockApp
//
//  A tab fragment containing a stock's status data if available.
//
//  Created by David McKnight on 2016-05-01.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.controller.detail;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import xyz.oswind.stockapp.R;
import xyz.oswind.stockapp.Reachability;
import xyz.oswind.stockapp.TimedProgress;
import xyz.oswind.stockapp.contentprovider.status.StockStatusDelegate;
import xyz.oswind.stockapp.model.StockDbHelper;
import xyz.oswind.stockapp.model.StockDbHelperDelegate;
import xyz.oswind.stockapp.model.entities.Status;

public class StockStatusFragment extends StockDetailFragment
        implements StockStatusDelegate, StockDbHelperDelegate.Status,
        Reachability.Callbacks {
    private static final String TAG = "StockStatusFragment";
    /* The time to show activity after view updates complete. */
    private static final long PAD_UPDATE_COMPLETE = 1500;
    /* The lockout time to prevent update button abuse. */
    private static final long UPDATE_DELAY_MS = 3000;
    /* Identifier for the state of the update button. This is used to bypass the
     * normal lockout during view creation. */
    private static final String UPDATE_STATE = "xyz.oswind.stockapp.update";
    private Button mUpdateButton; /* Button to request status update. */
    /* Text views containing the status data. */
    private TextView mCurrency, mChange, mPctChange, mPrvClose, mOpen, mBid,
            mAsk, mYrTgtEst, mDayRange, mYrRange, mVolume, mAvgVolume, mMktCap,
            mLastDateTime;
    /* Image views containing the change indicator arrows. */
    private ImageView mChangeArrow, mPctChangeArrow;
    private volatile ProgressBar mUpdateIndicator; /* Indicate updating. */
    private WakeUpdateAfterDelay mWake; /* The update button lockout. */
    private boolean mStatusDidSave; /* The last status requested was saved. */

    @LayoutRes
    @Override
    /* A tab containing a stock's status. */
    protected int getLayoutResId() {
        return R.layout.fragment_stock_status;
    }

    @Override
    /* A reachability broadcast has been received. */
    public void onReachabilityUpdate() {
        if (mUpdateButton != null) {
            /* Update the button text and state based on reachability. */
            updateViewWithReachability(
                    /* Check the network state. */
                    Reachability.get(getActivity()).isNetworkOnline());
        }
    }

    /* Update the view depending on the parameter `isOnline`. Show a change in
     * the view to indicate if search functionality is available. */
    private void updateViewWithReachability(boolean isOnline) {
        if (!isOnline) {
            /* Network offline. Disable button and update displayed text. */
            mUpdateButton.setText(R.string.update_button_offline);
            mUpdateButton.setEnabled(false);
        } else {
            /* Network online. Enable button and update displayed text. */
            mUpdateButton.setText(R.string.update_button);
            resetWakeUpdateTimer(); /* Default lockout applies. */
        }
    }

    @Override
    /* Override point for when the retained fragment is created. */
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /* Use Status updates to refresh this fragment's view. */
        mStatusProvider.setDelegate(this);
        /* This fragment will confirm database updates. */
        StockDbHelper.get(getActivity()).setStatusDelegate(this);
        /* Reset DbHelper operation indicators when the stock in this status
         * fragment is not the same as the last update operation. */
        StockDbHelper.get(getActivity()).resetForNextStatus(mStock);
        /* Start observing reachability events. */
        Reachability.get(getActivity()).addObserver(this);
        /* Set the StockStatus to save automatically. */
        mStatusProvider.setStockId(mStock.getId()); /* Configures save. */
        mStatusDidSave = true; /* Last update operation saved. */
    }

    @Override
    /* Override point for destroying a fragment. */
    public void onDestroy() {
        super.onDestroy();
        cancelWake(); /* Update button wake not needed. */
        /* No longer handling database status updates. */
        StockDbHelper.get(getActivity()).setStatusDelegate(null);
        mStatusProvider.setDelegate(null); /* Not handling status updates. */
        /* Stop observing reachability events. */
        Reachability.get(getActivity()).removeObserver(this);
    }

    @Override
    /* Override point for when creating the menu. */
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        /* Create a menu using the menu resource. */
        inflater.inflate(R.menu.menu_stock_status, menu);
        /* Show an icon when the last status was saved. */
        MenuItem saved = menu.findItem(R.id.menu_item_icon_saved);
        saved.setVisible(StockDbHelper.get(getActivity())
                .isCompletedPendingUpdates() && mStatusDidSave);
        /* Show an icon when using a cached status. */
        MenuItem cached = menu.findItem(R.id.menu_item_icon_cached);
        cached.setVisible(mStatusProvider.getStockStatus() != null);
    }

    @Override
    /* Override point for handling selected menu items. */
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_item_icon_saved) {
            /* Show the same text as the icon long-press. Status is saved. */
            Toast msg = Toast.makeText(getActivity(),
                    R.string.icon_saved, Toast.LENGTH_SHORT);
            msg.setGravity(Gravity.TOP|Gravity.RIGHT, 0, 60);
            msg.show();
        } else if (item.getItemId() == R.id.menu_item_icon_cached) {
            /* Show the same text as the icon long-press. Status is cached. */
            Toast msg = Toast.makeText(getActivity(),
                    R.string.icon_cached, Toast.LENGTH_SHORT);
            msg.setGravity(Gravity.TOP|Gravity.RIGHT, 0, 60);
            msg.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    /* Override point for when the fragment view state is being stashed. */
    public void onSaveInstanceState(Bundle outState) {
        /* When restoring view state also set the update button state. */
        outState.putBoolean(UPDATE_STATE, mUpdateButton.isEnabled());
        super.onSaveInstanceState(outState);
    }

    @Override
    /* Override point for when the fragment's view is created. */
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View statusView = super.onCreateView( /* See StockDetailFragment. */
                /* Apply common tab properties. */
                inflater, container, savedInstanceState);
        /* Cache the update indicator view. */
        assert statusView != null;
        mUpdateIndicator = (ProgressBar)statusView.findViewById(R.id.fragment_stock_status_active);
        /* Set the status name text view to the stock name. */
        TextView nameView = (TextView)statusView.findViewById(R.id.fragment_stock_status_name);
        nameView.setText(mStock.getStockName()); /* Use the stock name. */
        /* Cache the update button view. */
        mUpdateButton = (Button)statusView.findViewById(R.id.fragment_stock_status_button_update);
        /* Check the network and immediately update view state if offline. */
        if (!Reachability.get(getActivity()).isNetworkOnline()) {
            updateViewWithReachability(false); /* No network found. */
        }
        /* Cache the view Ids of the stock's status attributes. */
        mChangeArrow = (ImageView)statusView.findViewById(R.id.fragment_stock_status_change_arrow);
        mPctChangeArrow = (ImageView)statusView.findViewById(R.id.fragment_stock_status_pct_chg_arrow);
        mCurrency = (TextView)statusView.findViewById(R.id.fragment_stock_status_currency);
        mChange = (TextView)statusView.findViewById(R.id.fragment_stock_status_change);
        mPctChange = (TextView)statusView.findViewById(R.id.fragment_stock_status_pct_change);
        mPrvClose = (TextView)statusView.findViewById(R.id.fragment_stock_status_prv_close);
        mOpen = (TextView)statusView.findViewById(R.id.fragment_stock_status_open);
        mBid = (TextView)statusView.findViewById(R.id.fragment_stock_status_bid);
        mAsk = (TextView)statusView.findViewById(R.id.fragment_stock_status_ask);
        mYrTgtEst = (TextView)statusView.findViewById(R.id.fragment_stock_status_yr_tgt_est);
        mDayRange = (TextView)statusView.findViewById(R.id.fragment_stock_status_day_range);
        mYrRange = (TextView)statusView.findViewById(R.id.fragment_stock_status_yr_range);
        mVolume = (TextView)statusView.findViewById(R.id.fragment_stock_status_volume);
        mAvgVolume = (TextView)statusView.findViewById(R.id.fragment_stock_status_avg_volume);
        mMktCap = (TextView)statusView.findViewById(R.id.fragment_stock_status_mkt_cap);
        mLastDateTime = (TextView)statusView.findViewById(R.id.fragment_stock_status_last_datetime);
        configureUpdateButtonClick(); /* Configure the update button. */
        /* Restore the last update button state if it exists. */
        restoreLastUpdateButtonState(savedInstanceState);
        /* Configure the attribution button. */
        configureAttributionInFragmentView(statusView);

        return statusView;
    }

    /* Setup the action taken when the update button is enabled and clicked. */
    private void configureUpdateButtonClick() {
        /* Configure the update button click listener. */
        mUpdateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* Show update indicator. */
                mUpdateIndicator.setVisibility(View.VISIBLE);
                mUpdateButton.setEnabled(false); /* Disable update button. */
                /* Request status data from StockStatus. */
                mStatusProvider.getStatusWithSymbol(mStock.getStockSymbol());
                mStatusDidSave = false; /* Request not yet saved. */
                /* Update icon indicators. */
                getActivity().invalidateOptionsMenu();
                resetWakeUpdateTimer(); /* Reset update lockout. */
            }
        });
    }

    /* If the parameter contains saved instance state use the update button
     * state that was last stashed. Otherwise reset update lockout. */
    private void restoreLastUpdateButtonState(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) { /* Check if the state exists. */
            /* Check if update was enabled when state was last stashed. */
            boolean isEnabled =  savedInstanceState.getBoolean(UPDATE_STATE);
            mUpdateButton.setEnabled(isEnabled); /* Update current state. */
        } else {
            /* No saved state. Use default update lockout. */
            resetWakeUpdateTimer();
        }
    }

    /* Load a fragment's view content for the first time. */
    private void initialiseDataSource() {
        /* Check for cached status data to display. */
        if (mStatusProvider.getStockStatus() != null) {
            /* Update the view with cached data. */
            doViewUpdateWithStatus(mStatusProvider.getStockStatus());
        } else {
            /* Check for a saved status. */
            Status lastStatus = StockDbHelper.get(getActivity())
                    .getStatusForSymbolId(mStock.getId());
            if (lastStatus != null) {
                /* Update the view with saved data. */
                doViewUpdateWithStatus(lastStatus);
            } else {
                /* No data. Request status data from StockStatus. */
                mStatusProvider.getStatusWithSymbol(mStock.getStockSymbol());
                mStatusDidSave = false; /* Request not yet saved. */
                /* Show a message that the first update has started. */
                Toast msg = Toast.makeText(
                        getActivity(),
                        R.string.loading_status,
                        Toast.LENGTH_SHORT);
                msg.setGravity(Gravity.CENTER, 0, 0);
                msg.show();
            }
        }
    }

    /* Reset the update button lockout. The update button will enable again
     * after UPDATE_DELAY_MS has passed. */
    private void resetWakeUpdateTimer() {
        cancelWake(); /* Cancel existing lockouts. */
        /* Create a new unlock task to enable the update button. */
        mWake = new WakeUpdateAfterDelay(UPDATE_DELAY_MS);
    }

    /* Cancel the update button wake (enabled) after lockout. */
    private void cancelWake() {
        if (mWake != null) {
            mWake.cancel(true); /* Stop the button enabler. */
        }
    }

    @Override
    /* Override point for when the fragment is resumed. */
    public void onResume() {
        super.onResume();
        /* Start the activity indicator until stopped. */
        mUpdateIndicator.setVisibility(View.VISIBLE);
        initialiseDataSource(); /* Prepare the view's content data. */
    }

    /* Update the view using the Status parameter provided. */
    private void doViewUpdateWithStatus(Status status) {
        /* Configure the dollar change views. */
        setChangeIndicator(mChangeArrow, mChange, status.getChange());
        /* Configure the percent change views. */
        setChangeIndicator(mPctChangeArrow, mPctChange, status.getPercentChange());
        /* Configure the stock's status attributes. */
        mCurrency.setText(status.getCurrency());
        mPrvClose.setText(status.getPrevClose());
        mOpen.setText(status.getOpen());
        mBid.setText(status.getBid());
        mAsk.setText(status.getAsk());
        mYrTgtEst.setText(status.getYearTargetEst());
        mDayRange.setText(status.getDaysRange());
        mYrRange.setText(status.getYearsRange());
        mVolume.setText(status.getVolume());
        mAvgVolume.setText(status.getAvgVolume());
        mMktCap.setText(status.getMarketCap());
        /* Check if the status being setup is a no-result-found type. */
        if (status.getLastTradeTime().equals("N/A")) {
            /* Don't show the unknown last trade time text. */
            status.setLastTradeTime("");
        }
        /* Show the date and time together. */
        String dateTime = status.getLastTradeDate() + " " + status.getLastTradeTime();
        mLastDateTime.setText(dateTime);
        /* Turn off the activity indicator after a slight delay. */
        new TimedProgress(mUpdateIndicator, PAD_UPDATE_COMPLETE);
    }

    /* The status fragment contains views which include arrow indicators. This
     * method accepts the image and text views to update based on a change
     * string. The views will be updated depending on change string prefix.  */
    private void setChangeIndicator(ImageView changeArrow,
                                    TextView change, String changeString) {
        /* Check if change string includes the +/- prefix. If it does use
         * prefix to update text color and arrow. Otherwise set default. */
        if (changeString.startsWith("+") || changeString.startsWith("-")) {
            if (changeString.startsWith("+")) {
                /* Found prefix +. Set view to positive change. */
                change.setTextColor(Color.rgb(0,150,0));
                changeArrow.setBackgroundResource(R.drawable.up);
            } else if (changeString.startsWith("-")) {
                /* Found prefix -. Set view to negative change. */
                change.setTextColor(Color.RED);
                changeArrow.setBackgroundResource(R.drawable.down);
            }
            /* Make sure the arrow is visible. */
            changeArrow.setVisibility(View.VISIBLE);
            /* The displayed change removes the prefix. */
            change.setText(changeString.substring(1));
        } else {
            /* Did not find the +/- prefix. Remove the arrow. */
            changeArrow.setVisibility(View.INVISIBLE);
            /* Use the change string as is when updating the text view. */
            change.setText(changeString);
        }
    }

    /* Link to Yahoo! per attribution requirements when using YQL or Yahoo!
     * APIs. Add an onClick listener to the Yahoo! image and load an implicit
     * intent to request URL be loaded by a browser. */
    private void configureAttributionInFragmentView(View statusView) {
        /* Request the attribution image. */
        ImageView attributionImage = (ImageView)statusView
                .findViewById(R.id.fragment_stock_status_attribution);
        /* Configure the image to load the attribution URL when touched. */
        attributionImage.setOnClickListener(new View.OnClickListener() {
            @Override
            /* Load the Yahoo! URL in a browser when the image is clicked. */
            public void onClick(View v) {
                /* Prepare an implicit intent to visit the Yahoo! URL. */
                Intent visitYahoo = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.yahoo.com/?ilc=401"));
                /* Load a browser and display the Yahoo! URL. */
                startActivity(visitYahoo);
            }
        });
    }

    @Override
    /* The pending Status update operation has now completed. */
    public void didCompletePendingStatusOperation() {
        mStatusDidSave = true; /* Update status saved flag. */
        getActivity().invalidateOptionsMenu(); /* Update notification icons. */
    }

    @Override
    /* A status query which was previously submitted has now completed.
     * Controllers can now update their views with the StockStatus. */
    public void statusDidCompleteWith(final Status status) {
        /* Update the view on the main thread. StockStatus uses many background
         * threads and this method may have been called from one of them. */
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                doViewUpdateWithStatus(status); /* Update view with status. */
            }
        });
    }

    @Override
    /* A status query which was previously submitted did not complete. The
     * parameter `error` contains the cause of the failure. */
    public void statusDidNotCompleteWithError(Throwable error) {
        Log.e(TAG, error.toString());
    }

    @Override
    /* A chart query which was previously submitted did not complete. The
     * parameter `error` contains the cause of the failure. */
    public void chartDidNotRetrieveWithError(Throwable error) {
        Log.e(TAG, error.toString());
    }

    @Override
    /* A competitor query which was previously submitted did not complete. The
     * parameter `error` contains the cause of the failure. */
    public void competitorsNotRetrievedWithError(Throwable error) {
        Log.e(TAG, error.toString());
    }

    /* An update button unlock timer using AsyncTask. The update button is
     * enabled after the delayMs parameter expires. Note in StockStatusFragment
     * the update button defaults disabled. It is enabled after this wake timer
     * has expired. */
    private class WakeUpdateAfterDelay extends AsyncTask<Long, Void, Void> {
        public WakeUpdateAfterDelay(Long delayMs) {
            executeOnExecutor(THREAD_POOL_EXECUTOR, delayMs);
        }
        @Override
        /* Snooze on a background thread. */
        protected Void doInBackground(Long... params) {
            try {
                Thread.sleep(params[0]);
            } catch (InterruptedException e) {
                /* Ignore interrupted. */
            }
            return null;
        }
        @Override
        /* Enable the update button on the main thread. */
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            /* Guard against updating buttons when view is destroyed. */
            if (mUpdateButton != null && !isCancelled()) {
                /* Check network state. Enable the button when online. */
                if (Reachability.get(getActivity()).isNetworkOnline()) {
                    mUpdateButton.setEnabled(true);
                }
            }
        }
    }
}
