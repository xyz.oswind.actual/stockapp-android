//
//  StockDetailFragment.java
//  StockApp
//
//  A base fragment implementation containing stock details in a tab bar.
//
//  Created by David McKnight on 2016-05-01.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.controller.detail;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import xyz.oswind.stockapp.SingleFragmentActivity;
import xyz.oswind.stockapp.contentprovider.status.StockStatus;
import xyz.oswind.stockapp.model.StockDbHelper;
import xyz.oswind.stockapp.model.entities.Stock;

public abstract class StockDetailFragment extends Fragment {
    /* Identifier for the symbolId included with intents. This is used by tab
     * fragments to select or update saved data. */
    private static final String EXTRA_SID = "xyz.oswind.stockapp.symbol_id";
    protected StockStatus mStatusProvider; /* Access cache if possible. */
    protected Stock mStock;                /* This fragment's stock. */

    @LayoutRes
    /* Obtain the layout resource id for this StockDetailFragment. */
    protected abstract int getLayoutResId();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /* Retain this fragment. This means the fragment will not be destroyed
         * during a configuration change. Instead it will be detached from the
         * current activity then attached to a new activity. This will recreate
         * views but allow the current search state to persist. */
        setRetainInstance(true);
        mStatusProvider = StockStatus.get(getActivity()); /* Check cache. */
        mStock = StockDbHelper.get(getActivity()) /* Load the stock entity. */
                .getStockWithSymbolId(getActivity()
                        /* Using the id from the (tab bar) detail activity. */
                        .getIntent().getLongExtra(EXTRA_SID, 0));
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        /* Create a stock detail view using the fragment's layout. */
        View fragmentView = inflater.inflate(
                getLayoutResId(), container, false);
        /* Set the toolbar title to the stock symbol. */
        getActivity().setTitle(mStock.getStockSymbol());
        configureNavigationUp(); /* Add a navigation up option. */
        return fragmentView;
    }

    @Override
    /* Override point for handling selected menu items. */
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            /* Navigate up was selected. */
            getActivity().finish();
            /* Override the transition animation back to watched stocks. */
            getActivity().overridePendingTransition(
                    android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /* Add the navigation up pattern to the support toolbar. */
    protected void configureNavigationUp() {
        /* Cast to enable getSupportActionBar() invocation. The invocation of
         * getActivity in a support fragment includes an explicit cast to a
         * FragmentActivity (which lacks getSupportActionBar). Since all
         * activities inherit from SingleFragmentActivity, which itself inherits
         * from AppCompatActivity, this should be okay. */
        SingleFragmentActivity activity = (SingleFragmentActivity)getActivity();
        /* Request the support toolbar. */
        ActionBar supportActionBar = activity.getSupportActionBar();
        /* Check for null to prevent some code lint warnings. */
        if (supportActionBar != null) {
            /* Add the up navigation option. */
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            setHasOptionsMenu(true);
        }
    }
}
