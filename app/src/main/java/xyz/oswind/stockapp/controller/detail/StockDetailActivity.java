//
//  StockDetailActivity.java
//  StockApp
//
//  An activity containing a tab bar controller for tabbing between stock detail
//  views. Pressing a tab switches to a fragment containing the related stock
//  detail. The default tab is the StockStatusFragment.
//
//  Created by David McKnight on 2016-05-01.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.controller.detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;

import xyz.oswind.stockapp.R;
import xyz.oswind.stockapp.SingleFragmentActivity;
import xyz.oswind.stockapp.contentprovider.status.StockStatus;

public class StockDetailActivity extends SingleFragmentActivity {
    private static final String TAG = "StockDetailActivity";
    /* Identifier for the symbolId included with intents. This is used by tab
     * fragments to select or update saved data. */
    private static final String EXTRA_SID = "xyz.oswind.stockapp.symbol_id";
    private Button mStatusTab;      /* The status tab bar button. */
    private Button mChartTab;       /* The chart tab bar button. */
    private Button mCompetitorsTab; /* The competitors tab bar button. */
    private View mDisplayState; /* The currently selected view. */
    private View.OnFocusChangeListener mTabBarListener;

    @Override
    /* Create the default stock detail view. */
    protected Fragment createFragment() {
        /* Default to showing the stock status fragment during activity load. */
        return new StockStatusFragment();
    }

    @LayoutRes
    @Override
    /* Obtain the layout resource id. Use a layout which contains both a tab bar
     * and a swappable content area. */
    protected int getLayoutResId() {
        return R.layout.activity_tabbed_fragment;
    }

    /* Request the creation of the stock detail view by intent. The parameter
     * `symbol` contains the stock symbol detail to be displayed. */
    public static Intent detailIntent(Context context, Long symbolId) {
        Intent detail = new Intent(context, StockDetailActivity.class);
        /* Indicate which stock to show by providing the unique symbol. */
        detail.putExtra(EXTRA_SID, symbolId);
        return detail;
    }

    @Override
    /* Override point for when the activity is stopped. */
    protected void onStop() {
        super.onStop();
        /* Shut off the tab buttons while stopped to prevent exception. */
        mStatusTab.setOnFocusChangeListener(null);
        mChartTab.setOnFocusChangeListener(null);
        mCompetitorsTab.setOnFocusChangeListener(null);
    }

    @Override
    /* Override point for when the activity is destroyed. */
    public void onDestroy() {
        super.onDestroy();
        /* Reset StockStatus if it's still running. Db operations will be
         * allowed to continue if they have started. */
        StockStatus.get(getApplicationContext()).reset();
    }

    @Override
    /* Override point for when the activity is starting. */
    protected void onStart() {
        super.onStart();
        configureTabBarButtons(); /* Enable the tab buttons. */
    }

    @Override
    /* Override point for when the activity is created. */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createTabBarButtons(); /* Configure the tab bar for use. */
    }

    private void createTabBarButtons() {
        /* Request the status button in the tab bar. */
        mStatusTab = (Button)findViewById(R.id.stock_detail_tab_status);
        mDisplayState = mStatusTab; /* Cache the displaying view. */
        /* Request the chart button in the tab bar. */
        mChartTab = (Button)findViewById(R.id.stock_detail_tab_chart);
        /* Request the competitors button in the tab bar. */
        mCompetitorsTab = (Button)findViewById(R.id.stock_detail_tab_competitors);
    }

    private void configureTabBarButtons() {
        /* Create a shared on-focus listener for the tab bar. */
        if (mTabBarListener == null) {
            mTabBarListener = new TabBarFocusListener();
        }
        /* Set the shared on focus listener. */
        mStatusTab.setOnFocusChangeListener(mTabBarListener);
        mChartTab.setOnFocusChangeListener(mTabBarListener);
        mCompetitorsTab.setOnFocusChangeListener(mTabBarListener);
    }

    /* A class that provides a shared focus change listener. Change the
     * displayed fragment based on the tab bar button pressed. */
    private class TabBarFocusListener implements View.OnFocusChangeListener {
        @Override
        /* Override point for handling the focus event. */
        public void onFocusChange(View view, boolean hasFocus) {
            /* Check if this is a new focus event. If true then load the detail
             * fragment corresponding to the tab bar button pressed. Otherwise
             * ignore the event. */
            if (hasFocus && view != mDisplayState) {
                Fragment fragment = null; /* The fragment to be displayed. */
                /* Check which detail fragment to display. */
                if (view == mStatusTab) {
                    /* Display the status fragment. */
                    fragment = new StockStatusFragment();
                } else if (view == mChartTab) {
                    /* Display the chart fragment. */
                    fragment = new StockChartFragment();
                } else if (view == mCompetitorsTab){
                    /* Display the competitors fragment. */
                    fragment = new StockCompetitorsFragment();
                }
                mDisplayState = view; /* Update the displayed view cache. */
                /* Update the view to display the selected fragment. */
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, fragment)
                        .commit();
            }
        }
    }

    @Override
    /* Override point for handling the back button being pressed. */
    public void onBackPressed() {
        finish(); /* Finish the detail activity. */
        /* Override the transition animation back to watched stocks. */
        overridePendingTransition(
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right);
    }
}
