//
//  StockChartFragment.java
//  StockApp
//
//  A tab fragment containing a stock's chart data if available.
//
//  Created by David McKnight on 2016-05-01.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.controller.detail;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import xyz.oswind.stockapp.R;
import xyz.oswind.stockapp.model.StockDbHelper;
import xyz.oswind.stockapp.model.entities.Chart;

public class StockChartFragment extends StockDetailFragment {
    private static final String TAG = "StockChartFragment";
    private ImageView mChartImage;       /* An image from chart data. */

    @LayoutRes
    @Override
    /* A tab containing a stock's chart. */
    protected int getLayoutResId() {
        return R.layout.fragment_stock_chart;
    }

    @Override
    /* Override point for when creating the menu. */
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        /* Create a menu using the menu resource. */
        inflater.inflate(R.menu.menu_stock_status, menu);
        /* Only show saved icon on the status tab. */
        MenuItem saved = menu.findItem(R.id.menu_item_icon_saved);
        saved.setVisible(false);
        /* Show an icon when using a cached chart. */
        MenuItem cached = menu.findItem(R.id.menu_item_icon_cached);
        cached.setVisible(mStatusProvider.getStockChart() != null);
    }

    @Override
    /* Override point for handling selected menu items. */
    public boolean onOptionsItemSelected(MenuItem item) {
        /* Check if the menu icon was clicked. */
        if (item.getItemId() == R.id.menu_item_icon_cached) {
            /* Show the same text as the icon long-press. Status is cached. */
            Toast msg = Toast.makeText(getActivity(),
                    R.string.icon_cached, Toast.LENGTH_SHORT);
            msg.setGravity(Gravity.TOP|Gravity.RIGHT, 0, 60);
            msg.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    /* Override point for when creating the fragment view. */
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View chartView = super.onCreateView( /* See StockDetailFragment. */
                /* Apply common tab properties. */
                inflater, container, savedInstanceState);
        assert chartView != null;
        mChartImage = (ImageView)chartView /* Cache the Id of the image view. */
                .findViewById(R.id.fragment_stock_chart_image);
        initialiseDataSource(); /* Prepare the view's chart data. */
        /* Configure the attribution button. */
        configureAttributionInFragmentView(chartView);

        return chartView;
    }

    /* Link to Yahoo! per attribution requirements when using YQL or Yahoo!
     * APIs. Add an onClick listener to the Yahoo! image and load an implicit
     * intent to request URL be loaded by a browser. */
    private void configureAttributionInFragmentView(View chartView) {
        /* Request the attribution image. */
        ImageView attributionImage = (ImageView)chartView
                .findViewById(R.id.fragment_stock_chart_attribution);
        /* Configure the image to load the attribution URL when touched. */
        attributionImage.setOnClickListener(new View.OnClickListener() {
            @Override
            /* Load the Yahoo! URL in a browser when the image is clicked. */
            public void onClick(View v) {
                /* Prepare an implicit intent to visit the Yahoo! URL. */
                Intent visitYahoo = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.yahoo.com/?ilc=401"));
                /* Load a browser and display the Yahoo! URL. */
                startActivity(visitYahoo);
            }
        });
    }

    /* Load a fragment's view content for the first time. */
    private void initialiseDataSource() {
        /* Check for cached chart data to display. */
        if (mStatusProvider.getStockChart() != null) {
            /* Update the view with cached data. */
            doViewUpdateWithChart(mStatusProvider.getStockChart());
        } else {
            /* Check for a saved chart. */
            Chart lastChart = new Chart(StockDbHelper.get(getActivity())
                    .getChartDataForSymbolId(mStock.getId()));
            if (lastChart.getChartData() != null) {
                /* Update the view with saved data. */
                doViewUpdateWithChart(lastChart);
            } else {
                /* Show a message that no data is available. */
                Toast msg = Toast.makeText(
                        getActivity(),
                        R.string.no_chart_data,
                        Toast.LENGTH_SHORT);
                msg.setGravity(Gravity.CENTER, 0, 0);
                msg.show();
            }
        }
    }

    /* Update the view using the Chart parameter provided. */
    private void doViewUpdateWithChart(Chart chart) {
        byte[] image = chart.getChartData(); /* Extract image bytes. */
        mChartImage.setImageBitmap(BitmapFactory /* Update the image view. */
                /* Decode the bytes as an image. */
                .decodeByteArray(image, 0, image.length));
    }
}
