//
//  StockWatchActivity.java
//  StockApp
//
//  The activity containing a fragment responsible for displaying watched
//  stocks. This is the first activity loaded after launching the application.
//
//  Created by David McKnight on 2016-04-22.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.controller.watched;

import android.content.res.Configuration;
import android.support.v4.app.Fragment;

import xyz.oswind.stockapp.Reachability;
import xyz.oswind.stockapp.SingleFragmentActivity;

public class StockWatchActivity extends SingleFragmentActivity {

    @Override
    /* Create the stock watch view. */
    protected Fragment createFragment() {
        /* Activate reachability since StockWatch is the app's main activity. */
        Reachability.get(getApplicationContext());
        return StockWatchFragment.newInstance();
    }

    /* The support toolbar can be hidden in landscape orientation to free up
     * space for fragment content. This method hides the toolbar in landscape
     * orientation and un-hides the toolbar otherwise. */
    protected void updateToolbarHiddenInLandscape() {
        if (getResources().getConfiguration().orientation ==
                Configuration.ORIENTATION_LANDSCAPE) {
            getSupportActionBar().hide(); /* Landscape orientation. */
        } else {
            getSupportActionBar().show(); /* Portrait orientation. */
        }
    }
}
