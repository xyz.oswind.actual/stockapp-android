//
//  StockWatchFragment.java
//  StockApp
//
//  The controller for a fragment responsible for displaying watched stocks.
//  These stocks are persisted in an application-private SQLite database.
//  Responds to touch events on rows which will load a stock's detail view.
//
//  Created by David McKnight on 2016-04-22.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.controller.watched;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.List;

import xyz.oswind.stockapp.DividerDecoration;
import xyz.oswind.stockapp.R;
import xyz.oswind.stockapp.Reachability;
import xyz.oswind.stockapp.StockQueryDecoration;
import xyz.oswind.stockapp.TimedProgress;
import xyz.oswind.stockapp.controller.detail.StockDetailActivity;
import xyz.oswind.stockapp.controller.search.StockSearchActivity;
import xyz.oswind.stockapp.model.StockDbHelper;
import xyz.oswind.stockapp.model.StockDbHelperDelegate;
import xyz.oswind.stockapp.model.cursor.StockCursor;
import xyz.oswind.stockapp.model.entities.Status;
import xyz.oswind.stockapp.model.entities.Stock;

public class StockWatchFragment extends Fragment {
    private static final String TAG = "StockWatchFragment";
    /* Identifiers for the fragment manager when showing dialogs. */
    private static final String SEARCH_DIALOG = "SearchDisabled";
    private static final String STATUS_DIALOG = "StatusDisabled";
    private RecyclerView mWatchedStocksList;      /* List of watched stocks. */
    private StockAdapter mWatchedStocksAdapter;   /* Watched stock adapter. */
    private AutoCompleteTextView mFilterTextView; /* The filter text view. */
    private ProgressBar mActivityWatched;         /* Activity indicator. */

    /* Create a new instance of a StockWatchFragment. */
    public static StockWatchFragment newInstance() {
        return new StockWatchFragment();
    }

    @Override
    /* Override point for when the fragment is created. */
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /* Set the toolbar title for this fragment. */
        getActivity().setTitle(R.string.navigation_watched);
        setRetainInstance(false);/* Retain this fragment. */
        setHasOptionsMenu(true); /* This fragment has a menu. */
        mWatchedStocksAdapter = new StockAdapter(); /* Retained adapter. */
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /* Unset this fragment as the database filter delegate. */
        StockDbHelper.get(getActivity()).setFilterDelegate(null);
    }

    @Override
    /* Override point for when the fragment's view is created. */
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        /* Create a watched stocks view using the fragment's layout. */
        View watchedStocks = inflater.inflate(
                R.layout.fragment_stock_watch, container, false);
        /* Request the activity indicator for watched stock updates. */
        mActivityWatched = (ProgressBar)watchedStocks
                .findViewById(R.id.fragment_stock_watch_active);
        /* Configure the search view appearance for min-API 16+. */
        configureFilterInFragmentView(watchedStocks);
        /* Configure a link to Yahoo! per attribution requirements. */
        configureAttributionInFragmentView(watchedStocks);
        /* Configure the list view appearance and behaviour. */
        configureListInFragmentView(watchedStocks);
        /* Set the list view to use the adapter as a data source. */
        mWatchedStocksList.setAdapter(mWatchedStocksAdapter);
        /* Hide the toolbar if the device is in landscape orientation. */
        StockWatchActivity activity = (StockWatchActivity)getActivity();
        activity.updateToolbarHiddenInLandscape();
        /* Activate the landscape-only button for search if needed. */
        activateSearchButtonInLandscape(watchedStocks);

        return watchedStocks;
    }

    @Override
    public void onResume() {
        super.onResume();
        /* Show a short progress indicator. */
        new TimedProgress(mActivityWatched,1500);
    }

    @Override
    /* Override point for when creating the menu. */
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        /* Create a menu using the menu resource. */
        inflater.inflate(R.menu.menu_stock_watch, menu);
    }

    @Override
    /* Override point for handling selected menu items. */
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_item_action_search) {
            invokeSearch(); /* Start stock search. */
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /* In landscape orientation the search menu option is unavailable. A button
     * appears in the view to replace the option. Activate the button click when
     * the device is in landscape orientation*/
    private void activateSearchButtonInLandscape(View fragmentView) {
        if (getResources().getConfiguration().orientation ==
                Configuration.ORIENTATION_LANDSCAPE) {
            /* Request the id of the search button. */
            ImageView searchButton = (ImageView)fragmentView
                    .findViewById(R.id.fragment_stock_watch_search_button);
            /* Set the on click listener to start search when clicked. */
            searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    invokeSearch(); /* Start stock search. */
                }
            });
        }
    }

    /* Request the stock search functionality. */
    private void invokeSearch() {
        /* A search button was pressed. Check if the app is online. */
        if (!Reachability.get(getActivity()).isNetworkOnline()) {
                /* App is not online. Show a message. */
            FragmentManager fragmentManager = getFragmentManager();
            SearchDialogFragment dialog = new SearchDialogFragment();
            dialog.show(fragmentManager, SEARCH_DIALOG);
        } else {
                /* App is online. Load the stock search view. */
            Intent search = StockSearchActivity.searchIntent(getActivity());
            startActivity(search);
                /* Override the transition animation to stock search. Note that in
                 * testing it appears the hardwareAccelerated attribute must be
                 * declared in the application tag of AndroidManifest. Without it
                 * this animation may fail completely. */
            getActivity().overridePendingTransition(
                    R.anim.slide_in_from_right,
                    R.anim.slide_out_to_left);
        }
    }

    /* Configure the appearance of the search/filter box in the provided view.
     * This means setting the initial appearance and focused indicator as
     * required. This is done in code to ensure a consistent appearance on pre-
     * lollipop devices. If backwards compatibility and/or consistency is not
     * required this can instead be performed on API 21+ using two XML
     * attributes as described in the layout file for this fragment. */
    private void configureFilterInFragmentView(View fragmentView) {
        /* Request the fragment's search view. */
        SearchView searchView = (SearchView)fragmentView
                .findViewById(R.id.fragment_stock_watch_filter_view);
        /* Obtain the id of text edit view contained in the search view. */
        int filterTextId = searchView.getResources()
                .getIdentifier("android:id/search_src_text", null, null);
        /* Request the text edit view contained in the search view. */
        mFilterTextView = (AutoCompleteTextView)searchView
                .findViewById(filterTextId);
        /* Turn off the blinking cursor in the text edit view. */
        mFilterTextView.setCursorVisible(false);
        /* Obtain the id of the `X` button contained in the search view. */
        int clearFilterId = searchView.getResources()
                .getIdentifier("android:id/search_close_btn", null, null);
        /* Request the `X` button contained in the search view. */
        ImageView clearFilterView = (ImageView)searchView
                .findViewById(clearFilterId);
        /* Set a custom action to perform when the `X` button is clicked.
         * Specifically clear the text without triggering the keyboard. */
        clearFilterView.setOnClickListener(new StockFilterClearedListener());
        /* Configure the search view to have a visual indicator on the contained
         * filter text view layout depending on the current focused state. */
        searchView.setOnQueryTextFocusChangeListener(
                new StockQueryDecoration(getActivity(), searchView));
        /* Configure the search view text to filter watched stocks. */
        searchView.setOnQueryTextListener(new StockFilterTextListener());
    }

    /* Link to Yahoo! per attribution requirements when using YQL or Yahoo!
     * APIs. Add an onClick listener to the Yahoo! image and load an implicit
     * intent to request URL be loaded by a browser. */
    private void configureAttributionInFragmentView(View fragmentView) {
        /* Request the attribution image. */
        ImageView attributionImage = (ImageView)fragmentView
                .findViewById(R.id.fragment_stock_watch_attribution);
        /* Configure the image to load the attribution URL when touched. */
        attributionImage.setOnClickListener(new View.OnClickListener() {
            @Override
            /* Load the Yahoo! URL in a browser when the image is clicked. */
            public void onClick(View v) {
                /* Prepare an implicit intent to visit the Yahoo! URL. */
                Intent visitYahoo = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.yahoo.com/?ilc=401"));
                /* Load a browser and display the Yahoo! URL. */
                startActivity(visitYahoo);
            }
        });
    }

    /* Configure the list used to display watched stocks. This means preparing
     * the RecyclerView's layout, adding the divider decoration, and adding a
     * swipe gesture listener. The swipe gesture is used to support swipe-to-
     * delete functionality. */
    private void configureListInFragmentView(View fragmentView) {
        /* Request the list's recycler view. */
        mWatchedStocksList = (RecyclerView)fragmentView
                .findViewById(R.id.fragment_stock_watch_recycler_view);
        /* Configure the layout as a linear, vertical list. */
        mWatchedStocksList.setLayoutManager(
                new LinearLayoutManager(getActivity()));
        /* Add a divider between list items. */
        mWatchedStocksList.addItemDecoration(
                new DividerDecoration(getActivity()));
        /* Create a swipe gesture listener to support swipe-to-delete. */
        ItemTouchHelper swipeListener =
                new ItemTouchHelper(new SwipeCallbacks());
        /* Add the swipe gesture listener to the list view. */
        swipeListener.attachToRecyclerView(mWatchedStocksList);
    }

    /* A class that represents a single stock displayed in a list view item. */
    private class StockHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        /* The layouts contained in a stock holder. */
        private final RelativeLayout mDetailLayout; /* Stock details. */
        private final LinearLayout mDeleteLayout;   /* Delete confirmation. */
        /* The views contained in a view holder. */
        private final TextView mSymbolView; /* Displayed stock symbol. */
        private final TextView mNameView;   /* Displayed stock name. */
        private final TextView mStatusDate; /* Stock last update. */
        private Stock mStock; /* The currently displayed stock. */
        private final Button mDelete; /* Used during swipe to delete. */
        private final Button mCancel; /* Used during swipe to cancel delete. */

        /* Default constructor that caches view identifiers. */
        public StockHolder(View itemView) {
            super(itemView);
            /* Cache the layout identifiers. */
            mDetailLayout = (RelativeLayout)itemView
                    .findViewById(R.id.layout_list_item_detail);
            mDeleteLayout = (LinearLayout)itemView
                    .findViewById(R.id.layout_list_item_delete);
            /* Cache the text view identifiers. */
            mSymbolView = (TextView)itemView
                    .findViewById(R.id.list_item_stock_symbol);
            mNameView = (TextView)itemView
                    .findViewById(R.id.list_item_stock_name);
            mStatusDate = (TextView)itemView
                    .findViewById(R.id.list_item_stock_status_date);
            /* Cache button Ids used during swipe to delete. */
            mDelete = (Button)itemView
                    .findViewById(R.id.button_list_item_delete);
            mCancel = (Button)itemView
                    .findViewById(R.id.button_list_item_cancel);
        }

        public void setOnDelete(View.OnClickListener onDelete) {
            /* Apply a shared delete event listener to the delete button. */
            mDelete.setOnClickListener(onDelete);
        }

        public void setOnCancel(View.OnClickListener onCancel) {
            /* Apply a shared cancel event listener to the cancel button. */
            mCancel.setOnClickListener(onCancel);
        }

        /* Set the contents of this view holder to a provided stock. */
        public void bindStock(Stock stock) {
            mStock = stock; /* Hang onto the currently displayed stock. */
            /* Set this (possibly reused) view holder to the detail view. */
            setDeleteState(false, StockAdapter.INVALID);
            /* Add a listener to load the stock status when clicked. */
            itemView.setOnClickListener(this);
            /* Fill in the stock details. */
            mSymbolView.setText(mStock.getStockSymbol()); /* Set the symbol. */
            mNameView.setText(mStock.getStockName());     /* Set the name. */
            /* Set the last status date. Note the holder may be recycled. */
            if (mStock.getLastUpdate() != null) {
                /* Last update exists in the stock bound to this holder. */
                mStatusDate.setText(mStock.getLastUpdate());
            } else {
                /* Last update does not exist. */
                mStatusDate.setText(""); /* Bound stock contains null. */
            }
        }

        @Override
        /* Override point for handling a click on the view holder. */
        public void onClick(View v) {
            /* Clear focus on the filter text view. */
            mFilterTextView.clearFocus();
            /* Cancel pending delete requests when required. */
            mWatchedStocksAdapter
                    .didCancelDeleteWithNewTouch(getAdapterPosition());
            /* A watched stock was clicked. Check if the app is online. */
            if (!Reachability.get(getActivity()).isNetworkOnline()) {
                /* Not online. Check for a saved status. */
                Status status = StockDbHelper.get(getActivity()).getStatusForSymbolId(mStock.getId());
                if (status == null) {
                    /* No saved data and app is not online. Show a message. */
                    FragmentManager fragmentManager = getFragmentManager();
                    StatusDialogFragment dialog = new StatusDialogFragment();
                    dialog.show(fragmentManager, STATUS_DIALOG);
                    return; /* Return without loading detail activity. */
                }
            }
            /* App is online. Load the stock detail view for this stock. */
            Intent detail = StockDetailActivity
                    .detailIntent(getActivity(), mStock.getId());
            startActivity(detail);
                /* Override the transition animation to stock details. Note that in
                 * testing it appears the hardwareAccelerated attribute must be
                 * declared in the application tag of AndroidManifest. Without it
                 * this animation may fail completely. */
            getActivity().overridePendingTransition(
                    R.anim.slide_in_from_right,
                    R.anim.slide_out_to_left);
        }

        /* Configure the delete confirmation state of this view holder. */
        public void setDeleteState(boolean isActive, int direction) {
            /* Re-configure the layout if delete confirmation is activated. */
            if (isActive) {
                /* Create the custom layout parameters. */
                LayoutParams layout = new LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                /* Center the contained views vertically. */
                layout.addRule(RelativeLayout.CENTER_VERTICAL);
                /* Check the direction of the swipe-to-delete. */
                switch (direction) {
                    case StockAdapter.RIGHT_SWIPE:
                        setButtonOrder(mDelete, mCancel); /* Swiped right. */
                        layout.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                        break;
                    case StockAdapter.LEFT_SWIPE:
                        setButtonOrder(mCancel, mDelete); /* Swiped left. */
                        layout.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                        break;
                    default:
                        /* Unknown swipe direction. */
                        layout.addRule(RelativeLayout.CENTER_HORIZONTAL);
                }
                /* Apply the parameters to the delete confirmation layout. */
                mDeleteLayout.setLayoutParams(layout);
                /* Disable clicking on the swiped view holder. Note, this
                 * doesn't impact the button onClick listeners. */
                itemView.setOnClickListener(null);
            }
            /* Set the visible layout based on swipe-to-delete being active. */
            mDetailLayout.setVisibility(isActive? View.INVISIBLE: View.VISIBLE);
            mDeleteLayout.setVisibility(isActive? View.VISIBLE: View.INVISIBLE);
        }

        /* Reset button order by removing them from the view. */
        private void resetButtonOrder() {
            mDeleteLayout.removeView(mDelete);
            mDeleteLayout.removeView(mCancel);
        }

        /* Add first to the view and then second. Note this only works if the
         * layout is linear. View order then depends on order added. */
        private void setButtonOrder(Button first, Button second) {
            resetButtonOrder();
            mDeleteLayout.addView(first);
            mDeleteLayout.addView(second);
        }
    }

    /* A class that represents the data source binding for a list view. It also
     * maintains view holders and reuses them where possible. The adapter will
     * bind/rebind the list items to the SQL data source as required. The
     * adapter supports swipe-to-delete on one list item at a time. Swipe-to-
     * delete requires reconfiguring a (possibly reused) view holder to display
     * a delete confirmation. */
    private class StockAdapter extends RecyclerView.Adapter<StockHolder>
            implements LoaderManager.LoaderCallbacks<StockCursor> {
        /* Declare state to be invalid. */
        public static final int INVALID = -1;
        /* Swipe left and right. */
        public static final int LEFT_SWIPE = ItemTouchHelper.LEFT;
        public static final int RIGHT_SWIPE = ItemTouchHelper.RIGHT;
        /* The id of the loader that monitors the watched stocks table. */
        private static final int WATCHED_STOCK_LOADER = 0;
        /* Shared listeners used when confirming a swipe-to-delete. */
        private final View.OnClickListener mOnCancel;
        private final View.OnClickListener mOnDelete;
        /* A cursor window over the results from an SQL query. */
        private StockCursor mStockCursor;
        /* Support for one swipe-to-delete at a time. */
        private boolean mIsSwiped;
        /* Support iew holder reuse and one swipe-to-delete at a time. */
        private int mDirectionSwiped; /* Support view holder reuse. */
        private int mPositionSwiped;  /* Support view holder reuse. */
        /* Support switching data source to a filtered stocks list. */
        private boolean mIsFilteringStocks;
        private List<Stock> mFilteredStocks;

        /* Default constructor that prepares this adapter for use. */
        public StockAdapter() {
            mIsFilteringStocks = false; /* Not filtering watched stocks. */
            resetSwipeState(); /* Set the initial state. */
            /* Create a shared onClick listener for delete events. */
            mOnDelete = new View.OnClickListener() {
                @Override
                /* Handle the delete button click. */
                public void onClick(View v) {
                    onDelete(); /* Delete the watched stock that was swiped. */
                }
            };
            /* Create a shared onClick listener for cancel events. */
            mOnCancel = new View.OnClickListener() {
                @Override
                /* Handle the cancel button click. */
                public void onClick(View v) {
                    onCancel(); /* Clear the swiped state. */
                }
            };
            /* Begin monitoring the watched stocks table. */
            getLoaderManager().initLoader(WATCHED_STOCK_LOADER, null, this);
            setHasStableIds(true); /* Enable animated view updates. */
        }

        @Override
        /* Override point when creating a new view holder. */
        public StockHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            /* Request a new view holder using the layout file. */
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View view = inflater.inflate(
                    R.layout.list_item_stock, parent, false);
            StockHolder viewHolder = new StockHolder(view);
            /* Configure the shared swipe to delete button click listeners. */
            viewHolder.setOnCancel(mOnCancel);
            viewHolder.setOnDelete(mOnDelete);
            return viewHolder; /* Supports recycle. */
        }

        @Override
        /* Override point when setting the content of a view holder. */
        public void onBindViewHolder(StockHolder holder, int position) {
            /* Check if the view holder is being used at a position that was
             * swiped-to-delete. This is required as the view holder may be
             * reused in other positions when the view is scrolled. */
            if (position != mPositionSwiped) {
                Stock stock; /* Set the stock depending on data source. */
                /* Check if the data source is currently filtered. If false then
                 * request the stock to display from the stock cursor. Otherwise
                 * set the stock to display from the list of filtered stocks. */
                if (!mIsFilteringStocks) {
                    /* Re-position the cursor window. */
                    mStockCursor.moveToPosition(position);
                    /* Extract the stock to be shown from unfiltered stocks. */
                    stock = mStockCursor.getStock();
                } else {
                    /* Extract the stock to be shown from filtered stocks. */
                    stock = mFilteredStocks.get(position);
                }
                /* Set the view holder content to the stock being displayed. */
                holder.bindStock(stock);
            } else {
                /* This position has a pending delete confirmation. */
                holder.setDeleteState(true, mDirectionSwiped);
            }
        }

        @Override
        /* Override point for a data source item's id. Required to support
         * animated view updates. Must be unique. The primary key of the table
         * represents a stable/unique id for unfiltered stocks. When filtered
         * just use the position in the data source. */
        public long getItemId(int position) {
            /* Check if the data source is currently filtered. If false then use
             * the unique id from the watched stock table. If true then the data
             * is filtered and the data source index/position is semi-stable for
             * the duration of being filtered. */
            if (!mIsFilteringStocks) {
                /* Re-position the cursor window. */
                mStockCursor.moveToPosition(position);
                return mStockCursor.getId(); /* Request the primary key. */
            } else {
                /* Request the absolute position as a semi-stable id in a
                 * filtered data source. */
                return position;
            }
        }

        @Override
        /* Override point for the count of items in the data source. */
        public int getItemCount() {
            /* Check if the data source is currently filtered. If false then use
             * the item count from the unfiltered stock cursor. Otherwise use
             * the count from the filtered stock list. */
            if (!mIsFilteringStocks) {
                /* Make sure the loader is available. */
                if (mStockCursor == null) {
                    return 0; /* Nothing to show. */
                }
                /* Total rows in the unfiltered data source. */
                return mStockCursor.getCount();
            } else {
                /* Total rows in the filtered data source. */
                return mFilteredStocks.size();
            }
        }

        /* Switch the data source to a list of filtered stocks. The reference
         * that was previously set to a filtered list of stocks will be over-
         * written with the parameter `filtered`. The unfiltered stock cursor
         * will remain cached for restoration. */
        public void setFilteredStocks(List<Stock> filtered) {
            mIsFilteringStocks = true;
            mFilteredStocks = filtered;
            notifyDataSetChanged();
        }

        /* Switch the data source to an unfiltered cursor of stocks. The adapter
         * starts in this state. This method only needs to be called when
         * switching the data source after calling setFilteredStocks. The
         * reference that was previously set to a filtered list of stocks will
         * be unset. The cached reference to the unfiltered stock cursor will
         * then be restored. */
        public void setUnfilteredStocks() {
            mIsFilteringStocks = false;
            mFilteredStocks = null;
            notifyDataSetChanged();
        }

        /* Support for one swipe-to-delete at a time. If the list has already
         * been swiped then check the previous position swiped and clear it as
         * required. Otherwise block another swipe. */
        public boolean isSwiped() { return mIsSwiped; }

        /* A view holder was swiped-to-delete. Update the view to show a
         * conformation before actually deleting the watched stock. */
        public void onSwipeDeleteViewHolder(StockHolder holder, int direction) {
            mIsSwiped = true; /* Indicate a pending delete. */
            /* Hang onto the position in the data source. */
            mPositionSwiped = holder.getAdapterPosition();
            /* The direction swiped is used to layout the confirmation. */
            mDirectionSwiped = direction;
            notifyItemChanged(mPositionSwiped); /* Reload the view swiped. */
        }

        /* A pending delete exists but another touch/swipe has happened. Check
         * the new touch location and compare it to the last location swiped. If
         * it's a different location then cancel the pending delete and redraw
         * the view un-swiped. Returns true if the touch was at a new location
         * and the last swipe-to-delete was cancelled. Otherwise returns false
         * to indicate a pending delete still exists. */
        public boolean didCancelDeleteWithNewTouch(int newPosition) {
            if (newPosition != mPositionSwiped) {
                onCancel();
                return true;
            }
            return false;
        }

        /* The last swipe-to-delete was cancelled. */
        private void onCancel() {
            int updatePosition = mPositionSwiped; /* Location to redraw. */
            resetSwipeState();                    /* Reset swipe state. */
            notifyItemChanged(updatePosition);    /* Redraw the view. */
        }

        /* The last swipe-to-delete was confirmed. */
        private void onDelete() {
            Stock deleted; /* Set the stock depending on data source. */
            /* Check if the data source is currently filtered. If false then use
             * the unfiltered stock cursor to select the swipe deleted stock.
             * Otherwise use the filtered stock list to select the swipe deleted
             * stock. */
            if (!mIsFilteringStocks) {
                /* Re-position the cursor window at the deleted stock. */
                mStockCursor.moveToPosition(mPositionSwiped);
                /* Request the stock to be deleted from unfiltered stocks. */
                deleted = mStockCursor.getStock();
            } else {
                /* Request the stock to be deleted from filtered stocks. */
                deleted = mFilteredStocks.get(mPositionSwiped);
                /* When the stocks are filtered remove the deleted stock from
                 * the displayed filtered stocks. This is required since the
                 * filtered stocks list was statically generated. Unfiltered
                 * stocks are already updated dynamically. */
                mFilteredStocks.remove(mPositionSwiped);
            }
            /* Now delete the stock which was swiped from the database. */
            StockDbHelper.get(getActivity()) /* Using the shared db helper. */
                    .deleteWatched(deleted); /* Delete the stock. */
            resetSwipeState(); /* Reset swipe state. */
        }

        /* Reset the swipe-to-delete state. */
        private void resetSwipeState() {
            mIsSwiped = false;          /* Not currently swiped. */
            mDirectionSwiped = INVALID; /* Invalidate direction. */
            mPositionSwiped = INVALID;  /* Invalidate position. */
        }

        @Override
        /* Callback used when creating a loader. */
        public Loader<StockCursor> onCreateLoader(int id, Bundle args) {
            return StockDbHelper             /* Using the shared db helper. */
                    .get(getActivity())
                    .getWatchedStockLoader();/* Request the current loader. */
        }

        @Override
        /* Callback used when the loader has provided refreshed results. */
        public void onLoadFinished(Loader<StockCursor> loader,
                                   StockCursor data) {
            /* Check if this is the same data in use. */
            if (mStockCursor == data) {
                return; /* Continue without refreshing the view. */
            }
            /* Update the current data source. */
            mStockCursor = data;
            /* The cursor data may have been refreshed due to a deletion or an
             * added watched stock. In the case of a deletion it would be easy
             * to maintain some internal state and call notifyItemRemoved()
             * here. On the other hand, in the case of an added watched stock,
             * it wouldn't be easy to call notifyItemInserted() without some
             * non-trivial code (the results are ordered in SQL). All options
             * require extra memory or cpu cycles in the app or sql. Note this
             * shouldn't be a huge concern as the data is loaded using a cursor
             * anyway. Instead just call notifyDataSetChanged() to handle both
             * cases until a better option is required. */
            notifyDataSetChanged(); /* Refresh the list view. */
        }

        @Override
        /* Callback used when the loader has been reset/shutdown. */
        public void onLoaderReset(Loader<StockCursor> loader) {
            mStockCursor = null; /* Updates not available. */
        }
    }

    /* A class that provides swipe functionality for a recycler view. */
    private class SwipeCallbacks extends ItemTouchHelper.SimpleCallback {

        /* A default constructor that supports left/right swipe only. */
        public SwipeCallbacks() {
            super(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
        }

        @Override
        /* Drag is not used or supported in this implementation. Documentation
         * recommends a stub-implementation even if drag is not used. */
        public boolean onMove(RecyclerView recyclerView,
                              RecyclerView.ViewHolder viewHolder,
                              RecyclerView.ViewHolder target) {
            return false; /* Do not move. */
        }

        @Override
        /* Override point for handling the swipe event. */
        public void onSwiped(RecyclerView.ViewHolder viewHolder,
                             int direction) {
            /* The recycler view contains StockHolder views. */
            StockHolder holder = (StockHolder)viewHolder;
            /* Display one delete prompt at a time. */
            mWatchedStocksAdapter /* Notify adapter of swipe-to-delete event. */
                    .onSwipeDeleteViewHolder(holder, direction);
        }

        @Override
        public int getSwipeDirs(RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder) {
            /* If the swipe wasn't new return IDLE. */
            if (!isSwipeAllowed(viewHolder)) {
                return 0;
            }
            /* Return the swipe direction LEFT or RIGHT. */
            return super.getSwipeDirs(recyclerView, viewHolder);
        }

        private boolean isSwipeAllowed(RecyclerView.ViewHolder viewHolder) {
            /* Check if a previous swipe-to-delete is still pending. */
            if (mWatchedStocksAdapter.isSwiped()) {
                /* Allow a new swipe if it is on some other StockHolder. */
                boolean allowSwipe = mWatchedStocksAdapter
                        /* Check if the swipe is on a different view. */
                        .didCancelDeleteWithNewTouch(
                                /* The StockHolder that was swiped this time. */
                                viewHolder.getAdapterPosition());
                /* If the swipe wasn't new then invalidate the swipe. */
                if (!allowSwipe) {
                    return false;
                }
            }
            return true;
        }
    }

    /* A class that acts as a filter query listener. Submits queries to the
     * database to filter stocks when a non-empty query is entered. Resets to
     * the unfiltered stocks when the filter query text is empty. */
    private class StockFilterTextListener
            implements SearchView.OnQueryTextListener,
            StockDbHelperDelegate.Filter {
        /* A default constructor which sets this instance as the delegate for
         * filter notifications from StockDbHelper. */
        public StockFilterTextListener() {
            StockDbHelper.get(getActivity()).setFilterDelegate(this);
        }

        @Override
        /* Dismiss the keyboard when handling the query submit event.
         * Clearing the focus is a hint to hide the keyboard. */
        public boolean onQueryTextSubmit(String query) {
            mFilterTextView.clearFocus();
            return false; /* Indicate the query has been handled. */
        }

        @Override
        /* Search using the current query text. */
        public boolean onQueryTextChange(String query) {
            /* Show the activity indicator. */
            mActivityWatched.setVisibility(View.VISIBLE);
            /* Check the query content to see if a filter is being applied or
             * removed. */
            if (!query.equals("")) {
                /* Non-empty query. Request filtered stocks. */
                StockDbHelper.get(getActivity()).submitFilterToWatched(query);
            } else {
                /* No query text. Reset to unfiltered stocks. */
                mWatchedStocksAdapter.setUnfilteredStocks();
                /* Stop the activity indicator. */
                mActivityWatched.setVisibility(View.INVISIBLE);
            }
            return true; /* Indicate the query has been handled. */
        }

        @Override
        /* A filter operation on the watched table has completed. The parameter
         * `filteredStocks` contains the result of filtering stocks. */
        public void didCompleteFilterWithResults(List<Stock> filteredStocks) {
            /* Use the filtered results. */
            mWatchedStocksAdapter.setFilteredStocks(filteredStocks);
            /* Stop the activity indicator. */
            mActivityWatched.setVisibility(View.INVISIBLE);
        }
    }

    /* A class that provides a custom action when the clear text `X` button is
     * pressed in a search view. Normally pressing `X` would trigger the
     * keyboard for text entry. However in the case of a filter applied to
     * stocks it appears reasonable to override this behaviour. Clearing the
     * filter should allow the unfiltered stocks to be restored without also
     * triggering the keyboard. */
    private class StockFilterClearedListener implements View.OnClickListener {
        /* Explicitly control the keyboard when the filter is cleared. */
        private final InputMethodManager mInputMethodManager;

        public StockFilterClearedListener() {
            /* Request the input method manager for explicit keyboard control
             * when the filter text is cleared. */
            mInputMethodManager = (InputMethodManager)getActivity()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
        }

        @Override
        /* Override point for handling the click event. */
        public void onClick(View v) {
            mFilterTextView.setText("");  /* Clear the filter text. */
            mFilterTextView.clearFocus(); /* Clear the focus. */
            /* Hide the keyboard since the filter was cleared. Meaning a filter
             * was applied and has now been removed. This allows the unfiltered
             * stocks to be displayed without the keyboard being triggered. */
            mInputMethodManager.hideSoftInputFromWindow(
                    v.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
