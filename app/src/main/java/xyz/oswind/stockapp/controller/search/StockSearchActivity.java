//
//  StockSearchActivity.java
//  StockApp
//
//  The activity containing a fragment responsible for searching for stocks.
//  Creates the fragment on-demand using an intent.
//
//  Created by David McKnight on 2016-04-22.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.controller.search;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.app.Fragment;

import xyz.oswind.stockapp.SingleFragmentActivity;

public class StockSearchActivity extends SingleFragmentActivity {

    @Override
    /* Create the stock search view. */
    protected Fragment createFragment() {
        return StockSearchFragment.newInstance();
    }

    /* Request the creation of the stock search view by intent. */
    public static Intent searchIntent(Context context) {
        return new Intent(context, StockSearchActivity.class);
    }

    @Override
    /* Override point for handling the back button being pressed. */
    public void onBackPressed() {
        finish(); /* Finish the search activity. */
        /* Override the transition animation back to watched stocks. */
        overridePendingTransition(
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right);
    }

    /* The support toolbar can be hidden in landscape orientation to free up
     * space for fragment content. This method hides the toolbar in landscape
     * orientation and un-hides the toolbar otherwise. */
    protected void updateToolbarHiddenInLandscape() {
        if (getResources().getConfiguration().orientation ==
                Configuration.ORIENTATION_LANDSCAPE) {
            getSupportActionBar().hide(); /* Landscape orientation. */
        } else {
            getSupportActionBar().show(); /* Portrait orientation. */
        }
    }
}
