//
//  StockCompetitorsFragment.java
//  StockApp
//
//  A tab fragment containing a stock's competitors data if available.
//
//  Created by David McKnight on 2016-05-01.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.controller.detail;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import xyz.oswind.stockapp.DividerDecoration;
import xyz.oswind.stockapp.R;
import xyz.oswind.stockapp.model.StockDbHelper;
import xyz.oswind.stockapp.model.entities.Competitor;
import xyz.oswind.stockapp.model.entities.Status;

public class StockCompetitorsFragment extends StockDetailFragment {
    private static final String TAG = "StockDetailFragment";
    private RecyclerView mCompetitorList;/* A list from competitor data. */

    @LayoutRes
    @Override
    /* A tab containing a stock's competitor comparison. */
    protected int getLayoutResId() {
        return R.layout.fragment_stock_competitors;
    }

    @Override
    /* Override point for when creating the menu. */
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        /* Create a menu using the menu resource. */
        inflater.inflate(R.menu.menu_stock_status, menu);
        /* Only show saved icon on the status tab. */
        MenuItem saved = menu.findItem(R.id.menu_item_icon_saved);
        saved.setVisible(false);
        /* Show an icon when using cached competitors. */
        MenuItem cached = menu.findItem(R.id.menu_item_icon_cached);
        cached.setVisible(mStatusProvider.getStockCompetitors() != null);
    }

    @Override
    /* Override point for handling selected menu items. */
    public boolean onOptionsItemSelected(MenuItem item) {
        /* Check if the menu icon was clicked. */
        if (item.getItemId() == R.id.menu_item_icon_cached) {
            /* Show the same text as the icon long-press. Status is cached. */
            Toast msg = Toast.makeText(getActivity(),
                    R.string.icon_cached, Toast.LENGTH_SHORT);
            msg.setGravity(Gravity.TOP|Gravity.RIGHT, 0, 60);
            msg.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    /* Override point for when creating the fragment view. */
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View competitorView = super.onCreateView( /* See StockDetailFragment. */
                /* Apply common tab properties. */
                inflater, container, savedInstanceState);
        /* Configure a list to contain competitor data. */
        assert competitorView != null;
        mCompetitorList = (RecyclerView)competitorView
                .findViewById(R.id.fragment_stock_competitors_recycler_view);
        mCompetitorList.setLayoutManager(new LinearLayoutManager(getActivity()));
        /* Add a divider between list items. */
        mCompetitorList.addItemDecoration(new DividerDecoration(getActivity()));
        initialiseDataSource(); /* Prepare the view's comparison data. */
        /* Configure the attribution button. */
        configureAttributionInFragmentView(competitorView);

        return competitorView;
    }

    /* Link to Yahoo! per attribution requirements when using YQL or Yahoo!
     * APIs. Add an onClick listener to the Yahoo! image and load an implicit
     * intent to request URL be loaded by a browser. */
    private void configureAttributionInFragmentView(View competitorView) {
        /* Request the attribution image. */
        ImageView attributionImage = (ImageView)competitorView
                .findViewById(R.id.fragment_stock_competitors_attribution);
        /* Configure the image to load the attribution URL when touched. */
        attributionImage.setOnClickListener(new View.OnClickListener() {
            @Override
            /* Load the Yahoo! URL in a browser when the image is clicked. */
            public void onClick(View v) {
                /* Prepare an implicit intent to visit the Yahoo! URL. */
                Intent visitYahoo = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.yahoo.com/?ilc=401"));
                /* Load a browser and display the Yahoo! URL. */
                startActivity(visitYahoo);
            }
        });
    }

    /* Load a fragment's view content for the first time. */
    private void initialiseDataSource() {
        /* Check for cached competitor data to display. */
        if (mStatusProvider.getStockCompetitors() != null) {
            /* The view update modifies the competitor list. For saved data this
             * is fine as the list is regenerated from saved data. Make a copy
             * of the cached List to avoid modifying the cached List. */
            List<Competitor> cached = new ArrayList<>(mStatusProvider.getStockCompetitors());
            doViewUpdateWithCompetitors(cached); /* Use cache to update view. */
        } else {
            /* Check for saved competitor data. */
            List<Competitor> lastCompetitors = StockDbHelper.get(getActivity())
                    .getCompetitorsForSymbolId(mStock.getId());
            /* Update the view with saved data. */
            doViewUpdateWithCompetitors(lastCompetitors);
            /* Check the size of the competitor comparison table. */
            if (lastCompetitors.size() == 1) {
                /* There are no competitors for comparison. */
                Toast msg = Toast.makeText(getActivity(),
                        R.string.no_competitor_data, Toast.LENGTH_SHORT);
                msg.setGravity(Gravity.CENTER, 0, 0);
                msg.show();
            }
        }
    }

    /* Update the view using the competitor's list provided. The competitors
     * list will be modified during the update. During the update include the
     * current stock data for comparison. Set the stock as the first item. */
    private void doViewUpdateWithCompetitors(List<Competitor> competitors) {
        /* Request the stock status for comparison data. */
        Status status = StockDbHelper.get(getActivity())
                .getStatusForSymbolId(mStock.getId());
        /* Add the stock as the first item in the competitors list. */
        competitors.add(0, new Competitor(
                null, /* This is a temporary list and stock id is unused. */
                mStock.getStockSymbol(),
                mStock.getStockName(),
                /* If the status for this stock is still being processed the
                 * percent change and market cap data is unavailable. Show a
                 * placeholder if this is the case. Otherwise use the save
                 * status to create comparison data. */
                (status == null)? "N/A": status.getPercentChange(),
                (status == null)? "N/A": status.getMarketCap()));
        /* Create an adapter containing comparison data. */
        CompetitorAdapter competitorAdapter = new CompetitorAdapter(competitors);
        /* Set the competitor list to use the comparison data. */
        mCompetitorList.setAdapter(competitorAdapter);
    }

    /* A single competitor displayed in a list view for comparison. */
    private class CompetitorHolder extends RecyclerView.ViewHolder {
        private final TextView mSymbolView; /* Displayed stock symbol. */
        private final TextView mNameView;   /* Displayed stock name. */
        private final TextView mPctChgView; /* Displayed percent change. */
        private final TextView mMktCapView; /* Displayed market cap. */
        private final ImageView mChangeArrow; /* Contains a change indicator. */
        private Competitor mCompetitor; /* The displayed competitor. */

        /* Default constructor that caches view identifiers. */
        public CompetitorHolder(View itemView) {
            super(itemView);
            /* Cache the text view identifiers. */
            mChangeArrow = (ImageView)itemView.findViewById(R.id.list_item_competitor_pct_chg_arrow);
            mSymbolView = (TextView)itemView.findViewById(R.id.list_item_competitor_symbol);
            mNameView = (TextView)itemView.findViewById(R.id.list_item_competitor_name);
            mPctChgView = (TextView)itemView.findViewById(R.id.list_item_competitor_pct_chg);
            mMktCapView = (TextView)itemView.findViewById(R.id.list_item_competitor_mkt_cap);
        }

        /* Set the contents of this view holder to a competitor. */
        public void bindCompetitor(Competitor competitor) {
            mCompetitor = competitor; /* Hang onto the displayed data. */
            /* Fill in the competitor details. */
            mSymbolView.setText(mCompetitor.getSymbol());
            mNameView.setText(mCompetitor.getName());
            mMktCapView.setText(mCompetitor.getMarketCap());
            /* Check if percent change includes the +/- prefix. If it does use
             * prefix to update text color and arrow. */
            if (competitor.getPercentChange().startsWith("+") ||
                    competitor.getPercentChange().startsWith("-")) {
                if (competitor.getPercentChange().startsWith("+")) {
                    /* Found prefix +. Set view to positive change. */
                    mPctChgView.setTextColor(Color.rgb(0,150,0));
                    mChangeArrow.setBackgroundResource(R.drawable.up);
                } else if (competitor.getPercentChange().startsWith("-")) {
                    /* Found prefix -. Set view to negative change. */
                    mPctChgView.setTextColor(Color.RED);
                    mChangeArrow.setBackgroundResource(R.drawable.down);
                }
                /* Make sure the arrow is visible. */
                mChangeArrow.setVisibility(View.VISIBLE);
                /* The displayed change removes the prefix. */
                mPctChgView.setText(competitor.getPercentChange().substring(1));
            } else {
                /* Did not find the +/- prefix. Remove the arrow. */
                mChangeArrow.setVisibility(View.INVISIBLE);
                /* Use the change data as is. */
                mPctChgView.setText(competitor.getPercentChange());
            }
        }

        /* Activate highlighted for comparison. This is usually called on the
         * holder containing the stock for comparison. */
        public void setHighlighted() {
            itemView.setPressed(true); /* Activate comparison highlight. */
        }
    }

    /* An adapter containing competitor comparison data. */
    private class CompetitorAdapter
            extends RecyclerView.Adapter<CompetitorHolder> {
        final List<Competitor> mCompetitors; /* The comparison data. */

        /* Default constructor that prepares this adapter for use. */
        public CompetitorAdapter(List<Competitor> list) {
            /* If null is passed create an empty adapter. */
            mCompetitors = (list == null)? new ArrayList<Competitor>(): list;
            setHasStableIds(true); /* Enable animated view updates. */
        }

        @Override
        /* The item unique id based on position in comparison data. */
        public long getItemId(int position) {
            return position;
        }

        @Override
        /* The size of the comparison data. */
        public int getItemCount() {
            return mCompetitors.size();
        }

        @Override
        /* Create a view holder on demand. */
        public CompetitorHolder onCreateViewHolder(
                ViewGroup parent, int viewType) {
            /* Request a new view holder using the layout file. */
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View view = inflater.inflate(
                    R.layout.list_item_competitor, parent, false);
            return new CompetitorHolder(view);
        }

        @Override
        /* Bind a view holder instance to comparison data. */
        public void onBindViewHolder(CompetitorHolder holder, int position) {
            /* Request the comparison data being bound. */
            Competitor competitor = mCompetitors.get(position);
            /* Check if this is the first position containing the stock. */
            if (position == 0) {
                /* First position. Activate highlighting for comparison. */
                holder.setHighlighted();
                /* For simplicity set the first position non-recyclable.*/
                holder.setIsRecyclable(false); /* Highlighting persists. */
            }
            /* Bind the comparison data to the view holder. */
            holder.bindCompetitor(competitor);
        }
    }
}
