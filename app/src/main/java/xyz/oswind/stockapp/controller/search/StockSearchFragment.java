//
//  StockSearchFragment.java
//  StockApp
//
//  The controller for a fragment responsible for searching for stocks. Submits
//  the query as required to a StockSearch object and responds to search results
//  by updating the current view.
//
//  Created by David McKnight on 2016-04-22.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.controller.search;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.List;

import xyz.oswind.stockapp.DividerDecoration;
import xyz.oswind.stockapp.R;
import xyz.oswind.stockapp.Reachability;
import xyz.oswind.stockapp.SingleFragmentActivity;
import xyz.oswind.stockapp.contentprovider.search.StockSearch;
import xyz.oswind.stockapp.contentprovider.search.StockSearchDelegate;
import xyz.oswind.stockapp.StockQueryDecoration;
import xyz.oswind.stockapp.model.entities.Stock;
import xyz.oswind.stockapp.model.StockDbHelper;

public class StockSearchFragment extends Fragment
        implements Reachability.Callbacks {
    private static final String TAG = "StockSearchFragment";
    /* The identifier for the state of total results found. */
    private static final String TOTAL_STATE = "TotalState";
    private String mLastQuery;                   /* The last query string. */
    private RecyclerView mSearchResultsList;     /* List of found stocks. */
    private ResultsAdapter mSearchResultsAdapter;/* Found stock adapter. */
    private SearchView mSearchView;        /* The stock search text box. */
    private StockSearch mSearchProvider;   /* Stock search results provider. */
    private ProgressBar mActivityIndicator;/* Activity searching. */
    private ProgressBar mTotalIndicator;   /* Activity checking totals. */
    private TextView mTotalTextView;       /* Total possible matches. */
    private boolean mDidGetMoreResultsMessage; /* More matches found. */
    private boolean mMoreResultsIsBusy; /* Checking for more matches. */
    private boolean mHasAllResults;     /* Already caching all matches. */
    private int mPreviousResultCount;   /* Count before more matches loaded. */

    /* Create a new instance of a StockSearchFragment. */
    public static StockSearchFragment newInstance() {
        return new StockSearchFragment();
    }

    @Override
    /* Override point for when the fragment is created. */
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /* Retain this fragment. This means the fragment will not be destroyed
         * during a configuration change. Instead it will be detached from the
         * current activity then attached to a new activity. This will recreate
         * views but allow the current search state to persist. */
        setRetainInstance(true);
        /* Configure the data source for list views and search results. */
        configureListViewDataSource();
        resetSearchDefaults(); /* Set the default internal state. */
        /* Start observing reachability events. */
        Reachability.get(getActivity()).addObserver(this);
    }

    @Override
    /* A reachability broadcast has been received. */
    public void onReachabilityUpdate() {
        if (mSearchView != null) {
            /* Update the search text box to show updated network state. */
            updateViewWithReachability(
                    /* Check the network state. */
                    Reachability.get(getActivity()).isNetworkOnline());
        }
    }

    /* Update the view depending on the parameter `isOnline`. Show a change in
     * the view to indicate if search functionality is available. */
    private void updateViewWithReachability(boolean isOnline) {
        mSearchView.setEnabled(isOnline);
        mSearchView.setFocusableInTouchMode(isOnline);
        if (!isOnline) {
            /* No network detected. Update search hint, and clear the focus. */
            mSearchView.setQueryHint(getString(R.string.query_offline));
            mSearchView.setQuery("", false);
            mSearchView.clearFocus();
        } else {
            /* App is online. Show the default search hint. */
            mSearchView.setQueryHint(getString(R.string.query_placeholder));
        }
    }

    @Override
    /* Override point for when the fragment view state is being stashed. */
    public void onSaveInstanceState(Bundle outState) {
        /* Save the total results view state. If the view is being destroyed due
         * to a configuration change restore this value. This is an alternative
         * to searching again, or simply clearing the last query. */
        outState.putCharSequence(TOTAL_STATE, mTotalTextView.getText());
        super.onSaveInstanceState(outState);
    }

    @Override
    /* Override point for when the fragment view state has been restored. */
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        /* When restoring view state also set the total results view state. */
        if (savedInstanceState != null && mTotalTextView != null) {
            mTotalTextView.setText(
                    savedInstanceState.getCharSequence(TOTAL_STATE));
        }
    }

    @Nullable
    @Override
    /* Override point for when the fragment's view is created. */
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        /* Create a stock search view using the fragment's layout. */
        View stockSearch = inflater.inflate(
                R.layout.fragment_stock_search, container, false);
        /* Cache the fragment's activity and total indicators. */
        cacheIndicatorsInFragmentView(stockSearch);
        /* Configure the search view appearance for min-API 16+. */
        configureSearchInFragmentView(stockSearch);
        /* Configure a link to Yahoo! per attribution requirements. */
        configureAttributionInFragmentView(stockSearch);
        configureNavigationUp(); /* Add a navigation up option. */
        /* Configure the list view appearance and behaviour. */
        configureListInFragmentView(stockSearch);
        /* Configure the segmented control for selecting market. */
        configureMarketSelectorInFragmentView(stockSearch);
        /* Hide the toolbar if the device is in landscape orientation. */
        StockSearchActivity activity = (StockSearchActivity)getActivity();
        activity.updateToolbarHiddenInLandscape();

        return stockSearch;
    }

    @Override
    /* Override point for handling selected menu items. */
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            /* Navigate up was selected. */
            getActivity().finish();
            /* Override the transition animation back to watched stocks. */
            getActivity().overridePendingTransition(
                    android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    /* Override point for destroying a fragment. */
    public void onDestroy() {
        super.onDestroy();
        mSearchProvider.resetSearch();     /* Stop pending search queries. */
        mSearchProvider.setDelegate(null); /* Remove search result handler. */
        /* Stop observing reachability events. */
        Reachability.get(getActivity()).removeObserver(this);
    }

    /* Cache the activity and total results indicator. */
    private void cacheIndicatorsInFragmentView(View fragmentView) {
        /* Request the activity indicator for search. */
        mActivityIndicator = (ProgressBar)fragmentView
                .findViewById(R.id.fragment_stock_search_active);
        /* Request the activity indicator for total results. */
        mTotalIndicator = (ProgressBar)fragmentView
                .findViewById(R.id.fragment_stock_search_total_active);
        /* Request the text view to indicate total results found. */
        mTotalTextView = (TextView)fragmentView
                .findViewById(R.id.fragment_stock_search_total_text);
    }

    /* Add the navigation up pattern to the support toolbar. */
    private void configureNavigationUp() {
        /* Cast to enable getSupportActionBar() invocation. The invocation of
         * getActivity in a support fragment includes an explicit cast to a
         * FragmentActivity (which lacks getSupportActionBar). Since all
         * activities inherit from SingleFragmentActivity, which itself inherits
         * from AppCompatActivity, this should be okay. */
        SingleFragmentActivity activity = (SingleFragmentActivity)getActivity();
        /* Request the support toolbar. */
        ActionBar supportActionBar = activity.getSupportActionBar();
        /* Check for null to prevent some code lint warnings. */
        if (supportActionBar != null) {
            /* Add the up navigation option. */
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            setHasOptionsMenu(true);
        }
    }

    /* Configure the appearance of the search/filter box in the provided view.
     * This means setting the initial appearance and focused indicator as
     * required. This is done in code to ensure a consistent appearance on pre-
     * lollipop devices. If backwards compatibility and/or consistency is not
     * required this can instead be performed on API 21+ using two XML
     * attributes as described in the layout file for this fragment. */
    private void configureSearchInFragmentView(View fragmentView) {
        /* Request the fragment's search view. */
        mSearchView = (SearchView)fragmentView
                .findViewById(R.id.fragment_stock_search_query_view);
        /* Configure the cursor which is shown in the search view. */
        configureCursorInSearchView(mSearchView);
        /* Configure the search view to have a visual indicator on the contained
         * query text view layout depending on the current focused state. */
        mSearchView.setOnQueryTextFocusChangeListener(
                new StockQueryDecoration(getActivity(), mSearchView));
        /* Configure the search view to submit search queries when needed. */
        mSearchView.setOnQueryTextListener(new StockSearchTextListener());
        /* Check if the app is online. Update the view if offline. */
        if (!Reachability.get(getActivity()).isNetworkOnline()) {
            updateViewWithReachability(false); /* Offline. */
        }
    }

    /* Turn off the blinking cursor in the provided search view. */
    private void configureCursorInSearchView(SearchView searchView) {
        /* Request the id of text edit view contained in the search view. */
        int textEditViewId = searchView
                .getResources() /* Request the generated resources. */
                /* Request the contained text edit view's id. */
                .getIdentifier("android:id/search_src_text", null, null);
        /* Request the text edit view contained in the search view. */
        AutoCompleteTextView textEditView = (AutoCompleteTextView)
                searchView.findViewById(textEditViewId);
        /* Turn off the cursor in the text edit view. */
        textEditView.setCursorVisible(false);
    }

    /* Link to Yahoo! per attribution requirements when using YQL or Yahoo!
     * APIs. Add an onClick listener to the Yahoo! image and load an implicit
     * intent to request URL be loaded by a browser. */
    private void configureAttributionInFragmentView(View fragmentView) {
        /* Request the attribution image. */
        ImageView attributionImage = (ImageView)fragmentView
                .findViewById(R.id.fragment_stock_search_attribution);
        /* Configure the image to load the attribution URL when touched. */
        attributionImage.setOnClickListener(new View.OnClickListener() {
            @Override
            /* Load the Yahoo! URL in a browser when the image is clicked. */
            public void onClick(View imageView) {
                /* Prepare an implicit intent to visit the Yahoo! URL. */
                Intent visitYahoo = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.yahoo.com/?ilc=401"));
                /* Load a browser and display the Yahoo! URL. */
                startActivity(visitYahoo);
            }
        });
    }

    /* Configure the list used to display search results. This means preparing
     * the RecyclerView's layout, adding the divider decoration, and adding a
     * listener to detect when near the end of currently displayed results. */
    private void configureListInFragmentView(View fragmentView) {
        /* Request the list's recycler view. */
        mSearchResultsList = (RecyclerView)fragmentView
                .findViewById(R.id.fragment_stock_search_recycler_view);
        /* Configure the layout as a linear, vertical list. */
        mSearchResultsList.setLayoutManager(
                new LinearLayoutManager(getActivity()));
        /* Add a divider between list items. */
        mSearchResultsList.addItemDecoration(
                new DividerDecoration(getActivity()));
        /* Set the adapter to use in binding view holders to the data source. */
        mSearchResultsList.setAdapter(mSearchResultsAdapter);
        /* Add a scroll listener to load more results when near the end. */
        mSearchResultsList.addOnScrollListener(
                new RecyclerView.OnScrollListener() {
            static final int POSITION_FROM_END = 5; /* Threshold from end. */
            @Override
            /* Override point for handling the scroll event. */
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                /* Request the linear layout manager previously set. */
                LinearLayoutManager layoutManager = (LinearLayoutManager)
                        recyclerView.getLayoutManager();
                /* Request the total count of displayable items. */
                int listItemCount = layoutManager.getItemCount();
                /* Request the last displayable position (zero-based). */
                int visibleIndex = layoutManager.findLastVisibleItemPosition();
                /* Check if the delegate message didGetTotalResultsForLastQuery
                 * has been sent with more results. If FALSE then it is known
                 * that no more results exist. If TRUE then check for more as
                 * the table scrolls. */
                if (mDidGetMoreResultsMessage && /* Got more results message. */
                        !mHasAllResults &&     /* Not displaying all results. */
                        !mMoreResultsIsBusy && /* Not waiting on new results. */
                        /* An optional check if more results exist. This can be
                         * omitted as StockSearch will guard against another
                         * query. This allows the scroll to fail-fast. */
                        mSearchProvider.getTotalResults() >= listItemCount &&
                        /* Check if scrolled near end of current results. */
                        visibleIndex > listItemCount - POSITION_FROM_END) {
                    /* Show the activity indicator. */
                    mActivityIndicator.setVisibility(View.VISIBLE);
                    mMoreResultsIsBusy = true; /* Block overlapping requests. */
                    /* Keep the current number of results for a batch update. */
                    mPreviousResultCount = mSearchProvider.getResultCount();
                    /* The list view is scrolled near the end. Request more
                     * results and update the list view with the new rows. */
                    mSearchProvider.requestNextResults();
                }
            }
        });
    }

    /* Configure the data source used to fill the list of search result. This
     * means creating a results adapter object, creating a search provider, and
     * setting adapter as the delegate for search provider messages. */
    private void configureListViewDataSource() {
        /* Create the adapter to use as a data source. */
        mSearchResultsAdapter = new ResultsAdapter();
        /* Create a stock search provider. */
        mSearchProvider = new StockSearch();
    }

    /* Configure the segmented control used to select a markets to search. This
     * means adding a common listener to each option. When a market is selected
     * pass the configuration change onto the StockSearch object. */
    private void configureMarketSelectorInFragmentView(View fragmentView) {
        /* Create a listener for selecting a market to search. */
        View.OnClickListener marketSelectionListener =
                new View.OnClickListener() {
            @Override
            /* Handle the click on the provided segmented control view. */
            public void onClick(View button) {
                /* Check which market button was selected. */
                switch (button.getId()) {
                    case R.id.fragment_stock_search_market_all:
                        /* All markets was selected. */
                        mSearchProvider.setQueryWorldMarkets(true);
                        break;
                    case R.id.fragment_stock_search_market_local:
                        /* Local markets was selected. */
                        mSearchProvider.setQueryWorldMarkets(false);
                        break;
                }
                /* If the search bar contains text re-submit the search. */
                requestSearchWithQuery(mSearchView.getQuery().toString());
            }
        };
        /* Request the `all markets` segmented control. */
        RadioButton marketAll = (RadioButton)fragmentView
                .findViewById(R.id.fragment_stock_search_market_all);
        /* Request the `local markets` segmented control. */
        RadioButton marketLocal = (RadioButton)fragmentView
                .findViewById(R.id.fragment_stock_search_market_local);
        /* Configure the onClick listener for selecting all markets. */
        marketAll.setOnClickListener(marketSelectionListener);
        /* Configure the onClick listener for selecting local markets. */
        marketLocal.setOnClickListener(marketSelectionListener);
    }

    /* Reset any search state defaults. Most likely this would occur for new
     * stock symbol search requests. These properties are intentionally part of
     * the fragment so that the state can persist with the retained fragment. */
    private void resetSearchDefaults() {
        mDidGetMoreResultsMessage = false;
        mMoreResultsIsBusy = false;
        mHasAllResults = false;
        mPreviousResultCount = 0;
    }

    /* Submit a query string to search. The parameter `query` contains the
     * partial, or whole string of a stock symbol/name/description. This method
     * is intentionally part of the fragment so that search can be triggered
     * from the search view and segmented market buttons. */
    private void requestSearchWithQuery(String query) {
        if (!query.equals("")) {
            /* Start the activity indicator. */
            mActivityIndicator.setVisibility(View.VISIBLE);
            resetSearchDefaults(); /* Reset defaults. */
            /* Request the search results for the query submitted. */
            mSearchProvider.performSearchWithString(query);
        }
    }

    /* A class that represents a single stock search result displayed in a list
     * view item. */
    private class ResultHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        private final TextView mSymbolView; /* Displayed stock symbol. */
        private final TextView mNameView;   /* Displayed stock name. */
        private Stock mStock; /* The currently displayed stock. */

        /* Default constructor that caches view identifiers. */
        public ResultHolder(View itemView) {
            super(itemView);
            /* Cache the text view identifiers. */
            mSymbolView = (TextView)itemView
                    .findViewById(R.id.list_item_result_symbol);
            mNameView = (TextView)itemView
                    .findViewById(R.id.list_item_result_name);
        }

        /* Set the contents of this view holder to a provided search result. */
        public void bindResult(Stock result) {
            mStock = result; /* Hang onto the currently displayed stock. */
            /* Add a listener to save the stock when clicked. */
            itemView.setOnClickListener(this);
            /* Fill in the stock details. */
            mSymbolView.setText(mStock.getStockSymbol()); /* Set the symbol. */
            mNameView.setText(mStock.getStockName());     /* Set the name. */
        }

        @Override
        /* Override point for handling a click on the view holder. */
        public void onClick(View v) {
            mSearchView.clearFocus(); /* Allow keyboard to be dismissed. */
            /* Persist the stock contained in this view. */
            StockDbHelper.get(getActivity()) /* Using the shared db helper. */
                    .addWatched(mStock);     /* Add a watched stock. */
            getActivity().finish(); /* Return to watched stocks view. */
            /* Override the transition animation back to watched stocks. */
            getActivity().overridePendingTransition(
                    android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right);
        }
    }

    /* A class that represents the data source binding for a list view. It also
     * maintains view holders and reuses them where possible. The adapter will
     * rebind the list items to the search results as required. */
    private class ResultsAdapter extends RecyclerView.Adapter<ResultHolder> {
        /* A default constructor that prepares this adapter for use. */
        public ResultsAdapter() {
            setHasStableIds(true); /* Enable animated view updates. */
        }

        @Override
        /* Override point for a data source item's id. Required to support
         * animated view updates. Must be unique. The position of an item in the
         * list of stocks represents a unique identifier (albeit transient). */
        public long getItemId(int position) {
            return position;
        }

        @Override
        /* Override point when creating a new view holder. */
        public ResultHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            /* Request a new view holder using the layout file. */
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View view = inflater.inflate(
                    R.layout.list_item_search, parent, false);
            return new ResultHolder(view); /* Supports reuse. */
        }

        @Override
        /* Override point when setting the content of a view holder. */
        public void onBindViewHolder(ResultHolder holder, int position) {
            /* Extract the search result to be shown in this view holder. */
            Stock result = mSearchProvider.getStockAtIndex(position);
            /* Set the view holder content. */
            holder.bindResult(result);
        }

        @Override
        /* Override point for the count of items in the data source. */
        public int getItemCount() {
            /* Make sure the results are available. */
            if (mSearchProvider.getSearchResults() == null) {
                return 0; /* No results to display. */
            }
            /* Total rows in the data source. */
            return mSearchProvider.getResultCount();
        }
    }

    private class StockSearchTextListener
            implements SearchView.OnQueryTextListener, StockSearchDelegate {
        /* A default constructor which sets this instance as the delegate for
         * search completion events. */
        public StockSearchTextListener() {
            mSearchProvider.setDelegate(this);
        }

        @Override
        /* Override point for handling the submit button pressed event. Nothing
         * needs to be done here as the query has already been submitted as
         * entered. */
        public boolean onQueryTextSubmit(String query) {
            mSearchView.clearFocus();
            return false; /* Indicate the query has been handled. */
        }

        @Override
        /* Override point for handling when the query text changes. Submit the
         * changed query for search results. */
        public boolean onQueryTextChange(String query) {
            /* Submit a query if it's not the same as the last one. The property
             * mLastQuery must exist in the containing fragment (cannot be
             * local) since it is used for persistence during configuration
             * changes. It could also be made a feature of StockSearch. */
            if (!query.equals(mLastQuery)) {
                mLastQuery = query; /* Update the last query submitted. */
                requestSearchWithQuery(query); /* Do the search. */
            }
            return true; /* Indicate the query has been handled. */
        }

        @Override
        /* A query which was previously submitted has completed. The parameter
         * `symbols` contains the results as Stock objects. */
        public void searchDidCompleteWithSymbols(final List<Stock> symbols) {
            /* Schedule updates on the application main thread. */
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    /* Update the list with the latest results. */
                    mSearchResultsAdapter.notifyDataSetChanged();
                    /* Re-position the list to the top. */
                    if (symbols.size() > 0) {
                        mSearchResultsList.smoothScrollToPosition(0);
                    }
                    /* Stop the activity indicator for search. */
                    mActivityIndicator.setVisibility(View.INVISIBLE);
                    /* Start the activity indicator for total results. */
                    mTotalIndicator.setVisibility(View.VISIBLE);
                }
            });
        }

        @Override
        /* A query which was previously submitted did not complete. The
         * parameter `error` contains the cause of the failure. */
        public void searchDidNotCompleteWithError(Throwable error) {
            Log.e(TAG,"A session failed with exception: " + error.getClass() +
                    ", and description: " + error.getMessage());
            /* Schedule updates on the application main thread. */
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    /* Stop activity indication. */
                    mActivityIndicator.setVisibility(View.INVISIBLE);
                    mTotalIndicator.setVisibility(View.INVISIBLE);
                }
            });
        }

        @Override
        /* A query which was previously submitted already has a total number of
         * results. This message will be sent when the total number of results
         * is less than the maximum returned per request. For Yahoo this is
         * defined in StockSearch as YAHOO_MAX_PER_REQUEST. The symbols
         * previously returned represent the total of results found for the
         * last query. */
        public void didAlreadyGetTotalResultsForLastQuery() {
            /* Schedule updates on the application main thread. */
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    /* Prepare the total string for display. */
                    String totalText = getString(R.string.total_results,
                            mSearchProvider.getTotalResults());
                    mTotalTextView.setText(totalText); /* Display the total. */
                    /* Stop the total results activity indicator. */
                    mTotalIndicator.setVisibility(View.INVISIBLE);
                }
            });
        }

        @Override
        /* A request for more results has completed without any changes to the
         * property mSearchResults associated with this instance of StockSearch.
         * Most likely this means all the symbols are already loaded and any
         * request for more is out-of-bounds. Interested controllers can use
         * this to indicate no further stock symbols are available to be
         * displayed.*/
        public void moreResultsDidComplete() {
            /* Schedule updates on the application main thread. */
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    /* Further requests would not return anything new. */
                    mHasAllResults = true;
                    onMoreResults(); /* Display latest results. */
                }
            });
        }

        /* When more results are available update the user interface to include
         * the latest results. */
        private void onMoreResults() {
            mMoreResultsIsBusy = false; /* Re-enable more results check. */
            /* Check how many results have now been processed. */
            int currentResultCount = mSearchProvider.getResultCount();
            /* Check if new rows need to be inserted. */
            if (currentResultCount > mPreviousResultCount) {
                /* Insert the new rows. */
                int addedItems = currentResultCount - mPreviousResultCount;
                mSearchResultsAdapter.notifyItemRangeInserted(
                        mPreviousResultCount, addedItems);
            }
            /* Stop the activity indicator. */
            mActivityIndicator.setVisibility(View.INVISIBLE);
        }

        @Override
        /* A query which was previously submitted now has a total number of
         * results. This may exceed the number of results previously seen when
         * receiving the searchDidCompleteWithSymbols message. This message will
         * be sent when the total number of results exceeds the maximum returned
         * per request. For Yahoo this is defined in StockSearch as
         * YAHOO_MAX_PER_REQUEST. */
        public void didGetTotalResultsForLastQuery(final String total) {
            /* Schedule updates on the application main thread. */
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    /* Prepare the total string for display. */
                    String totalText = getString(R.string.total_results, total);
                    mTotalTextView.setText(totalText); /* Display the total. */
                    /* Validate the more results message. */
                    if (mSearchProvider.getTotalResults() >
                            StockSearch.YAHOO_MAX_PER_REQUEST) {
                        mDidGetMoreResultsMessage = true; /* Validated. */
                    }
                    /* Stop the total results activity indicator. */
                    mTotalIndicator.setVisibility(View.INVISIBLE);
                }
            });
        }

        @Override
        /* A request for more results has completed. The getter
         * getSearchResults() associated with this instance of StockSearch
         * reflects the current view of total symbols. The updated view of total
         * symbols includes all previous symbols with new symbols appended to
         * the end. Interested controllers should update their view with the
         * new entries. */
        public void moreResultsDidCompleteWithMore() {
            /* Schedule updates on the application main thread. */
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    onMoreResults(); /* Display latest results. */
                }
            });
        }
    }
}
