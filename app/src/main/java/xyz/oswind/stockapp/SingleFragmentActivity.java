//
//  SingleFragmentActivity.java
//  StockApp
//
//  An activity which will contain a single view fragment. This is expected to
//  use the AppCompat Library for activities and hosted fragments.
//
//  Created by David McKnight on 2016-04-15.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

public abstract class SingleFragmentActivity extends AppCompatActivity {

    /* Create a fragment containing the content to be displayed. */
    protected abstract Fragment createFragment();

    @LayoutRes
    /* Obtain the layout resource id. */
    protected int getLayoutResId() {
        return R.layout.activity_single_fragment;
    }

    @Override
    /* Override point for when the activity is created. */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /* Set the content view using the layout resource. */
        setContentView(getLayoutResId());
        /* Prepare the toolbar for use. */
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        /* Request the support fragment manager. */
        FragmentManager fm = getSupportFragmentManager();
        /* Check if the content already exists. */
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);
        if (fragment == null) {
            /* The fragment does not already exist. */
            fragment = createFragment();
            /* Add the fragment containing the content to be displayed. */
            fm.beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();
        }
    }
}
