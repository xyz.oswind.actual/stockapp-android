//
//  StockStatusDelegate.java
//  StockApp
//
//  Declares the protocol used when a controller is interested in symbol status
//  events. Such a controller would be interested in completed requests for
//  stock status and any errors that may occur.
//
//  Created by David McKnight on 2016-04-25.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

package xyz.oswind.stockapp.contentprovider.status;

import xyz.oswind.stockapp.model.entities.Status;

public interface StockStatusDelegate {
    /* A status query which was previously submitted has now completed.
     * Controllers can now update their views with the StockStatus. */
    void statusDidCompleteWith(Status status);

    /* A status query which was previously submitted did not complete. The
     * parameter `error` contains the cause of the failure. */
    void statusDidNotCompleteWithError(Throwable error);

    /* A chart query which was previously submitted did not complete. The
     * parameter `error` contains the cause of the failure. */
    void chartDidNotRetrieveWithError(Throwable error);

    /* A competitor query which was previously submitted did not complete. The
     * parameter `error` contains the cause of the failure. */
    void competitorsNotRetrievedWithError(Throwable error);
}
