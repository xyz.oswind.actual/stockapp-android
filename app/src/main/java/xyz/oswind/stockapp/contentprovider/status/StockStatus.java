//
//  StockStatus.java
//  StockApp
//
//  An object that performs symbol status checks and notifies interested
//  controllers when this task is completed using the StockStatusDelegate
//  protocol. The connections are performed asynchronously using AsyncTask.
//  StockStatus is a factory producing Status entities from the response data
//  produced during status update requests.
//
//  -- Implementation Notes ----------------------------------------------------
//  This class uses Yahoo! Finance's API for obtaining current stock status. The
//  Yahoo URL used is:
//  ""download.finance.yahoo.com/d/quotes.csv?
//                                  s=QUERY&f=snc4c1p2pobat8mwva2j1d1t1&e=.csv""
//  where `s=QUERY` is the symbol status requested, and the string following
//  `&f=` represents API tags for the columns of interest. These tags are found
//  from many sources online, but not documented in any official way (so are
//  subject to change).
//
//  The results are converted to JSON using YQL:
//  https://developer.yahoo.com/yql/ . Where the select string used is:
//      select * from csv where url='YAHOO_URL_ABOVE' and
//  columns='symbol,desc,currency,change,percent_change,prev_close,open,bid,ask,
//  year_target_est,days_range,year_range,volume,avg_volume,market_cap,
//  last_trade_date,last_trade_time'
//
//  For reference all columns can be retrieved using the select string:
//      select * from yahoo.finance.quotes where symbol='QUERY'
//
//  Finally, once the JSON object is returned, it is converted to Foundation
//  objects by selecting the appropriate keys or key paths. In general this
//  means less data is read by the iOS device and less local processing is
//  required. Thereby reducing expensive wireless radio use and cpu time.
//  ----------------------------------------------------------------------------
//
//  Created by David McKnight on 2016-04-25.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

package xyz.oswind.stockapp.contentprovider.status;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

import xyz.oswind.stockapp.model.StockDbHelper;
import xyz.oswind.stockapp.model.entities.Chart;
import xyz.oswind.stockapp.model.entities.Competitor;
import xyz.oswind.stockapp.model.entities.Status;

@SuppressWarnings("UnusedAssignment")
public class StockStatus {
    private static final String TAG = "StockStatus";
    /* Supported session types. */
    private static final int STATUS_SESSION_TYPE = 0;
    private static final int CHART_SESSION_TYPE = 1;
    private static final int COMPETITOR_SESSION_TYPE = 2;
    /* Local constants declaring the partial strings of a stock status query. */
    private static final String STATUS_URL_START = "https://query.yahooapis.c" +
            "om/v1/public/yql?q=select%20*%20from%20csv%20where%20url%3D'http" +
            "%3A%2F%2Fdownload.finance.yahoo.com%2Fd%2Fquotes.csv%3Fs%3D";
    private static final String STATUS_URL_ENDING = "%26f%3Dsnc4c1p2pobat8mwv" +
            "a2j1d1t1%26e%3D.csv'%20and%20columns%3D'symbol%2Cdesc%2Ccurrency" +
            "%2Cchange%2Cpercent_change%2Cprev_close%2Copen%2Cbid%2Cask%2Cyea" +
            "r_target_est%2Cdays_range%2Cyear_range%2Cvolume%2Cavg_volume%2Cm" +
            "arket_cap%2Clast_trade_date%2Clast_trade_time'&format=json&env=s" +
            "tore%3A%2F%2Fdatatables.org%2Falltableswithkeys";
    /* Local constants declaring the partial strings of a stock chart query. */
    private static final String CHART_URL_START =
            "https://chart.finance.yahoo.com/z?s=";
    private static final String CHART_URL_ENDING = "&t=1d&q=l&l=on&z=l&p=s";
    /* Local constants declaring the partial strings of a competitors query. */
    private static final String COMPETITOR_URL_START =
            "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20" +
            "html%20where%20url%3D'https%3A%2F%2Fca." +
            "finance.yahoo.com%2Fq%3Fs%3D";
    private static final String COMPETITOR_URL_ENDING =
            "'%20and%20xpath%3D'%2F%2Fdiv%5Bcontains(%40id%2C%22yfi_compariso" +
            "n%22)%5D%2F%2Fdiv%5Bcontains(%40class%2C%22bd%22)%5D%2F%2Ftable%" +
            "2Ftbody%2F*'&format=json&env=store%3A%2F%2Fdatatables." +
            "org%2Falltableswithkeys";
    private static StockStatus sStockStatus; /* App static StockStatus. */
    private static Context sContext;         /* Static in the app's context.*/
    private volatile Long mId;               /* The id of the watched stock. */
    private volatile Status mStockStatus;    /* Cached data. */
    private volatile Chart mStockChart;      /* Cached data. */
    private volatile List<Competitor> mStockCompetitors; /* Cached data. */
    private StatusTask mStatus; /* Task obtaining the status data. */
    private StatusTask mChart;  /* Task obtaining the chart data. */
    private StatusTask mCompetitors; /* Task obtaining the competitors data. */
    private volatile boolean mIsStatusDone;      /* Status data ready. */
    private volatile boolean mIsChartDone;       /* Chart data is ready. */
    private volatile boolean mIsCompetitorsDone; /* Competitor data is ready. */
    private StockStatusDelegate mDelegate; /* Receives completion messages. */
    /* Indicates the last status request has completed. This will be true as
     * soon the Status is usable. If this is true after submitting a status
     * update request the Status is also cached. */
    private volatile boolean mDidFinishStatusUpdate;

    /* Shared StockStatus singleton access. */
    public static StockStatus get(Context context) {
        /* Check if the StockStatus already exists. */
        if (sStockStatus == null) {
            sContext = context.getApplicationContext();
            sStockStatus = new StockStatus(); /* Create the StockStatus. */
        }
        return sStockStatus; /* Return the context-shared StockStatus. */
    }

    /* A default constructor with last status completed and no cached data. */
    private StockStatus() {
        mDidFinishStatusUpdate = true;
        resetInternalState();
    }

    /* Check if the last status requested has completed. */
    public boolean isCompletedStatusUpdate() {
        return mDidFinishStatusUpdate;
    }

    /* Check if all status data is available. */
    private boolean isDone() {
        return (mIsStatusDone && mIsChartDone && mIsCompetitorsDone);
    }

    /* Set the stock id of a Status produced from status update requests to the
     * parameter stockId. This is an optional convenience setter used when the
     * Status is to be automatically saved. When the stockId has been set before
     * making status update requests the Status will be saved to the app-shared
     * SQLite database. When set null the Status will not be saved to the
     * database. */
    public void setStockId(Long stockId) { mId = stockId; }

    /* Request the Status from the the last update request. */
    public Status getStockStatus() {
        return isDone()? mStockStatus:null;
    }

    /* Request the Chart from the last update request. */
    public Chart getStockChart() {
        return isDone()? mStockChart:null;
    }

    /* Request the Competitors from the last update request. */
    public List<Competitor> getStockCompetitors() {
        return isDone()? mStockCompetitors:null;
    }

    /* Set the delegate to handle status request completion. */
    public void setDelegate(StockStatusDelegate delegate) {
        mDelegate = delegate;
    }

    /* Reset the StockStatus. The next status update will not automatically save
     * unless the stock id is again set. When the parameter `interrupt` is true
     * incomplete status updates should be interrupted. Otherwise allow the
     * tasks to complete normally. */
    public void reset() {
        resetInternalState();
        reset(false);
    }

    /* Reset the StockStatus for another status update request. The next status
     * update will save using a previously set stock id. When the parameter
     * `interrupt` is true incomplete status updates should be interrupted.
     * Otherwise allow the tasks to complete. */
    private void reset(boolean interrupt) {
        resetTasks(interrupt);
        resetProcessedState();
        resetCache();
        /* Last status completed and no cached data. */
        mDidFinishStatusUpdate = true;
    }

    /* Reset the tasks performing status update related processing. */
    private void resetTasks(boolean interrupt) {
        if (mStatus != null &&
                mStatus.getStatus() == AsyncTask.Status.RUNNING) {
            mStatus.cancel(interrupt);
        }
        if (mChart != null &&
                mChart.getStatus() == AsyncTask.Status.RUNNING) {
            mChart.cancel(interrupt);
        }
        if (mCompetitors != null &&
                mCompetitors.getStatus() == AsyncTask.Status.RUNNING) {
            mCompetitors.cancel(interrupt);
        }
    }

    /* Reset the StockStatus cache. The next read will encounter null. */
    private void resetCache() {
        mStockStatus = null;
        mStockChart = null;
        mStockCompetitors = null;
    }

    /* Convenience method which accepts one parameter `symbol`. The symbol
     * provided must exist otherwise this method will not return any useful
     * information. The symbol provided is used to obtain the current status of
     * the stock. Once the current status is parsed the stock status becomes
     * available for use with delegate notification. Delegate controllers must
     * have implemented the StockStatusDelegate protocol. */
    public void getStatusWithSymbol(String symbol) {
        /* Guard against status lookup with empty symbols. */
        if (symbol != null && !symbol.equals("")) {
            reset(true); /* Reset the StockStatus for a new request. */
            mDidFinishStatusUpdate = false; /* Update completion state. */
            /* Begin processing the status update request. */
            mStatus = new StatusTask(symbol, STATUS_SESSION_TYPE);
            mChart = new StatusTask(symbol, CHART_SESSION_TYPE);
            mCompetitors = new StatusTask(symbol, COMPETITOR_SESSION_TYPE);
        }
    }

    /* Get an URL corresponding to a current status lookup. The parameter
     * `symbol` should contain a valid stock symbol. No checks for validity of
     * the symbol are performed. */
    private URL getURLForStatus(String symbol) throws MalformedURLException {
        /* The string representation of the URL. */
        String urlString = STATUS_URL_START + symbol + STATUS_URL_ENDING;
        return new URL(urlString);
    }

    /* Get an URL corresponding to a current chart lookup. The parameter
     * `symbol` should contain a valid stock symbol. No checks for validity of
     * the symbol are performed. */
    private URL getURLForChart(String symbol) throws MalformedURLException {
        /* The string representation of the URL. */
        String urlString = CHART_URL_START + symbol + CHART_URL_ENDING;
        return new URL(urlString);
    }

    /* Get an URL corresponding to a current competitors lookup. The parameter
     * `symbol` should contain a valid stock symbol. No checks for validity of
     * the symbol are performed. */
    private URL getURLForCompetitors(String symbol)
            throws MalformedURLException {
        /* The string representation of the URL. */
        String urlString = COMPETITOR_URL_START +
                symbol + COMPETITOR_URL_ENDING;
        return new URL(urlString);
    }

    private Status getNoResultFoundStatus() {
        /* Return an empty status when no results were found. */
        return new Status(null, "N/A", "N/A", "N/A", "N/A", "N/A",
                "N/A", "N/A", "N/A", "N/A", "N/A", "N/A", "N/A",
                "N/A", "N/A", "N/A", null, null);
    }

    /* Create an HttpsURLConnection using the URL passed. Any existing status
     * related sessions can be cancelled in the calling StatusTask. */
    private byte[] createSessionWithURL(URL url)
            throws IOException {
        /* Create a new session. This may reuse an existing socket from the
         * URLConnection pool if the keep-alive has not yet expired. */
        HttpsURLConnection session = (HttpsURLConnection)url.openConnection();
        try {
            /* Create an output stream to store the incoming stream. */
            ByteArrayOutputStream responseData = new ByteArrayOutputStream();
            /* The connection is actually created here. */
            BufferedInputStream in = /* Request the input stream. */
                    new BufferedInputStream(session.getInputStream());
            /* Check the response code indicated a successful connection. */
            if (session.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(session.getResponseMessage() +
                        ": with url: " + url.toString());
            }
            int bytesRead; /* Count of bytes read. */
            /* Buffered streams are 8kb aligned in memory. */
            byte[] buffer = new byte[8192];
            /* Read the input stream data until end-of-stream. */
            while ((bytesRead = in.read(buffer,0,buffer.length)) > 0) {
                /* Concatenate the received data together. */
                responseData.write(buffer, 0, bytesRead);
            }
            in.close(); /* Close the input stream to release resources. */
            responseData.close(); /* No more data to concatenate. */
            /* Prepare the response data for further processing. */
            return responseData.toByteArray();
        } finally {
            session.disconnect(); /* Disconnect the session. */
        }
    }

    /* A check for stock status has completed. Validate response data and notify
     * delegate controllers of the status being available. */
    private void notifyOnStatusAvailable(byte[] responseData)
            throws JSONException {
        /* Check if the response contains a valid result. */
        int resultCount = getCountFromResponse(responseData);
        if (resultCount != 1) {
            /* Response data doesn't contain a single result. */
            mStockStatus = null; /* Maybe there's no results. */
        } else {
            /* Extract a Status from the response data. */
            mStockStatus = getStatusFromResponse(responseData);
        }
        /* Notify delegate controllers that the status request is complete. */
        mIsStatusDone = true;
        notifyOnReadyToPostStatus();
    }

    /* A check for stock status has failed due to an exception. Notify delegate
     * controllers of the failure and provide the Throwable object. */
    private void notifyOnStatusFailedWithError(Throwable error) {
        if (mDelegate != null) {
            mDelegate.statusDidNotCompleteWithError(error);
        }
    }

    /* A check for competitors has failed due to an exception. Notify delegate
     * controllers of the failure and provide the Throwable object. */
    private void notifyOnCompetitorsFailedWithError(Throwable error) {
        if (mDelegate != null) {
            mDelegate.competitorsNotRetrievedWithError(error);
        }
    }

    /* A check for stock competitors has completed. Process the response data
     * and notify delegate controllers of the data being available. */
    private void notifyOnCompetitorsAvailable(byte[] responseData)
            throws JSONException {
         /* Check if competitors were returned in the response data. */
        int competitorsCount = getCountFromResponse(responseData);
        if (competitorsCount > 0) {
            /* Extract the competitors data from the response data. */
            mStockCompetitors = extractCompetitorsFromResponse(responseData);
        }
        /* Notify delegate controllers that the competitor data is available. */
        mIsCompetitorsDone = true;
        notifyOnReadyToPostStatus();
    }

    /* Parse the competitors data from the response data. */
    private List<Competitor> extractCompetitorsFromResponse(byte[] data)
            throws JSONException {
        /* Store the extracted competitors (if any) in a temporary location. */
        List<Competitor> list = new ArrayList<>();
        /* Convert the response data to a JSONObject. */
        JSONObject competitorData = new JSONObject(new String(data))
                .getJSONObject("query")
                .getJSONObject("results");
        JSONArray competitors;
        try {
            competitors = competitorData.getJSONArray("tr");
        } catch (JSONException e) {
            return list;
        }
        /* Enumerate over competitor data. */
        for (int i = 1; i < getCountFromResponse(data); i++) {
            /* Extract the nested object containing the change indicator. */
            JSONObject nestedObject = competitors.getJSONObject(i)
                    .getJSONArray("td").getJSONObject(1).getJSONObject("span");
            /* As of April 2016 Yahoo! can sometimes provide malformed data in
             * competitor response data. Specifically Yahoo! doesn't appear to
             * consistently update the trading symbol when it changes. An
             * example would be the merge of Burger King and Tim Hortons where
             * trading of previous symbols ceased and a new symbol, QSR, was
             * introduced (end of 2014). Previous references should have been
             * updated, and this didn't happen. As a result the JSON response
             * becomes slightly different when it contains these symbols. This
             * difference triggers an exception. Here the implementation checks
             * for this Yahoo! bug using try/catch.
             * REFERENCE: Symbol GTIM and MCD include BKW instead of QSR. */
            try {
                /* Extract the competitor's change. This will throw an exception
                 * when encountering the Yahoo! bug described above. */
                JSONObject validate = nestedObject.getJSONObject("img");
            } catch (JSONException e) {
                Log.i(TAG, "Encountered a malformed competitor during " +
                        "extractCompetitorsFromResponse. Skipping entry.");
                continue; /* Skip the entry. */
            }
            /* Extract the competitor's symbol. */
            String symbol = competitors.getJSONObject(i)
                    .getJSONArray("td").getJSONObject(0).getJSONObject("a")
                    .getString("content");
            /* Extract the competitor's company name. */
            String name = competitors.getJSONObject(i)
                    .getJSONArray("td").getJSONObject(0).getJSONObject("a")
                    .getString("title");
            /* Guard against missing change data from content provider. */
            String changePercent = null;
            changePercent = nestedObject.getJSONObject("b").isNull("content") ?
                    "00.00": nestedObject.getJSONObject("b").optString("content", "00.00");
            /* Guard against missing up/down indicator from content provider. */
            String change = nestedObject.getJSONObject("img").isNull("alt") ?
                    "": nestedObject.getJSONObject("img").optString("alt");
            /* Encode change up/down in changePercent. */
            if (!change.isEmpty()) {
                if (change.equals("Up")) {
                    changePercent = "+" + changePercent;
                } else if (change.equals("Down")) {
                    changePercent = "-" + changePercent;
                }
            }
            String marketCap = null;
            try {
                /* Extract the nested entry containing the market cap. */
                nestedObject = competitors.getJSONObject(i)
                    .getJSONArray("td").getJSONObject(2).getJSONObject("span");
                /* Guard against missing market cap data from content provider. */
                marketCap = nestedObject.isNull("content") ?
                        "00.00B": nestedObject.optString("content", "00.00B");
            } catch (JSONException e) {
                Log.i(TAG, "Encountered a malformed competitor during " +
                        "extractCompetitorsFromResponse. Skipping entry.");
                continue;
            }
            /* Update the list of competitors. */
            list.add(new Competitor( /* Create the model entity. */
                    null, symbol, name, changePercent, marketCap));
        }

        return list; /* The list containing competitor data. */
    }

    /* A check for stock chart has failed due to an exception. Notify delegate
     * controllers of the failure and provide the throwable object. */
    private void notifyOnChartFailedWithError(Throwable error) {
        if (mDelegate != null) {
            mDelegate.chartDidNotRetrieveWithError(error);
        }
    }

    /* A check for stock chart has completed. Process the response data and
     * notify delegate controllers of the chart being available. */
    private void notifyOnChartAvailable(byte[] responseData) {
        /* Create a Chart using the response data. */
        mStockChart = new Chart(responseData);
        /* Notify delegate controllers that the chart data is available. */
        mIsChartDone = true;
        notifyOnReadyToPostStatus();
    }

    /* The count contained in a JSON formatted result is the authoritative count
     * of usable objects described. */
    private int getCountFromResponse(byte[] data) throws JSONException {
            return new JSONObject(new String(data))
                    .getJSONObject("query").getInt("count");
    }

    /* Extract the stock status from the query response data and make it
     * available. The JSONObject parameter `data` contains the unfiltered
     * response from a completed status query session. */
    private Status getStatusFromResponse(byte[] data) throws JSONException {
        /* Select the json object containing status data. */
        JSONObject statusObject = new JSONObject(new String(data))
                .getJSONObject("query")
                .getJSONObject("results")
                .getJSONObject("row");
        /* Return a new Status initialised with the json data. */
        return new Status(
                null, /* The unset stock id (can be set later). */
                statusObject.getString("currency"),
                statusObject.getString("change"),
                statusObject.getString("percent_change"),
                statusObject.getString("prev_close"),
                statusObject.getString("open"),
                statusObject.getString("bid"),
                statusObject.getString("ask"),
                statusObject.getString("year_target_est"),
                statusObject.getString("days_range"),
                statusObject.getString("year_range"),
                statusObject.getString("volume"),
                statusObject.getString("avg_volume"),
                statusObject.getString("market_cap"),
                statusObject.getString("last_trade_date"),
                statusObject.getString("last_trade_time"),
                /* Initialise with empty chart and competitors. */
                null,null);
    }

    /* Some of the numeric attribute returned during a status update request
     * contain numeric content padded with arbitrary zeros. The conversion to a
     * intermediate float removes any zero padding. This method returns the
     * attribute unmodified if it cannot be parsed as numeric content. */
    private String formatNumericAttribute(String attribute) {
        /* Check if it's possible to convert the attribute to a number. */
        try {
            Float value = Float.parseFloat(attribute);
            /* Numeric reformatting completed. */
            return String.format(Locale.getDefault(), "%1.2f", value);
        } catch (NumberFormatException e) {
            return attribute; /* The non-numeric attribute unformatted. */
        }
    }

    /* Reformat a Status's attributes. The parameter `status` will be modified
     * during formatting. Returns the modified Status. */
    private Status formatStatus(Status status) {
        /* Update the status data after formatting. */
        status.setPrevClose(formatNumericAttribute(status.getPrevClose()));
        status.setOpen(formatNumericAttribute(status.getOpen()));
        status.setBid(formatNumericAttribute(status.getBid()));
        status.setAsk(formatNumericAttribute(status.getAsk()));
        status.setYearTargetEst(formatNumericAttribute(status.getYearTargetEst()));
        /* Reformat day's range. */
        String[] dayRange = status.getDaysRange().split("[-]");
        String low = formatNumericAttribute(dayRange[0]);
        String high = formatNumericAttribute(dayRange[1]);
        status.setDaysRange(low + " - " + high);
        /* Reformat year's range. */
        String[] yearRange = status.getYearsRange().split("[-]");
        low = formatNumericAttribute(yearRange[0]);
        high = formatNumericAttribute(yearRange[1]);
        status.setYearsRange(low + " - " + high);
        /* Reformat the last trade date. */
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
            Date lastTradeDate = dateFormat.parse(status.getLastTradeDate());
            dateFormat.applyPattern("MMM dd yyyy");
            status.setLastTradeDate(dateFormat.format(lastTradeDate));
        } catch (ParseException e) {
            Log.i(TAG, "During formatStatus last trade date was not formatted");
        }
        /* Return the parameter status with updates applied. */
        return status;
    }

    /* Check if the internal state signals the processing of data is complete.
     * Validate response data and notify delegate controllers that the status
     * requests have completed and the data is ready to use. */
    private void notifyOnReadyToPostStatus() {
        if (mIsStatusDone && mIsChartDone && mIsCompetitorsDone) {
            /* Check if a status was found during the update request. */
            if (mStockStatus == null) {
                /* No result was found. Provide an empty status. */
                mStockStatus = getNoResultFoundStatus();
            } else {
                /* Make any last modifications to the status. */
                mStockStatus = formatStatus(mStockStatus);
            }
            /* Merge in the chart and competitor data. */
            mStockStatus.setChartData(mStockChart);
            mStockStatus.setCompetitorData(mStockCompetitors);
            /* Check if the status can be saved automatically. */
            if (mId != null) {
                /* The stockId is known. */
                mStockStatus.setStockId(mId);
                /* Save the status. */
                StockDbHelper.get(sContext).submitStatus(mStockStatus);
            }
            /* Notify interested controllers that the status is available. */
            if (mDelegate != null) {
                mDelegate.statusDidCompleteWith(mStockStatus);
            }
            mDidFinishStatusUpdate = true;
        }
    }

    /* Reset the internal state to initial values. */
    private void resetInternalState() {
        mId = null; /* Next Status produced will not auto-save. */
        resetProcessedState();
    }

    private void resetProcessedState() {
        mIsStatusDone = false;
        mIsChartDone = false;
        mIsCompetitorsDone = false;
    }

    /* A Task which performs a status update related data request. The data
     * requested depends on the session type specified during initialise. The
     * symbol parameter is the stock whose status is being updated. Session type
     * may be STATUS_SESSION_TYPE, CHART_SESSION_TYPE, and
     * COMPETITOR_SESSION_TYPE. Requests the data on a background thread and
     * then processes it to produce Status, Chart, and Competitors. */
    private class StatusTask extends AsyncTask<String,Void,Void> {
        private final int mSessionType;
        /* Default constructor that executes a status update using the provided
         * symbol name and session type. */
        public StatusTask(String symbolParam, int sessionType) {
            /* Support parallel execution for performance. */
            mSessionType = sessionType;
            executeOnExecutor(THREAD_POOL_EXECUTOR, symbolParam);
        }
        @Override
        /* Perform the task on a background thread. */
        protected Void doInBackground(String... params) {
            try {
                if (params.length != 1) {
                    throw new IllegalArgumentException(
                            "A status update was executed with an invalid " +
                            "parameter. Provide a valid symbol.");
                }
                /* Use the symbol parameter passed during instance creation. */
                String symbol = params[0];
                /* Request the URL for this status's session. */
                URL url = null;
                switch (mSessionType) {
                    case STATUS_SESSION_TYPE:
                        url = getURLForStatus(symbol);
                        break;
                    case CHART_SESSION_TYPE:
                        url = getURLForChart(symbol);
                        break;
                    case COMPETITOR_SESSION_TYPE:
                        url = getURLForCompetitors(symbol);
                        break;
                    default:
                        break;
                }
                /* Create the stats session. */
                byte[] responseData = createSessionWithURL(url);
                /* Check if this StatusTask has been cancelled. */
                if (!isCancelled()) {
                    /* Process response data. */
                    switch (mSessionType) {
                        case STATUS_SESSION_TYPE:
                            notifyOnStatusAvailable(responseData);
                            break;
                        case CHART_SESSION_TYPE:
                            notifyOnChartAvailable(responseData);
                            break;
                        case COMPETITOR_SESSION_TYPE:
                            notifyOnCompetitorsAvailable(responseData);
                            break;
                        default:
                            break;
                    }
                }
            } catch (InterruptedIOException interrupted) {
                /* Not doing anything here as the StatusTask was cancelled. */
                return null;
            } catch (Exception error) {
                /* Pass all other exceptions to the delegate for action. */
                switch (mSessionType) {
                    case STATUS_SESSION_TYPE:
                        notifyOnStatusFailedWithError(error);
                        break;
                    case CHART_SESSION_TYPE:
                        notifyOnChartFailedWithError(error);
                        break;
                    case COMPETITOR_SESSION_TYPE:
                        notifyOnCompetitorsFailedWithError(error);
                        break;
                    default:
                        break;
                }
            }
            return null; /* Finished this background thread. */
        }
    }
}
