//
//  StockSearch.java
//  StockApp
//
//  An object that performs symbol lookups and notifies interested controllers
//  when this task is completed using the StockSearchDelegate protocol. The
//  connections are performed asynchronously using AsyncTask.
//
//  -- Implementation Notes ----------------------------------------------------
//  This class uses Yahoo! Finance for symbol lookup. The Yahoo URL used is:
//  https://ca.finance.yahoo.com/lookup/stocks?s=QUERY&t=S&b=0&m=ALL
//  where `s=QUERY` is the symbol or description to search for, `t=S` represents
//  a search for stocks, `b=0` is the offset into results (only useful if total
//  results exceeds 20), and `m=ALL` represents the market to search (ALL, or US
//  for US and Canada).
//
//  The results are pre-filtered and converted to JSON using YQL:
//  https://developer.yahoo.com/yql/ where the select string used is either
//
//  (A) For symbol lookup using <tr> tags of class yui-dt-odd or yui-dt-even.
//  select * from html where url='YAHOO_URL_ABOVE' and xpath=
//  '//tr[contains(@class,"yui-dt-")]'
//
//  -or-
//
//  (B) For obtaining total results using <li> tags of class selected.
//  select * from html where url='YAHOO_URL_ABOVE' and xpath=
//  '//li[contains(@class,"selected")]//a/em'
//
//  Finally, once the JSON data is returned, it is converted to JSONObjects
//  by selecting the appropriate keys or key paths. In general this means less
//  data is read by the Android device and less local processing is
//  required. Thereby reducing expensive wireless radio use and cpu time.
//  ----------------------------------------------------------------------------
//
//  Created by David McKnight on 2016-04-15.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.contentprovider.search;

import android.os.AsyncTask;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.ssl.HttpsURLConnection;
import xyz.oswind.stockapp.model.entities.Stock;
import static android.os.AsyncTask.Status;

@SuppressWarnings("FieldCanBeLocal")
public class StockSearch {
    private static final String TAG = "StockSearch";
    /* Public constants declaring stock-provider specific parameters. */
    public static final int YAHOO_MAX_PER_REQUEST = 20;
    /* Local constants declaring the partial strings of a stock symbol lookup-
     * specific query. These are expected to be the same for all symbol lookups.
     * See getFormattedUrlStringWithQuery for how the string is joined. */
    private static final String QUERY_URL_START =
            "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20" +
            "html%20where%20url%3D'https%3A%2F%2Fca.finance.yahoo.com%2Flooku" +
            "p%2Fstocks%3Fs%3D";
    private static final String QUERY_URL_PRE_INDEX = "%26t%3DS%26b%3D";
    private static final String QUERY_URL_POST_INDEX_PRE_MARKET = "%26m%3D";
    private static final String QUERY_URL_PRE_MARKET = "%26t%3DS%26m%3D";
    private static final String QUERY_URL_POST_MARKET = "'%20and%20xpath%3D";
    private static final String QUERY_URL_ENDING =
            "&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
    private static final String QUERY_URL_XPATH_SEARCH =
            "'%2F%2Ftr%5Bcontains(%40class%2C%22yui-dt-%22)%5D'";
    private static final String QUERY_URL_XPATH_TOTAL =
            "'%2F%2Fli%5Bcontains(%40class%2C%22selected%22)%5D%2F%2Fa%2Fem'";
    private static final String QUERY_URL_WORLD_MARKETS = "ALL";
    private static final String QUERY_URL_LOCAL_MARKETS = "US";
    /* Local constants declaring common key-paths for selecting stock data. */
    private static final String[] KEY_PATH_FOR_MANY_ROWS =
            {"query","results","tr"};
    private static final String[] KEY_PATH_FOR_SINGLE_ROW =
            {"query","results","tr","td"};
    /* Hang onto the current formatted query string. It may be needed for
     * multiple requests such as more results and obtaining totals. */
    private String mCurrentFormattedQuery;
    /* Contains the query response data obtained from the server. */
    private JSONObject mResponseData;
    /* A shared regular expression matcher for re-formatting queries. */
    private final Matcher mQueryFormatter;
    /* Contains the current query session. May not be running or valid. */
    private SearchTask mSearchTask;
    /* Notify a controller that implements the StockSearchDelegate protocol when
     * symbol queries complete or when errors occur. */
    private StockSearchDelegate mDelegate;
    /* Contains the current state. For simplicity the StockSearch object can be
     * reading `stock symbol` data or `total stock symbols` data. If the state
     * is FALSE then mResponseData contains unprocessed `stock symbol` data. If
     * the state is is TRUE then mResponseData contains unprocessed `total stock
     * symbols` data. */
    private volatile boolean mStateIsReadingTotals;
    /* A controller using StockSearch can issue multiple requests for more
     * results. Keep track of when the session is active, in particular for
     * requesting more results. This will guard against overlapping requests for
     * more results. */
    private volatile boolean mMoreResultsSessionIsBusy;
    /* Contains the offset into mTotalResults when requesting more results. */
    private volatile int mIndexIntoResults;
    /* The total number of results found for the current query. This may not be
     * the same as the current number of results processed in mSearchResults. */
    private volatile int mTotalResults;
    /* When searching for stocks this property indicates if results should be
     * returned from world markets. Default is TRUE. A setting of FALSE returns
     * stocks from local (US & Canada) markets. */
    private volatile boolean mQueryWorldMarkets;
    /* When a search query is completed make it available for view controllers.
     * This reference may be passed to controllers implementing the
     * StockSearchDelegate protocol. The symbols are stored as Stock objects. */
    private volatile List<Stock> mSearchResults;

    /* The default constructor which prepares the initial state. */
    public StockSearch() {
        mQueryWorldMarkets = true;         /* Search world markets. */
        mStateIsReadingTotals = false;     /* Processing symbol data. */
        mTotalResults = 0;                 /* No results yet. */
        mIndexIntoResults = 0;             /* Starting offset into results. */
        mMoreResultsSessionIsBusy = false; /* Not requesting more results. */
        /* Prepare a shared regular-expression matcher for reuse. */
        mQueryFormatter = Pattern.compile("[-\\.\\s]").matcher("");
    }

    /* Notify a controller that implements the StockSearchDelegate protocol when
     * symbol queries complete or when errors occur. The parameter `delegate` is
     * the controller that will receive protocol messages. */
    public void setDelegate(StockSearchDelegate delegate) {
        mDelegate = delegate; /* Set the delegate. */
    }

    public void resetSearch() {
        if (mSearchTask != null &&
                mSearchTask.getStatus() == Status.RUNNING) {
            mSearchTask.cancel(true);
        }
    }

    /* When a search query is completed make it available for view controllers.
     * This reference may be passed to controllers implementing the
     * StockSearchDelegate protocol. The symbols are stored as Stock objects.
     * Returns a reference to the currently processed search results. */
    public List<Stock> getSearchResults() {
        return mSearchResults;
    }

    /* The total number of results found for the current query. This may not be
     * the same as the current number of results processed in mSearchResults. */
    public int getTotalResults() {
        return mTotalResults;
    }

    /* A convenience method which returns the count of stock symbols that have
     * been processed. */
    public int getResultCount() {
        return mSearchResults.size();
    }

    /* When searching for stocks this property indicates if results should be
     * returned from world markets. Default is TRUE. A setting of FALSE returns
     * stocks from local (US & Canada) markets. */
    public void setQueryWorldMarkets(boolean queryWorldMarkets) {
        mQueryWorldMarkets = queryWorldMarkets;
    }

    /* Perform a stock symbol lookup using the query string passed. The string
     * parameter `query` can contain a stock description, a partial stock
     * symbol, or a complete stock symbol to search for. */
    public void performSearchWithString(String query) {
        /* Guard against a missing delegate or search with empty queries. */
        if (mDelegate != null && query != null && !query.equals("")) {
            /* A search was requested so reset current state. */
            resetInternalState();
            /* Hang onto the formatted query as it may be needed again. */
            mCurrentFormattedQuery = reformatQueryString(query);
            /* Reset any pending search that has not yet completed. */
            resetSearch();
            /* Request a search using the Xpath for symbol search. */
            mSearchTask = new SearchTask(QUERY_URL_XPATH_SEARCH);
        }
    }

    /* Obtain a stock entry at an offset into available symbols. The parameter
     * `index` contains the offset into available symbols requested. Returns the
     * Stock object found or an exception if it was out-of-bounds. */
    public Stock getStockAtIndex(int index) {
        /* Validate the index requested. */
        if (index >= mSearchResults.size() || index < 0) {
            throw new IndexOutOfBoundsException("The index specified: " +
                    index + " was out of bounds on result set.");
        }
        return mSearchResults.get(index); /* Return the Stock object. */
    }

    /* Request the next window of results using the last query provided. If more
     * results were previously found this will create a SearchTask using the
     * query string for the next set of results. The results will then be loaded
     * into mResponseData for extraction. If no more results were found this
     * method will do nothing. Controllers must have implemented the
     * StockSearchDelegate protocol to be notified of completion. */
    public void requestNextResults() {
        /* Total results is only updated if more results were found. Note the
         * second predicate (unprocessed results) depends on successfully
         * processing both total results and existing results. */
        if (!mMoreResultsSessionIsBusy && /* Not already busy. */
                /* Check for unprocessed results. */
                mTotalResults > mSearchResults.size() &&
                /* Check the next window of results remains in range. */
                nextIndexIsInRange()) {
            /* Prevent overlapping requests. */
            mMoreResultsSessionIsBusy = true;
            /* Update index for use with getURLForCurrentQueryWithXpath. */
            mIndexIntoResults += YAHOO_MAX_PER_REQUEST;
            /* Request the next window of results using the Xpath for search. */
            mSearchTask = new SearchTask(QUERY_URL_XPATH_SEARCH);
        } else {
            /* A sanity check failed:
             * - a more results session may already be active and incomplete.
             * - all results may have already been processed.
             * - the requested index may be out of bounds.*
             * Send the delegate a message indicating no further results. */
            if (mTotalResults == mSearchResults.size() ||
                    !nextIndexIsInRange()) {
                /* A controller should take note that no more results exist. */
                mDelegate.moreResultsDidComplete();
                /* Re-enable more results requests. */
                mMoreResultsSessionIsBusy = false;
            }
        }
    }

    /* Get an URL corresponding to a stock symbol lookup. Internally uses the
     * query string which is expected to have already been reformatted as
     * required (using reformatQueryString) and set to currentFormattedQuery.
     * Also internally uses the current index into the results (default zero
     * unless indexIntoResults is updated). The parameter `xpath` must be one of
     * either queryUrlXpathTotal or queryUrlXpathSearch for the URL to be valid.
     * Returns an URL initialized to complete a stock symbol lookup. */
    private URL getURLForCurrentQueryWithXpath(String xpath)
            throws MalformedURLException {
        /* The formatted string representation of the URL. */
        String urlString = getUrlStringWithQuery(
                mCurrentFormattedQuery,  /* Current formatted query. */
                mIndexIntoResults,       /* Current window of results. */
                getURLMarketSpecifier(), /* World or local markets. */
                xpath /* The xpath to use when filtering response data. */
        );
        return new URL(urlString); /* The URL representation. */
    }

    /* Based on the current state of property mQueryWorldMarkets obtain the
     * market specifier to use in query strings. */
    private String getURLMarketSpecifier() {
        if (mQueryWorldMarkets) {
            return QUERY_URL_WORLD_MARKETS; /* Search in world markets. */
        } else {
            return QUERY_URL_LOCAL_MARKETS; /* Search in local markets. */
        }
    }

    /* Apply any required fixes to a query string. Convert any characters that
     * (1) violate HTML specifications, or (2) would otherwise cause the search
     * to fail. The string parameter `query` can contain a stock description, a
     * partial stock symbol, or a complete stock symbol to search for.
     * Reformatting requires converting problem characters to the HTML entity
     * `&nbsp;`. This ensures compatibility with lookup engines that support
     * embedded spaces. It also prevents spaces from breaking queries in general
     * (at least in the case of Yahoo!). This may require additional adjustments
     * for international localization. See mQueryFormatter when changing the
     * regular expression used during formatting. */
    private String reformatQueryString(String query) {
        /* The reformatted query string. */
        return mQueryFormatter.reset(query).replaceAll("%26nbsp%3B");
    }

    /* Create a HttpsURLConnection using the URL passed. Any existing search
     * query related sessions can be cancelled in SearchTask. Parallel
     * connections are, however, allowed when sessions are not related to a new
     * search query. In general this won't happen often as the StockSearch
     * implementation prevents requesting more results until total results is
     * known. When the initial results count equals YAHOO_MAX_PER_REQUEST it
     * becomes possible to have a parallel `total results` session together with
     * requesting the next window of results. Delegated controllers are
     * currently expected to block this behaviour until receiving total results
     * (using either didAlreadyGetTotalResultsForLastQuery or
     * didGetTotalResultsForLastQuery). */
    private void createSessionWithURL(URL url)
            throws IOException,JSONException {
        /* Create a new session. This may reuse an existing socket from the
         * URLConnection pool if the keep-alive has not yet expired. */
        HttpsURLConnection session = (HttpsURLConnection)url.openConnection();
        try {
            /* Create an output stream to store the incoming stream. */
            ByteArrayOutputStream responseData = new ByteArrayOutputStream();
            /* The connection is actually created here. */
            BufferedInputStream in = /* Request the input stream. */
                    new BufferedInputStream(session.getInputStream());
            /* Check the response code indicated a successful connection. */
            if (session.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(session.getResponseMessage() +
                        ": with url: " + url.toString());
            }
            int bytesRead; /* Count of bytes read. */
            /* Buffered streams are 8kb aligned in memory. */
            byte[] buffer = new byte[8192];
            /* Read the input stream data until end-of-stream. */
            while ((bytesRead = in.read(buffer,0,buffer.length)) > 0) {
                /* Concatenate the received data together. */
                responseData.write(buffer, 0, bytesRead);
            }
            in.close(); /* Close the input stream to release resources. */
            responseData.close(); /* No more data to concatenate. */
            /* Prepare the response data for further processing. */
            mResponseData = new JSONObject(
                    /* Create a string representation of the byte-stream. */
                    new String(responseData.toByteArray())
            );
        } finally {
            session.disconnect(); /* Disconnect the session. */
        }
    }

    /* Extract stock symbols and their descriptions from the query's response
     * data. The parameter `data` contains the unfiltered response from a
     * completed query session. Returns a List object initialized with the Stock
     * objects extracted from `data`. */
    private List<Stock> getSymbolsFromResponse(JSONObject data)
            throws JSONException {
        /* Store the Stocks extracted from `data`. */
        List<Stock> extractedSymbols = new ArrayList<>();
        /* Extract the number of symbols returned in the response data. The
         * response data contains the authoritative count of results. This is
         * important because the response data changes structure in a subtle way
         * depending on if multiple results are returned. Counting the results
         * using JSON serialization methods and key path is not always accurate.
         * The key path to use can then be adjusted accordingly. */
        int count = getCountFromResponse(data);
        /* Only proceed if results were found. */
        if (count > 0) {
            /* Key path for row extraction is based on the count of results.
             * Originally this was set to always extract the row key path. When
             * a single result is contained in the response data the <td> is not
             * a valid key path. */
            String[] keyPath = count > 1 ?
                    KEY_PATH_FOR_MANY_ROWS: KEY_PATH_FOR_SINGLE_ROW;
            /* The last key path entry selects the useful rows. */
            int rowSelector = keyPath.length - 1;
            JSONObject rowFilter = data; /* Begin row extraction. */
            for (int i = 0; i < rowSelector; i++) {
                /* Iterate over the key path in use to obtain contained rows. */
                rowFilter = rowFilter.getJSONObject(keyPath[i]);
            }
            /* Now select the rows containing useful content. */
            JSONArray htmlRows = rowFilter
                    .getJSONArray(keyPath[rowSelector]);
            /* Extract symbols and descriptions from the html rows. */
            if (count == 1) {
                /* Response data contains a single result. Process the row. */
                Stock result = processResultWithRow(htmlRows);
                extractedSymbols.add(result);
            } else {
                /* Response data contains multiple results. Iterate over the
                 * rows and extract the stocks. */
                for (int i = 0; i < htmlRows.length(); i++) {
                    Stock result = processResultWithRow(
                            htmlRows.getJSONObject(i).getJSONArray("td"));
                    extractedSymbols.add(result);
                }
            }
        }
        /* Return the filtered data consisting of extracted Stock objects. */
        return extractedSymbols;
    }

    /* A check for stock symbols using the last query has completed. Notify any
     * interested controllers of the completion when required. */
    private void notifyOnResults() throws JSONException {
        /* Two kinds of results needs to be processed. The initial window and
         * more results. If a more results session is not busy then the response
         * data contains initial results. Otherwise response data contains more
         * results to be processed. */
        if (!mMoreResultsSessionIsBusy) {
            /* A more results session is not active. This indicates response
             * data contains unprocessed initial results. The session has
             * completed and is ready for further processing. Extract the
             * initial results then notify interested controllers. */
            processInitialResults();
        } else {
            /* A more results session is active. This indicates response data
             * contains new unprocessed stock symbols. Concatenate original
             * results and new results then notify interested controllers. */
            processMoreResults();
        }
    }

    /* A check for stock symbols has failed due to an exception. Notify any
     * interested controllers of the failure, providing the Throwable object. */
    private void notifyOnSessionFailedWithError(Throwable error) {
        mDelegate.searchDidNotCompleteWithError(error);
    }

    /* Rows extracted from response data using valueForKey may contain objects
     * of type Null instead of String. The parameter `row` contains a html row
     * from the response data. Check the row and replace any instances of Null
     * with empty strings. This ensures that any objects the rely on the
     * processed symbols will get strings consistently. Returns a Stock object
     * containing the extracted symbol and description. */
    private Stock processResultWithRow(JSONArray row) throws JSONException {
        /* Extract the symbol string. Replace Null with empty strings. */
        String symbol = row.getJSONObject(0)
                .getJSONObject("a").optString("content"); /* Note optString. */
        /* Check if symbol was missing. This should probably never be seen. */
        if (symbol.equals("")) {
            /* Log the missing symbol. */
            Log.i(TAG,"Response data contains an unset symbol.");
        }
        /* Extract the description/name. Replace Null with empty strings. */
        String name;
        /* Check if the value for stock name is a null value. */
        if (row.isNull(1)) {
            name = ""; /* Found a null value where name should have been. */
        } else {
            /* Use optString to catch unforeseen coercion bugs. */
            name = row.optString(1);
        }
        /* Check if a log entry for name is needed. */
        if (name.equals("")) {
            /* Log the missing description/name. */
            Log.i(TAG,"Response data contains an unset name.");
        }
        return new Stock(symbol, name); /* Return the extracted Stock. */
    }

    /* Obtain the total number of stocks returned by a symbol lookup query. By
     * default the source (Yahoo) returns 20 symbols at a time. This is used to
     * obtain the maximum number of symbols that can be shown for a query. Note
     * this is intended to be called either before or after the actual results.
     * It shouldn't be called in parallel because it shares mResponseData (which
     * has not been annotated as volatile) and *may* overwrite any unprocessed
     * results. It also doesn't perform any additional checks on the validity of
     * query (not null or empty). These checks/formatting are expected to have
     * already been performed prior to this method being called. */
    private void getTotalResultsWithCurrentQuery() {
        /* Update the current processing state. Switch to reading total. */
        mStateIsReadingTotals = true;
        /* Request total results for the current query. */
        mSearchTask = new SearchTask(QUERY_URL_XPATH_TOTAL);
    }

    /* Obtain the complete string representation of an URL. This is intended to
     * be used prior to creating the URL. The parameter `query` is expected to
     * have already been reformatted if required (using reformatQueryString).
     * Returns the result of joining URL partial strings. */
    private String getUrlStringWithQuery(String query, int andIndex,
                                         String andMarket, String andXpath) {
        return QUERY_URL_START +
                query +
                QUERY_URL_PRE_INDEX +
                andIndex +
                QUERY_URL_PRE_MARKET +
                andMarket +
                QUERY_URL_POST_MARKET +
                andXpath +
                QUERY_URL_ENDING;
    }

    /* The count contained in a JSON formatted result is the authoritative count
     * of useful objects described. */
    private int getCountFromResponse(JSONObject data) throws JSONException {
        return data.getJSONObject("query").getInt("count");
    }

    /* Obtain the total number of stock symbols that can be returned from a GET.
     * This is based on the query performed and facilitates lazy loading. The
     * parameter `data` must contain response data from a query that used
     * queryUrlXpathTotal otherwise this method will return zero. The total is
     * returned as a string object. */
    private String getTotalFromResponse(JSONObject data) throws JSONException {
        /* Select the content key path which contains the total symbols found.
         * Then convert this result to a string representation. */
        String totalContent = data.getJSONObject("query")
                .getJSONObject("results").getJSONObject("em")
                .getString("content");
        /* The value to extract is enclosed in parenthesis. */
        String[] totalString = totalContent.split("[()]");
        /* Guard against returning an invalid total. */
        if (totalString.length != 3 || totalString[1] == null) {
            Log.e(TAG,"Total symbols was requested but is invalid.");
            return "0";
        }
        return totalString[1]; /* The total is at offset [1]. */
    }

    /* Check if the last query submitted has more results than the current
     * symbols found. Reuses the last formatted query with queryUrlXpathTotal to
     * obtain a concrete number of symbols that can be lazy loaded. */
    private void checkLastQueryForMoreResults() {
        /* Now that some results have been processed request the total
         * number of results that exist. Only do this if the source-specific
         * maximum has been reached (for Yahoo this is 20). */
        if (mSearchResults.size() == YAHOO_MAX_PER_REQUEST) {
            /* Check for more symbols because there might be more. */
            getTotalResultsWithCurrentQuery();
        } else if (mSearchResults.size() < YAHOO_MAX_PER_REQUEST) {
            /* There are no more symbols. */
            mTotalResults = mSearchResults.size();
            mDelegate.didAlreadyGetTotalResultsForLastQuery();
        }
    }

    /* A check for more results using the last query has completed. Notify any
     * interested controllers if more symbol results were found. */
    private void notifyOnTotalResults() throws JSONException {
        /* The response data should contain a count of JSON objects equal to one
         * or zero. A result of zero means that no further results were found. A
         * result of one means more symbols are available. */
        int jsonResultCount = getCountFromResponse(mResponseData);
        if (jsonResultCount == 1) {
            /* Obtain the total number of results. */
            String total = getTotalFromResponse(mResponseData);
            /* Hang onto the numeric value for convenience. */
            mTotalResults = Integer.parseInt(total);
            /* Notify the delegate that more results were found. */
            mDelegate.didGetTotalResultsForLastQuery(total);
        } else if (jsonResultCount > 1) {
            /* Most likely this means an error in application logic. */
            Log.e(TAG,"Expected a single JSON object containing total " +
                    "results data but instead found: " + jsonResultCount);
        }
        /* Internal state is set back to processing stock symbols. */
        mStateIsReadingTotals = false;
    }

    /* On requesting more results the current index is incremented. However the
     * next index may not be in range. Returns TRUE if the next index is still
     * in range of available results. Otherwise returns FALSE. */
    private boolean nextIndexIsInRange() {
        int nextIndex = mIndexIntoResults + YAHOO_MAX_PER_REQUEST;
        return nextIndex < mTotalResults;
    }

    /* Restore the internal state for processing response data to defaults. */
    private void resetInternalState() {
        mStateIsReadingTotals = false;
        mTotalResults = 0;
        mIndexIntoResults = 0;
        mMoreResultsSessionIsBusy = false;
    }

    /* A query session has completed and response data contains initial results.
     * Extract initial results and notify any interested controllers. */
    private void processInitialResults() throws JSONException {
        /* Extract the symbols from the response data. */
        mSearchResults = getSymbolsFromResponse(mResponseData);
        mIndexIntoResults = 0; /* Set the starting offset into results. */
        /* Symbols have been extracted. Notify interested controllers. */
        mDelegate.searchDidCompleteWithSymbols(mSearchResults);
        /* The first symbols have been reported. Now check if more exist. */
        checkLastQueryForMoreResults();
    }

    /* A query session has completed and response data contains more results.
     * These results need to be extracted and appended to existing results.
     * Notify any interested controllers when this task is complete. */
    private void processMoreResults() throws JSONException {
        /* Append the new results to existing results. */
        mSearchResults.addAll(getSymbolsFromResponse(mResponseData));
        /* Notify interested controllers of the updated results. */
        mDelegate.moreResultsDidCompleteWithMore();
        /* Re-enable more results requests. */
        mMoreResultsSessionIsBusy = false;
    }

    /* When a SearchTask is not cancelled it will call this method to begin
     * processing the response data. Checks if the current state is reading
     * symbol data or total results. */
    private void onSessionDidComplete() throws JSONException {
        /* Check the current processing state. */
        if (!mStateIsReadingTotals) {
            notifyOnResults();      /* Processing symbol data. */
        } else {
            notifyOnTotalResults(); /* Processing total results data. */
        }
    }

    /* An implementation of AsyncTask which will perform StockSearch tasks on a
     * background thread. */
    private class SearchTask extends AsyncTask<String,Void,Void> {
        /* Default constructor that executes a search using an xpath. */
        public SearchTask(String xpathParam) {
            /* Support parallel execution for performance. */
            executeOnExecutor(THREAD_POOL_EXECUTOR, xpathParam);
        }

        @Override
        /* Perform a search using a background thread. */
        protected Void doInBackground(String... params) {
            try {
                if (params.length != 1) {
                    throw new IllegalArgumentException(
                            "A search was executed with an invalid parameter. "
                            + "Provide a single xpath string parameter.");
                }
                /* Use the xpath parameter passed during instance creation. */
                String xpathParam = params[0];
                /* Request the URL for query sessions. */
                URL url = getURLForCurrentQueryWithXpath(xpathParam);
                /* Create the query session. */
                createSessionWithURL(url);
                /* Check if this SearchTask has been cancelled. */
                if (!isCancelled()) {
                    onSessionDidComplete(); /* Process response data. */
                }
            } catch (InterruptedIOException interrupted) {
                /* Not doing anything here as the SearchTask was cancelled. */
                return null;
            } catch (Exception error) {
                /* Pass all other exceptions to the delegate for action. */
                notifyOnSessionFailedWithError(error);
            }
            return null; /* Finished this background thread. */
        }
    }
}
