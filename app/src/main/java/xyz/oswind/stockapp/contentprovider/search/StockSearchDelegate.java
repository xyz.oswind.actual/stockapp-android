//
//  StockSearchDelegate.java
//  StockApp
//
//  Declares the protocol used when a controller is interested in symbol lookup
//  events. Such a controller would be interested in completed queries and any
//  errors that may occur.
//
//  Created by David McKnight on 2016-04-15.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp.contentprovider.search;

import java.util.List;
import xyz.oswind.stockapp.model.entities.Stock;

public interface StockSearchDelegate {
    /* A query which was previously submitted has completed. The parameter
     * `symbols` contains the results as Stock objects. */
    void searchDidCompleteWithSymbols(List<Stock> symbols);
    /* A query which was previously submitted did not complete. The parameter
     * `error` contains the cause of the failure. */
    void searchDidNotCompleteWithError(Throwable error);
    /* A query which was previously submitted now has a total number of results.
     * This may exceed the number of results previously seen when receiving the
     * searchDidCompleteWithSymbols message. This message will be sent when the
     * total number of results exceeds the maximum returned per request. For
     * Yahoo this is defined in StockSearch as YAHOO_MAX_PER_REQUEST. */
    void didGetTotalResultsForLastQuery(String total);
    /* A query which was previously submitted already has a total number of
     * results. This message will be sent when the total number of results is
     * less than the maximum returned per request. For Yahoo this is defined in
     * StockSearch as YAHOO_MAX_PER_REQUEST. The symbols previously returned
     * represent the total of results found for the last query. */
    void didAlreadyGetTotalResultsForLastQuery();
    /* A request for more results has completed. The getter getSearchResults()
     * associated with this instance of StockSearch reflects the current view of
     * total symbols. The updated view of total symbols includes all previous
     * symbols with new symbols appended to the end. Interested controllers
     * should update their view with the new entries. */
    void moreResultsDidCompleteWithMore();
    /* A request for more results has completed without any changes to the
     * property mSearchResults associated with this instance of StockSearch.
     * Most likely this means all the symbols are already loaded and any request
     * for more is out-of-bounds. Interested controllers can use this to
     * indicate no further stock symbols are available to be displayed.*/
    void moreResultsDidComplete();
}
