//
//  TimedProgress.java
//  StockApp
//
//  A task which runs a progress bar for a specific time.
//
//  Created by David McKnight on 2016-05-02.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp;

import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;

public class TimedProgress extends AsyncTask<Void,Void,Void> {
    private final ProgressBar mProgressBar; /* The bar to activate. */
    private final long mLengthMs;           /* The time until deactivated. */

    /* A default constructor which accepts a progress bar and the duration the
     * progress bar should remain visible. */
    public TimedProgress(ProgressBar progressBar, long duration) {
        mProgressBar = progressBar;
        mLengthMs = duration;
        /* AsyncTask's must be started on the ui thread. It should be safe to
         * modify the visible state of the progress bar. */
        mProgressBar.setVisibility(View.VISIBLE);
        executeOnExecutor(THREAD_POOL_EXECUTOR);
    }

    @Override
    /* Sleep on a background thread after activating the progress bar. The bar
     * must have previously been set visible using the default constructor or
     * set visible manually prior to execute. */
    protected Void doInBackground(Void... params) {
        try {
            Thread.sleep(mLengthMs);
        } catch (InterruptedException e) {
            /* Ignore interrupted exceptions. */
        }
        return null;
    }

    @Override
    /* Run on the ui thread. Deactivate the progress bar. */
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        mProgressBar.setVisibility(View.INVISIBLE);
    }
}
