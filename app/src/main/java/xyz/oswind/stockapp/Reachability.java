//
//  Reachability.java
//  StockApp
//
//  A class that attempts to reproduce the reachability api from ios. Monitors
//  the network state of the device and notifies observers when network related
//  events have happened.
//
//  Created by David McKnight on 2016-05-03.
//  Copyright (C) 2016 David McKnight actual@oswind.xyz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/gpl.html>.
//

package xyz.oswind.stockapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.net.ConnectivityManagerCompat;

import java.util.ArrayList;
import java.util.List;

public class Reachability {
    private static final String TAG = "Reachability";
    private static Reachability sMonitor;/* App static Reachability. */
    private static Context sContext;     /* Static in the app's context.*/
    private final ConnectivityManager mConnectivityManager; /* From the device. */
    private final IntentFilter mConnectivity; /* The broadcast filter. */
    private NetworkInfo mLastInfo;      /* The last network state. */
    private List<Callbacks> mObservers; /* Observers of network changes. */

    /* All observers must implement the required callbacks for events. */
    public interface Callbacks {
        void onReachabilityUpdate(); /* Network conditions have changed. */
    }

    /* Shared Reachability singleton access. */
    public static Reachability get(Context context) {
        if (sMonitor == null) {
            sContext = context.getApplicationContext();
            sMonitor = new Reachability();
        }
        return sMonitor;
    }

    /* A default constructor which begins monitoring network state. */
    private Reachability() {
        /* Request the connectivity manager for registering the dynamic
         * broadcast receiver. */
        mConnectivityManager = (ConnectivityManager)sContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        /* Filter broadcasts to connectivity related. */
        mConnectivity = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        mObservers = new ArrayList<>(); /* Initialise observer list. */
        beginMonitoring(); /* Begin monitoring network state. */
    }

    /* An observer would like to monitor network related events. */
    public void addObserver(Callbacks observer) {
        /* Create the observers list if it doesn't exist. */
        if (mObservers == null) {
            mObservers = new ArrayList<>();
        }
        /* Add an observer to the list. */
        if (observer != null) {
            mObservers.add(observer);
        }
    }

    /* An observer would like to stop monitoring network related events. */
    public void removeObserver(Callbacks observer) {
        /* Remove the observer from the list. */
        if (mObservers != null) {
            mObservers.remove(observer);
        }
    }

    /* Clear all observers of network events. */
    private void clearObservers() {
        if (mObservers != null) {
            mObservers.clear();
        }
    }

    /* Check if the last network state was online. */
    public boolean isNetworkOnline() {
        return (mLastInfo != null) && mLastInfo.isConnected();
    }

    /* Create the dynamic broadcast receiver. */
    private void beginMonitoring() {
        sContext.registerReceiver(mOnReachabilityUpdate, mConnectivity);
    }

    /* Remove the dynamic broadcast receiver. */
    private void endMonitoring() {
        clearObservers(); /* Remove the observers. */
        sContext.unregisterReceiver(mOnReachabilityUpdate);
    }

    /* A broadcast receiver which will accept network state broadcasts and
     * notify observers of events. */
    private final BroadcastReceiver mOnReachabilityUpdate = new BroadcastReceiver() {
        @Override
        /* A network state broadcast has been received. */
        public void onReceive(Context context, Intent intent) {
            /* Request the NetworkInfo of the broadcast that triggered this
             * broadcast receiver. */
            NetworkInfo networkInfo = ConnectivityManagerCompat
                    .getNetworkInfoFromBroadcast(mConnectivityManager,intent);
            /* Check if the network is disabled. */
            if (networkInfo == null) {
                mLastInfo = null; /* Most likely the network is disabled. */
            } else {
                /* Network is not disabled. */
                mLastInfo = networkInfo; /* Update last seen network state. */
            }
            /* Notify observers of the updated network state. */
            for (Callbacks observer: mObservers) {
                observer.onReachabilityUpdate(); /* Notify observer. */
            }
        }
    };
}
